'''
Created on 27/mag/2011

@author: Alessandro Timmi
'''
# TODO:
#-aggiungere altri campi al file configurazione json
#-vedi se e' il caso di definire tutti gli attributi di self dentro l'init
#-salvataggio dei plot (pdf, png ecc) only_active_tab / all_tabs;
#-Spostare classe crop dialog in vsutils?
#-sistemare dialog preferences e il fatto che chiude/riapre l'acquisizione
#-span non prende grafico velocita a sinistra
#-metti spunta per attivare/disattivare grafici a piacere
#-acquisizione senza opengl ma dati inviati in tempo reale a manichino 3d python
#-apri pagina html con javascript analytics quando l'utente aggiorna il software

# MEDIO TERMINE:
#-risolvere problema resize plot: poiche' l'evento e' collegato ad ogni canvas,
# quando ridimensiono la finestra la relativa funzione viene richiamata tante volte
# quanti sono i canvas. Forse occorre usare l'evento di wx invece di mpl, ma sembra dare problemi.
#-resize plot solo a rilascio mouseclick per velocizzare resize.
#-mettere annotation su tecnica "best" 
#-aggiungi stesso header del mocap file anche ai file csv esportati.
#-Test con accelerometro per verifica accelerazioni;
#-modifica c++ in modo che crei il file dopo avvenuta auto-calibrazione (circa 10 s), tramite evento (che attualmente non esiste).
#-occorre anche un segnale che dica all'utente che la registrazione e' partita.
#-modifica c++ in modo che l'output sia in una dialog testuale apposita

#-Problema in Subprocess: se si preme rec e l'exe non trova il kinect, occorre cmq
# premere stop. Stessa cosa, dopo aver premuto la x della finestra dell'exe, occorre comunque premere stop.
# Ho provato con Popen.wait() e funziona ma blocca tutta la GUI.
#-Inserimento email da GUI e non da sito, in modo da poter caricare il setup su altri siti
#-subprocess: mettere output vslitetracker.exe in log file invece di console
#-mettere nuova valutazione bonta' acquisizione, basata su affidabilita' fotogrammi
#-C++: far stampare affidabilita' vicino coordinate: se <0.5, il giunto diventa rosso durante riproduzione e il plot anche
#-nel comando export, convertire la data in modo che mostri il mese in lettere invece di numeri (Sept invece di 9)
#-ingrandire carattere font degli static text delle intestazioni delle due sezioni della gui
#-sistema preferences in modo che non occorra chiudere e riaprire il mocap file;

# LUNGO TERMINE
#-aggiungi proiezione a terra baricentro: NON FATTIBILE perche' conosco solo la normale al pavimento (3 elementi della matrice di rotaz).
#-provare animazione tramite unity
#-Possibilita' di leggere file BVH
# aggiungi pulsante che apre multichoice per scegliere semitrasparenza link, da regolare con opacity di vpython
#-aggiungi possibilita' confronto 2 acquisizioni, con time shifting manuale per sincronizzare le tecniche.
#-rendi tutti i button trasparenti e quando li clicchi diventano blu per un attimo tramite apposita funzione
#-vedi per cattura video da kin
#-studia emipolari per sincronizzare 2 kin
#-validazione con accelerometro
#-protezione con seriale
#-avvia pratica partita iva
#
#-creare controlli tramite gesture
#-vedere per acquisizione  + users
#-elaborazione dati per evento calibrazione finita fatta tramite python, inviando dati da c++ subprocess

#-TESTING PER VENDIBILITA':
#-FASE 1) test nostro su kinect + VSLite: mettersi nei panni dell'utente;
#-FASE 2) prendere 10 utenti e farglielo provare da setup a utilizzo
# (prendere feedback su tutto: setup, comprensione e utilita' dell'output);
#-FASE 3) studio dei feedback e modifiche al programma;
#-Capire il tipo di utente tipo e capire le caratteristiche che cerca in questo programma


# Giornata test 1
#-schermata principale con modalita': esercitati, visualizza risultati, tutorial con i campioni, ranking mondiale
#-creare database internazionale coinvolgendo anche JEsse e altri atleti internazionali
#-rifare interfaccia in maniera + intuitiva;
#-in modalita' rec, mettere menu a cascata con tecniche preimpostate + acquisizione libera;
#-per ogni tecnica preimpostata, mostrare dialog (tips) con immagine e testo che spiegano come disporsi per acquisire;
#-reintegra calibrazione psi-pose con cambio colore quando ha calibrato e leva messaggio warning.
#-convertire i file delle acquisizione in binario e permettere i download solo dei primi 10;
#-mettere possibilita' di segnalare acquisizioni inappropriate.
#-standardizzare lunghezza acquisizioni a 10 s;
#-riduci numero grafici visualizzati contemporaneamente per velocizzare riproduzione (es solo pugni o calci);



# Librerie standard:
import os
# libreria per eseguire i file .exe:
import subprocess
# libreria per formattare stringhe, usata per la funzione browse:
import textwrap
import urllib
import json
from datetime import datetime
import webbrowser

# Librerie di terze parti:
import wx
import  wx.lib.colourselect as csel
import numpy as np
# Importa la libreria per la creazione di dialog con testo scorribile:
from wx.lib import dialogs
from wx.lib.wordwrap import wordwrap

# Moduli di virtual sensei:
import cfg
import vsutils
import vscalc2
import vsplot2
import vsanim


        
         
class VSFrame(wx.Frame):
    """GUI di Virtual Sensei Lite."""
    def __init__(self, parent, id_num, title):
        
        # Inizializzazione della finestra della GUI:
        wx.Frame.__init__(self, parent, id_num, title=title,
                          size=cfg.VS_GUI_SIZE)
        
        # legge il file json di configurazione e lo salva in un dict come
        # attributo della GUI:
        self.config_dict = vsutils.json2dict(cfg.CONFIG_FILE_NAME, parent=self) 
        
        # Centra la GUI nello schermo:
        self.Centre()   
                
        # Imposta le dimensioni minime della GUI, pari a quelle iniziali:
        self.SetMinSize(cfg.VS_GUI_SIZE)    
        
        # Icona del programma:
        icon = wx.Icon(cfg.SW_GRAPHICS_FOLDER+os.sep+cfg.SW_ICON,
                       wx.BITMAP_TYPE_ICO)
        self.SetIcon(icon)             

        # Creazione pannello interno alla finestra:
        self.panel = wx.Panel(self)
        
        self.panel.SetBackgroundColour("BLACK")
        vsLogo = wx.StaticBitmap(self.panel, bitmap=wx.Bitmap(cfg.SW_GRAPHICS_FOLDER+os.sep+cfg.SW_LOGO))       
        
        pic_acquisition = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'capture.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_acquisition_disabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'captureDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        btn_acquisition = wx.BitmapButton(self.panel, wx.ID_ANY, pic_acquisition)
        btn_acquisition.SetBitmapDisabled(pic_acquisition_disabled)
        btn_acquisition.SetToolTip(wx.ToolTip('Record a new acquisition'))


        pic_reproduction = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'capture.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_reproduction_disabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'captureDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        btn_reproduction = wx.BitmapButton(self.panel, wx.ID_ANY, pic_reproduction)
        btn_reproduction.SetBitmapDisabled(pic_reproduction_disabled)
        btn_reproduction.SetToolTip(wx.ToolTip('Record a new acquisition'))
        
        pic_ranking = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'capture.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_ranking_disabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'captureDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        btn_ranking = wx.BitmapButton(self.panel, wx.ID_ANY, pic_ranking)
        btn_ranking.SetBitmapDisabled(pic_ranking_disabled)
        btn_ranking.SetToolTip(wx.ToolTip('See the world ranking'))
        
        pic_tutorials = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'capture.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_tutorials_disabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'captureDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        btn_tutorials = wx.BitmapButton(self.panel, wx.ID_ANY, pic_tutorials)
        btn_tutorials.SetBitmapDisabled(pic_tutorials_disabled)
        btn_tutorials.SetToolTip(wx.ToolTip('Watch the tutorial with the champions'))
        
        
        btn_help = wx.Button(self.panel, wx.ID_HELP)

        self.Bind(wx.EVT_BUTTON, self.on_btn_acquisition, btn_acquisition)
        self.Bind(wx.EVT_BUTTON, self.on_btn_reproduction, btn_reproduction)
        self.Bind(wx.EVT_BUTTON, self.on_btn_ranking, btn_ranking)
        self.Bind(wx.EVT_BUTTON, self.on_btn_tutorials, btn_tutorials)
        self.Bind(wx.EVT_BUTTON, self.on_btn_help, btn_help)
        
        mode_sizer = wx.BoxSizer(wx.VERTICAL)
        mode_sizer.Add(vsLogo, proportion=0, flag=wx.ALIGN_CENTRE | wx.ALL, border=cfg.EMPTY_BORDER)
        mode_sizer.Add(btn_acquisition, proportion=0, flag=wx.ALIGN_CENTRE_VERTICAL | wx.ALIGN_LEFT | wx.ALL, border=cfg.EMPTY_BORDER)
        mode_sizer.Add(btn_reproduction, proportion=0, flag=wx.ALIGN_CENTRE_VERTICAL | wx.ALIGN_LEFT | wx.ALL, border=cfg.EMPTY_BORDER)
        mode_sizer.Add(btn_ranking, proportion=0, flag=wx.ALIGN_CENTRE_VERTICAL | wx.ALIGN_LEFT | wx.ALL, border=cfg.EMPTY_BORDER)
        mode_sizer.Add(btn_tutorials, proportion=0, flag=wx.ALIGN_CENTRE_VERTICAL | wx.ALIGN_LEFT | wx.ALL, border=cfg.EMPTY_BORDER)
        mode_sizer.Add(btn_help, proportion=0, flag=wx.ALIGN_RIGHT | wx.ALL, border=cfg.EMPTY_BORDER)
        self.panel.SetSizerAndFit(mode_sizer)
        
        
        
    def on_btn_acquisition(self, event):
        print 'acquisition'
        
    def on_btn_reproduction(self, event):
        print 'reproduction'
    
    def on_btn_ranking(self, event):
        print 'ranking'
    
    def on_btn_tutorials(self, event):
        print 'tutorials'
        
    def on_btn_help(self,event):
        print 'help'


####################################################################
# MAIN che esegue la GUI principale di VIRTUAL SENSEI LITE  
if __name__ == '__main__':   
    #False evita che stderr e stdout vadano in una finestra separata:
    app=wx.App(False) 
    # Costruisce il frame della GUI usando la classe VSGui:
    vs_main_frame = VSFrame(parent=None, id_num=wx.ID_ANY,
                            title=cfg.SW_NAME+' v.'+cfg.SW_VERSION)
    # Mostra il frame costruito:
    vs_main_frame.Show(True)
    # TODO: spostare questa
    # verifica online (in maniera silenziosa) se esiste una versione
    # piu' aggiornata del software all'avvio:
    #vs_main_frame.OnMenuCheckUpdates(None, is_silent=True)
    # Avvia il ciclo interno dell'applicazione,
    # necessario per far funzionare tutto:    
    app.MainLoop()
    
    
    