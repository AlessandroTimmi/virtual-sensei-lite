Virtual Sensei Lite� README
---------------------------

IMPORTANT:
OpenNI, NITE and SensorKinect modules are required to make the tracking
module work. The online help will guide you through the entire
installation process:
	
	* if this is your first installation of Virtual Sensei Lite�, go to
	http://www.virtualsensei.it/lite/help/#installation

	* if you are updating Virtual Sensei Lite� go to
	http://www.virtualsensei.it/lite/help/#update
	

On our website you can also find:
	* Video tutorials:
	http://www.virtualsensei.it/tutorials/

	* A contact form:
	http://www.virtualsensei.it/contact-us/

	* Our newsletter with the latest news:
	http://www.virtualsensei.it/subscribe/

Thanks for downloading Virtual Sensei Lite� and...
... Discover your kinetic energy, everywhere�.