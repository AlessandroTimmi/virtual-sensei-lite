Virtual Sensei Lite - End User License Agreement (EULA)

(C) 2011 Alessandro Timmi. All rights reserved.

Developers: Alessandro Timmi, Ettore Pennestri', Pier Paolo Valentini
and Andrea Pilia

Last Updated: September 15th, 2011

Please, read this carefully. By using all or any portion of the
Software VIRTUAL SENSEI LITE you accept all the terms and conditions
of this Agreement. If you do not agree, do not use or install this
Software.

1. DEFINITIONS
1.1. "WE" and "AUTHOR" shall refer to Alessandro Timmi and Virtual
Sensei.
1.2. "SOFTWARE" shall refer to products developed and provided by
Alessandro Timmi and Virtual Sensei, including the software Virtual
Sensei Lite, the software setup files, any update and addition, any
associated software component, any media, the software logos, the
copyright notice, the user's guide, any printed material and any
online or electronic documentation.
1.3. "USER" shall refer to you, the user (either as an individual,
single entity or group).

This End User License Agreement (EULA) is an agreement between you,
the USER and the mentioned AUTHOR. By installing, copying, or
otherwise using the mentioned SOFTWARE you agree to have read, fully
understand and agree to be bound by, the terms set out below.
If you do not agree to the terms of this EULA, do not install or use
the SOFTWARE.

2. LICENSE
2.1. Under the terms of this agreement, a non-exclusive (FREEWARE)
license is granted to the USER on the understanding the AUTHOR
retains ALL rights to the SOFTWARE.
2.2. You are free to download and/or use the SOFTWARE only for
personal use (not commercial) and only for legal purpose.
2.3. The SOFTWARE may be installed and used by the USER on any number
of systems. 
2.4. The USER may not modify, adapt, translate, reverse engineer,
decompile, disassemble, alter, merge or otherwise mis-use the
SOFTWARE.
2.5. The USER may not attempt to discover the source code of the
SOFTWARE.
2.6. The USER may not make derivative works based upon the SOFTWARE.
2.7. The USER may re-distribute the SOFTWARE, on the understanding no
attempt is made to sell or otherwise personally or financially gain
from the distribution of such SOFTWARE. When re-distributed, the
SOFTWARE must remain unchanged, including all the files described
in 1.2.
2.8. The USER may not "bundle" the SOFTWARE with any product and/or
service that has not been developed and/or provided by the AUTHOR.
2.9. The following trademarks are property of the AUTHOR: Virtual
Sensei, the Virtual Sensei logo, Virtual Sensei Lite, the Virtual
Sensei Lite logo, VSLITE, the VSLITE logo and "Discover your kinetic
energy".
2.10 The USER may publish (on the web, in scientific articles or
elsewhere) data and results processed using the SOFTWARE (both in
graphical or textual form). The only restrictions are:
(a) the USER must give credits to the software "Virtual Sensei Lite";
(b) the USER must link to Virtual Sensei Lite webpage when publishing
data on the web, adding the following link on EACH PAGE containing
the data. The HTML code for this link is:

Motions analysis performed using <a title="Virtual Sensei Lite 
homepage" href="http://www.virtualsensei.it/lite/" 
target="_blank">Virtual Sensei Lite</a>.


THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

If you have any questions, please visit the Virtual Sensei forums at
http://www.virtualsensei.it/forums/ or email info@virtualsensei.it.


########################################
ADDITIONAL LICENSES: Portions of the SOFTWARE are based on
source-code licensed under different licenses, as follows:
########################################

Icons by "Axialis Team" (http://www.axialis.com/free/icons),
licensed under the Creative Commons Attribution License
(http://creativecommons.org/licenses/by/2.5/) and modified by
Alessandro Timmi.

########################################

OpenNI 1.1 Alpha                                                         
Copyright (C) 2011 PrimeSense Ltd.                                       
                                                                           
This file is part of OpenNI.                                             
                                                                            
OpenNI is free software: you can redistribute it and/or modify           
it under the terms of the GNU Lesser General Public License as published  
by the Free Software Foundation, either version 3 of the License, or      
(at your option) any later version.                                       
                                                                             
OpenNI is distributed in the hope that it will be useful,                 
but WITHOUT ANY WARRANTY; without even the implied warranty of            
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the              
GNU Lesser General Public License for more details.                       
                                                                             
You should have received a copy of the GNU Lesser General Public License  
along with OpenNI. If not, see <http://www.gnu.org/licenses/>.  

########################################

Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010
Python Software Foundation; All Rights Reserved


PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2
--------------------------------------------

1. This LICENSE AGREEMENT is between the Python Software Foundation
("PSF"), and the Individual or Organization ("Licensee") accessing and
otherwise using this software ("Python") in source or binary form and
its associated documentation.

2. Subject to the terms and conditions of this License Agreement, PSF hereby
grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
analyze, test, perform and/or display publicly, prepare derivative works,
distribute, and otherwise use Python alone or in any derivative version,
provided, however, that PSF's License Agreement and PSF's notice of copyright,
i.e., "Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010
Python Software Foundation; All Rights Reserved" are retained in Python alone or
in any derivative version prepared by Licensee.

3. In the event Licensee prepares a derivative work that is based on
or incorporates Python or any part thereof, and wants to make
the derivative work available to others as provided herein, then
Licensee hereby agrees to include in any such work a brief summary of
the changes made to Python.

4. PSF is making Python available to Licensee on an "AS IS"
basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND
DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON WILL NOT
INFRINGE ANY THIRD PARTY RIGHTS.

5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON,
OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.

6. This License Agreement will automatically terminate upon a material
breach of its terms and conditions.

7. Nothing in this License Agreement shall be deemed to create any
relationship of agency, partnership, or joint venture between PSF and
Licensee.  This License Agreement does not grant permission to use PSF
trademarks or trade name in a trademark sense to endorse or promote
products or services of Licensee, or any third party.

8. By copying, installing or otherwise using Python, Licensee
agrees to be bound by the terms and conditions of this License
Agreement.

########################################

Copyright (c) 2002-2009 John D. Hunter; All Rights Reserved

License agreement for matplotlib 1.0.1
---------------------------------------

1. This LICENSE AGREEMENT is between John D. Hunter (�JDH�), and the Individual or Organization (�Licensee�) accessing and otherwise using matplotlib software in source or binary form and its associated documentation.

2. Subject to the terms and conditions of this License Agreement, JDH hereby grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce, analyze, test, perform and/or display publicly, prepare derivative works, distribute, and otherwise use matplotlib 1.0.1 alone or in any derivative version, provided, however, that JDH�s License Agreement and JDH�s notice of copyright, i.e., �Copyright (c) 2002-2009 John D. Hunter; All Rights Reserved� are retained in matplotlib 1.0.1 alone or in any derivative version prepared by Licensee.

3. In the event Licensee prepares a derivative work that is based on or incorporates matplotlib 1.0.1 or any part thereof, and wants to make the derivative work available to others as provided herein, then Licensee hereby agrees to include in any such work a brief summary of the changes made to matplotlib 1.0.1.

4. JDH is making matplotlib 1.0.1 available to Licensee on an �AS IS� basis. JDH MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED. BY WAY OF EXAMPLE, BUT NOT LIMITATION, JDH MAKES NO AND DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF MATPLOTLIB 1.0.1 WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.

5. JDH SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF MATPLOTLIB 1.0.1 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING MATPLOTLIB 1.0.1, OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.

6. This License Agreement will automatically terminate upon a material breach of its terms and conditions.

7. Nothing in this License Agreement shall be deemed to create any relationship of agency, partnership, or joint venture between JDH and Licensee. This License Agreement does not grant permission to use JDH trademarks or trade name in a trademark sense to endorse or promote products or services of Licensee, or any third party.

8. By copying, installing or otherwise using matplotlib 1.0.1, Licensee agrees to be bound by the terms and conditions of this License Agreement.

########################################

Copyright (c) 2001, 2002 Enthought, Inc.
All rights reserved.

Copyright (c) 2003-2009 SciPy Developers.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  a. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
  b. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  c. Neither the name of the Enthought nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

########################################

Copyright (c) 2005, NumPy Developers

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    Neither the name of the NumPy Developers nor the names of any contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS� AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

########################################

The Visual library is Copyright (c) 2000 by David Scherer.

                        All Rights Reserved

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of David Scherer not be 
used in advertising or publicity pertaining to distribution of the 
software without specific, written prior permission.

DAVID SCHERER DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN 
NO EVENT SHALL DAVID SCHERER BE LIABLE FOR ANY SPECIAL, INDIRECT OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.

The following notice applies to the CXX library, which is
linked into cvisual.dll:

*** Legal Notice for all LLNL-contributed files ***

Copyright (c) 1996. The Regents of the University of California. All
rights reserved.

Permission to use, copy, modify, and distribute this software for any
purpose without fee is hereby granted, provided that this entire
notice is included in all copies of any software which is or includes
a copy or modification of this software and in all copies of the
supporting documentation for such software.

This work was produced at the University of California, Lawrence
Livermore National Laboratory under contract no. W-7405-ENG-48 between
the U.S. Department of Energy and The Regents of the University of
California for the operation of UC LLNL.

DISCLAIMER
This software was prepared as an account of work sponsored by an
agency of the United States Government. Neither the United States
Government nor the University of California nor any of their
employees, makes any warranty, express or implied, or assumes any
liability or responsibility for the accuracy, completeness, or
usefulness of any information, apparatus, product, or process
disclosed, or represents that its use would not infringe
privately-owned rights. Reference herein to any specific commercial
products, process, or service by trade name, trademark, manufacturer,
or otherwise, does not necessarily constitute or imply its
endorsement, recommendation, or favoring by the United States
Government or the University of California. The views and opinions of
authors expressed herein do not necessarily state or reflect those of
the United States Government or the University of California, and
shall not be used for advertising or product endorsement purposes.
