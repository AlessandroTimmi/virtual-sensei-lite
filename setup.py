'''
Created on 26/giu/2011

@author: Alessandro Timmi
'''
# Per creare l'exe e tutti i file necessari ad eseguirlo occorre eseguire questo file da prompt di dos scrivendo:
# python path/setup.py py2exe


from distutils.core import setup
import py2exe #IGNORE:W0611
import matplotlib
from glob import glob # serve a richiamare file con i comandi tipo *.*
import os

import cfg

# The data_files takes a tuple 1) the location to store the data and 2) the location to copy the data from

myData_Files = []

## Aggiunge le DLL di visual C: 
## UPDATE: Rimpiazzate dal file vcredist.exe che viene installato manualmente dall'utente 
#myData_Files.append(("Microsoft.VC90.CRT",
#                   glob(r'C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\redist\x86\Microsoft.VC90.CRT\*.*')))

# Aggiunge dei file necessari a VPython, anche se non utilizzati da Virtal Sensei Lite:
myData_Files.append(('visual', glob(r'C:\Python27\Lib\site-packages\vis\*.tga')))

# Aggiunge tutti i file di supporto al programma aggiunti da me
# (licenza, readme, changes, icone, .dll, .exe, ecc.).
# Non aggiungo i file activation.json e config.json perche' vengono generati dinamicamente dal software.
# Essi non devono essere parte dell'archivio di setup, altrimenti verrebbero automaticamente 
# sovrascritti in caso di aggiornamento o rimossi in caso di disinstallazione.
# Inoltre devono trovarsi in una cartella con permessi di scrittura.
myData_Files.append(('',                        glob(r'D:\Tesi\VirtualSenseiLite\src' + os.sep + cfg.SW_LICENSE_FILE)))
myData_Files.append(('',                        glob(r'D:\Tesi\VirtualSenseiLite\src' + os.sep + cfg.SW_README_FILE)))
myData_Files.append(('',                        glob(r'D:\Tesi\VirtualSenseiLite\src' + os.sep + cfg.SW_CHANGES_FILE)))
myData_Files.append((cfg.SW_GRAPHICS_FOLDER,    glob(r'D:\Tesi\VirtualSenseiLite\src' + os.sep + cfg.SW_GRAPHICS_FOLDER + os.sep + '*.*')))
myData_Files.append((cfg.SW_MODULES_FOLDER,     glob(r'D:\Tesi\VirtualSenseiLite\src' + os.sep + cfg.SW_MODULES_FOLDER + os.sep + '*.*')))
myData_Files.append((cfg.SW_LEGAL_FOLDER,       glob(r'D:\Tesi\VirtualSenseiLite\src' + os.sep + cfg.SW_LEGAL_FOLDER + os.sep + '*.*')))

# Aggiunge dei file necessari per matplotlib: USARE EXTEND al posto di APPEND, altrimenti vengono delle parentesi
# quadre in piu' che danno errore in fase di compilazione.
# Extend the tuple list because matplotlib returns a tuple list. 
myData_Files.extend(matplotlib.get_py2exe_datafiles())

# purtroppo l'opzione bundle che permette di creare un unico file exe non sembra funzionare con
# i data_files (non li trova perche' li cerca nell'exe stesso mentre stanno in cartelle esterne).
# Per creare un file unico conviene allora creare un setup (con Inno o NSIS).
myOptions = {'py2exe':
             {
              'optimize':2,                                      # string or int of optimization level (toglie i commenti, sembra efficace solo a 2)
                                                                 # (0, 1, or 2) 0 = don't optimize (generate .pyc)
                                                                 # 1 = normal optimization (like python -O)
                                                                 # 2 = extra optimization (like python -OO)
                                                                 # See http://docs.python.org/distutils/apiref.html#module-distutils.util for more info. 
              'includes': 'matplotlib.backends.backend_wxagg',   # list of module names to include
              #'packages': [''],                                 # list of packages to include with subpackages
              #'ignores': [''],                                  # list of modules to ignore if they are not found  
              'excludes': ['_gtkagg', '_tkagg',                  # esclude cose importate automaticamente ma inutili   
                           "Tkconstants","Tkinter","tcl"],           
              'dll_excludes': ["MSVCP90.dll"],                   # esclude la DLL di MS Visual C++, che verra' poi installata manualmente dall'utente tramite pacchetto redistributable
              #'dist_dir': 'dist',                               # directory in which to build the final files 
              #'compressed': 1,                                  # (boolean) create a compressed zipfile 
              #'bundle_files': 1,                                # bundle dlls in the zipfile or the exe. Valid values for bundle_files are:
                                                                 # 3 = don't bundle (default)
                                                                 # 2 = bundle everything but the Python interpreter
                                                                 # 1 = bundle everything, including the Python interpreter
              'skip_archive':1,                                  # (boolean) do not place Python bytecode files in an archive, put them directly in the file system
              #'ascii':1,                                        # NON USARE, DA' PROBLEMI e FA GUADADGNARE SOLO 400 KB - (boolean) do not automatically include encodings and codecs              
              }
             }


myWindows = [
             {
              "script": cfg.SW_MAIN_SCRIPT,                            # nome del file principale (main) del programma da compilare
              "description": cfg.SW_DESCRIPTION,
              "icon_resources": [(0, 'icons'+os.sep+cfg.SW_ICON)],       # file icona del programma, visualizzato in explorer di windows ma non nella finestra (questa va impostata a parte)
              "copyright": cfg.SW_COPYRIGHT,
              "company_name": cfg.SW_COMPANY_NAME           
              }
             ]

# TODO: non si vedono autore e copyright nei details
setup(
    windows = myWindows,        # windows invece di console crea un file di testo come log invece della console
    name = cfg.SW_NAME,
    version = cfg.SW_VERSION,
    author = cfg.SW_AUTHOR,
    author_email = cfg.SW_EMAIL,
    url = cfg.SW_URL,
    description = cfg.SW_DESCRIPTION,
    #long_decription =,          
    download_url = cfg.SW_URL,
    #classifiers=,
    #platforms=,
    license = cfg.SW_COPYRIGHT+'. See the file '+cfg.SW_LICENSE_FILE+' for the software license',
    # icon_file = ,                    # non sembra necessario perche' l'icona viene caricata dal parametro "windows"
    options = myOptions,
    data_files = myData_Files,
    #zipfile = None      # None will bundle files in exe instead of zip file. non compatibile con 'skip_archive'
                        # non riesco a inserire i data files nell'exe
)