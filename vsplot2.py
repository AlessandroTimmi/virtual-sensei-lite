'''
Created on Feb 25, 2012

@author: Alex
'''
import numpy as np
import wx
import wx.aui
import matplotlib as mpl
mpl.use('wxagg') # sceglie il backend e permette di escludere gli altri dal setup
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as Canvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2Wx as Toolbar
# Per ancorare il logo di VSLite nei plot in maniera facile:
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText
import os

import cfg



class PlotFrame(wx.Frame):
    """Crea il frame dei plot.
    
    Attributi:
    Parent = (built-in) padre della classe;
    n_tabs= numero di tab esistenti.
    n_ax_per_tab = lista contenente il numero di axes per ogni tab
    canvases = lista contenente i canvas (uno per ogni tab);
    axes = lista multi-D contenente gli axes (una dimensione per ogni tab);
    cursors = lista multi-D contenente i cursori;     
    labels = lista multi-D contenente le label.
    spans = lista multi-D contenente gli span verticali usati per il cropping;
    backgrounds = lista multi-D contenente gli sfondi fissi dei plot.
    
    """
    def __init__(self, parent, id_num, title, frm, ke_tot, v_stroke, a_stroke):
        wx.Frame.__init__(self, parent, id_num, title=title,
                          size=cfg.PLOT_FRAME_DEFAULT_SIZE)
        #self.Centre()         
        #self.SetMinSize(cfg.VS_GUI_SIZE)    
        icon = wx.Icon(cfg.SW_GRAPHICS_FOLDER+os.sep+cfg.SW_ICON,
                       wx.BITMAP_TYPE_ICO)
        self.SetIcon(icon) 
        
        notebook = PlotNotebook(self)
        
        # Le tab sono sottoclassi di wx.Panel:
        tab1 = notebook.add_tab('Speeds')
        # TODO: ripristina plot accelerazioni dopo test con accelerometri:
#        tab2 = notebook.add_tab('Accelerations')
        
        # Inizialmente voglio che sia attiva la prima tab:
        cfg.active_tab = 0
        
        figure1 = tab1.figure
#        figure2 = tab2.figure
     
#        self.canvases = [tab1.canvas, tab2.canvas]
        self.canvases = [tab1.canvas]
        
        # numero di tab esistenti:
        self.n_tabs = len(self.canvases)
        self.backgrounds = self.empty_list_for_tabs()
        ######################################################################
        # Binding eventi:
        notebook.Bind(wx.aui.EVT_AUINOTEBOOK_PAGE_CHANGED,
                      self.on_page_changed)     
        
        for canvas in self.canvases:
            canvas.mpl_connect('resize_event',
                               self.adapt_fontsize_on_canvas_resize)
            canvas.mpl_connect('pick_event', self.on_pick_labels)

        ######################################################################
        # TAB 1: Grafici delle velocita'
        # Crea la griglia dei subplot
        gs1 = mpl.gridspec.GridSpec(3, 2)
        # Ne aggiorna gli spazi tra gli axes, per garantire sufficiente spazio
        # alle xlabel e ylabel:
        gs1.update(wspace=0.4, hspace=0.4)
        
        # Crea gli axes nella figure:
        ax_ke1 = figure1.add_subplot(gs1[0, :])
        ax_vlh = figure1.add_subplot(gs1[1, 0])
        ax_vrh = figure1.add_subplot(gs1[1, 1])
        ax_vlf = figure1.add_subplot(gs1[2, 0])
        ax_vrf = figure1.add_subplot(gs1[2, 1])
                
        # Imposta l'estetica dei plot:  
        ax_ke1.set_title('Total kinetic energy')
        ax_ke1.set_ylabel('J')
        #ax_ke1.set_xlabel('Time [cs]')
        
        ax_vlh.set_title('Left hand speed')
        ax_vlh.set_ylabel('m/s')
        #ax_vLH.set_xlabel('Time [cs]')
        
        ax_vrh.set_title('Right hand speed')
        #ax_vrh.set_ylabel('[m/s]')
        #ax_vrh.set_xlabel('Time [cs]')
            
        ax_vlf.set_title('Left foot speed')
        ax_vlf.set_ylabel('m/s')
        ax_vlf.set_xlabel('Time [cs]')
        
        ax_vrf.set_title('Right foot speed')
        #ax_vrf.set_ylabel('[m/s]')
        ax_vrf.set_xlabel('Time [cs]')
        
        # Traccia andamento energia cinetica totale dell'intero corpo umano:
        ax_ke1.plot(ke_tot)
        # Tracciamento andamento delle velocita' di mani e piedi:
        ax_vlh.plot(v_stroke[:, 0])
        ax_vrh.plot(v_stroke[:, 1])
        ax_vlf.plot(v_stroke[:, 2])
        ax_vrf.plot(v_stroke[:, 3])
        
        # Limite superiore y dei plot per averli tutti nella stessa scala:
        v_stroke_max = np.amax(self.Parent.results.v_stroke)
        
        ax_vlh.set_ylim(bottom=0, top=v_stroke_max)
        ax_vrh.set_ylim(bottom=0, top=v_stroke_max)
        ax_vlf.set_ylim(bottom=0, top=v_stroke_max)
        ax_vrf.set_ylim(bottom=0, top=v_stroke_max)
        
        # Cursori (la virgola serve, altrimenti restituisce una lista):
        c_ke1, = ax_ke1.plot(frm, ke_tot[frm], color='r', marker='v', markersize=10, animated=True)
        c_vlh, = ax_vlh.plot(frm, v_stroke[frm,0], color='r', marker='v', markersize=10, animated=True)
        c_vrh, = ax_vrh.plot(frm, v_stroke[frm,1], color='r', marker='v', markersize=10, animated=True)
        c_vlf, = ax_vlf.plot(frm, v_stroke[frm,2], color='r', marker='v', markersize=10, animated=True)
        c_vrf, = ax_vrf.plot(frm, v_stroke[frm,3], color='r', marker='v', markersize=10, animated=True)
            
        # Imposta alcune caratteristiche delle text label:
        bbox_props = dict(boxstyle="round,pad=0.3", facecolor=[1, 1, 0.5],
                              edgecolor="black", linewidth=1, alpha=0.7)
            
        l_ke1 = ax_ke1.text(frm, ke_tot[frm], 'x=%d\ny=%.3f'%(frm, ke_tot[frm]),
                            animated=True, bbox=bbox_props, ha='left',
                            va='bottom', picker=5, visible=cfg.is_labels_visible)
        l_vlh = ax_vlh.text(frm, v_stroke[frm, 0], 'x=%d\ny=%.3f'%(frm, v_stroke[frm, 0]),
                             animated=True, bbox=bbox_props, ha='left',
                             va='bottom', picker=5, visible=cfg.is_labels_visible)
        l_vrh = ax_vrh.text(frm, v_stroke[frm, 1], 'x=%d\ny=%.3f'%(frm, v_stroke[frm, 1]),
                             animated=True, bbox=bbox_props, ha='left',
                             va='bottom', picker=5, visible=cfg.is_labels_visible)
        l_vlf = ax_vlf.text(frm, v_stroke[frm, 2], 'x=%d\ny=%.3f'%(frm, v_stroke[frm, 2]),
                             animated=True, bbox=bbox_props, ha='left',
                             va='bottom', picker=5, visible=cfg.is_labels_visible)
        l_vrf = ax_vrf.text(frm, v_stroke[frm, 3], 'x=%d\ny=%.3f'%(frm, v_stroke[frm, 3]),
                             animated=True, bbox=bbox_props, ha='left',
                             va='bottom', picker=5, visible=cfg.is_labels_visible)
        
#        ######################################################################
#        # TAB 2: Grafici delle accelerazioni
#        # Crea la griglia dei subplot
#        gs2 = mpl.gridspec.GridSpec(3, 2)
#        # Ne aggiorna gli spazi tra gli axes, per garantire sufficiente spazio
#        # alle xlabel e ylabel:
#        gs2.update(wspace=0.4, hspace=0.4)
#        
#        # Crea gli axes nella figure:
#        ax_ke2 = figure2.add_subplot(gs2[0, :])
#        ax_alh = figure2.add_subplot(gs2[1, 0])
#        ax_arh = figure2.add_subplot(gs2[1, 1])
#        ax_alf = figure2.add_subplot(gs2[2, 0])
#        ax_arf = figure2.add_subplot(gs2[2, 1])
#               
#        ax_ke2.set_title('Total kinetic energy')
#        ax_ke2.set_ylabel('J')
#        #ax_ke2.set_xlabel('Time [cs]')
#        
#        ax_alh.set_title('Left hand accel.')
#        ax_alh.set_ylabel('m/s^2')
#        #ax_aLH.set_xlabel('Time [cs]')
#        
#        ax_arh.set_title('Right hand accel.')
#        #ax_arh.set_ylabel('[m/s]')
#        #ax_arh.set_xlabel('Time [cs]')
#            
#        ax_alf.set_title('Left foot accel.')
#        ax_alf.set_ylabel('m/s^2')
#        ax_alf.set_xlabel('Time [cs]')
#        
#        ax_arf.set_title('Right foot accel.')
#        #ax_arf.set_ylabel('[m/s]')
#        ax_arf.set_xlabel('Time [cs]')
#        
#        # Traccia andamento energia cinetica totale dell'intero corpo umano:
#        ax_ke2.plot(ke_tot)
#        # Tracciamento andamento delle accelerazioni di mani e piedi:
#        ax_alh.plot(a_stroke[:, 0])
#        ax_arh.plot(a_stroke[:, 1])
#        ax_alf.plot(a_stroke[:, 2])
#        ax_arf.plot(a_stroke[:, 3])
#        
#        # Limite superiore y dei plot per averli tutti nella stessa scala:
#        a_stroke_max = np.amax(self.Parent.results.a_stroke)
#        
#        ax_alh.set_ylim(bottom=0, top=a_stroke_max)
#        ax_arh.set_ylim(bottom=0, top=a_stroke_max)
#        ax_alf.set_ylim(bottom=0, top=a_stroke_max)
#        ax_arf.set_ylim(bottom=0, top=a_stroke_max)
#                
#        # Cursori (la virgola serve, altrimenti restituisce una lista):
#        c_ke2, = ax_ke2.plot(frm ,ke_tot[frm], color='r', marker='v', markersize=10, animated=True)
#        c_alh, = ax_alh.plot(frm, a_stroke[frm,0], color='r', marker='v', markersize=10, animated=True)
#        c_arh, = ax_arh.plot(frm, a_stroke[frm,1], color='r', marker='v', markersize=10, animated=True)
#        c_alf, = ax_alf.plot(frm, a_stroke[frm,2], color='r', marker='v', markersize=10, animated=True)
#        c_arf, = ax_arf.plot(frm, a_stroke[frm,3], color='r', marker='v', markersize=10, animated=True)
#        
#        
#        l_ke2 = ax_ke2.text(frm, ke_tot[frm], 'x=%d\ny=%.3f'%(frm, ke_tot[frm]),
#                            animated=True, bbox=bbox_props, ha='left',
#                            va='bottom', picker=5, visible=cfg.is_labels_visible)
#        l_alh = ax_alh.text(frm, a_stroke[frm, 0], 'x=%d\ny=%.3f'%(frm, a_stroke[frm, 0]),
#                            animated=True, bbox=bbox_props, ha='left',
#                            va='bottom', picker=5, visible=cfg.is_labels_visible)
#        l_arh = ax_arh.text(frm, a_stroke[frm, 1], 'x=%d\ny=%.3f'%(frm, a_stroke[frm, 1]),
#                             animated=True, bbox=bbox_props, ha='left',
#                             va='bottom', picker=5, visible=cfg.is_labels_visible)
#        l_alf = ax_alf.text(frm, a_stroke[frm, 2], 'x=%d\ny=%.3f'%(frm, a_stroke[frm, 2]),
#                             animated=True, bbox=bbox_props, ha='left',
#                             va='bottom', picker=5, visible=cfg.is_labels_visible)
#        l_arf = ax_arf.text(frm, a_stroke[frm, 3], 'x=%d\ny=%.3f'%(frm, a_stroke[frm, 3]),
#                             animated=True, bbox=bbox_props, ha='left',
#                             va='bottom', picker=5, visible=cfg.is_labels_visible)
        
        ######################################################################            
        # Logo e versione di Virtual Sensei Lite nei plot dell'en. cinetica.
        # Crea un oggetto AnchoredText con bordo (frame), ancorandolo in alto
        # a dx:
        logo_plot = AnchoredText(cfg.SW_NAME+' v.'+cfg.SW_VERSION,
                                prop=dict(size=8), frameon=False, loc=1,)
        # setta il bordo:
        #logoPlot.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
        # applica l'oggetto al grafico dell'energia cinetica:
        ax_ke1.add_artist(logo_plot)
#        ax_ke2.add_artist(logo_plot)
        
        ######################################################################   
        # Crea le liste bidimensionali accessibili dall'esterno:
        # Lista contenente i canvas (uno per ogni tab):
        
        
        
        
        # Lista contenente gli axes:
#        self.axes = [[ax_ke1, ax_vlh, ax_vrh, ax_vlf, ax_vrf],
#                     [ax_ke2, ax_alh, ax_arh, ax_alf, ax_arf]]
        self.axes = [[ax_ke1, ax_vlh, ax_vrh, ax_vlf, ax_vrf]]
        
        # Lista contenente il numero di axes per ogni tab:
        self.n_ax_per_tab = []
        for t in range(self.n_tabs):
            self.n_ax_per_tab.append(len(self.axes[t]))
        
        # Lista contenente i cursori:
#        self.cursors = [[c_ke1, c_vlh, c_vrh, c_vlf, c_vrf],
#                        [c_ke2, c_alh, c_arh, c_alf, c_arf]]
        self.cursors = [[c_ke1, c_vlh, c_vrh, c_vlf, c_vrf]]
        
        # Lista contenente le label:
#        self.labels = [[l_ke1, l_vlh, l_vrh, l_vlf, l_vrf],
#                       [l_ke2, l_alh, l_arh, l_alf, l_arf]]
        self.labels = [[l_ke1, l_vlh, l_vrh, l_vlf, l_vrf]]
        
        ######################################################################
        # Setta alcuni parametri:
        for tab in self.axes:
            for ax in tab:
                # Cicla su ogni singola label dell'asse x
                for label in ax.get_xticklabels():
                    label.set_rotation(30)
                    label.set_horizontalalignment('right')

        
        
        ######################################################################
        # Crea gli span verticali usati durante il cropping dell'acquisizione
        # e li aggiunge ad una lista bidimensionale: questo compito e'
        # delegato ad un metodo perche' occorre effettuarlo piu' volte:
        self.spans = self.empty_list_for_tabs()
        self.create_spans()
                
        ######################################################################
        # Prima di renderizzare le figure occorre mostrare la finestra, in
        # modo che il renderer conosca la grandezza degli axes in cui
        # disegnare. Mettendo il comando show alla fine, si otterrebbero
        # background nel buffer di dimensioni errate, perche' calcolati sulle
        # dimensioni di default della finestra di matplotlib.
        self.Show()

        # Solo dopo aver mostrato il frame posso impostare la dimensione
        # del font, che voglio sia dipendente dalla dimensione del canvas,
        # Tuttavia, poiche' quest'ulitma varia nell'atto di renderizzare
        # il frame col comando Show(), l'adattamento del font avviene
        # automaticamente tramite l'apposito evento.

        # Renderizza le figure e salva gli sfondi fissi nel buffer:
        self.draw_canvas_and_save_bgs()
        
    
    
    def on_page_changed(self, event):
        """Eseguita quando l'utente cambia tab.
        
        Cambia la variabile "tab attiva" e aggiorna background, cursori e label.
        """
        cfg.active_tab = event.GetSelection()
        # Traccia e salva i background dei plot della tab selezionata:
        self.draw_canvas_and_save_bgs()
        # Aggiorna la posizione di cursori e label nella tab attiva:
        # poiche' ad ogni fotogramma aggiorno solo la tab attiva,
        # al cambio tab devo aggiornare le posizioni di cursori e label,
        # non basta solo ritracciarli cosi' come sono.
        self.Parent.update_cursors_and_labels()
        
    
    def adapt_fontsize_on_canvas_resize(self, event):
        """Modifica dimensione font matplotlib in proporzione alla tab."""
        min_canvas_side =  min([event.width, event.height])

        mpl.rc('font', size=min_canvas_side / cfg.PLOT_FONT_DIVISOR)
        self.draw_canvas_and_save_bgs()
        self.restore_bgs_and_draw_artists()
        
    
    def on_pick_labels(self, event):
        """Quando l'utente clicca su una label, cicla fra 4 posizioni."""
        this_label = event.artist
        if this_label.get_ha() == 'left' and this_label.get_va() == 'bottom':
            this_label.set_va('top')
        elif this_label.get_ha() == 'left' and this_label.get_va() == 'top':
            this_label.set_ha('right')
        elif this_label.get_ha() == 'right' and this_label.get_va() == 'top':
            this_label.set_va('bottom')
        elif this_label.get_ha() == 'right' and this_label.get_va() == 'bottom':
            this_label.set_ha('left')
                
        self.restore_bgs_and_draw_artists()
        
    
    def empty_list_for_tabs(self):
        """Crea una lista con una sottolista vuota per ogni tab."""
        empty_list = [[] for t in range(self.n_tabs)]
        return empty_list
    
    
    def draw_canvas_and_save_bgs(self):
        """Traccia e salva la parte fissa dei plot nella tab attiva."""
        t = cfg.active_tab
        self.canvases[t].draw()
        
        # Elimina i background della tab attiva precedentemente salvati:
        self.backgrounds[t] = []
        
        for ax in range(self.n_ax_per_tab[t]):   # cicla fra i plot di ogni tab
            # Save the clean slate background -- everything but the animated artists
            # is drawn and saved in the pixel buffer background:
            self.backgrounds[t].append(self.canvases[t].copy_from_bbox(self.axes[t][ax].bbox))
                
                
    def restore_bgs_and_draw_artists(self):
        """Renderizza i background salvati e gli artist nella tab attiva."""
        t = cfg.active_tab
        for a in range(self.n_ax_per_tab[t]):
            # Restore the clean slate background:
            self.canvases[t].restore_region(self.backgrounds[t][a])
            # Just draw the animated artist:
            self.axes[t][a].draw_artist(self.cursors[t][a])
            self.axes[t][a].draw_artist(self.labels[t][a])
            self.axes[t][a].draw_artist(self.spans[t][a])
            
            # Just redraw the axes rectangle
            # (blit unisce il background nel buffer e gli artist animati):
            self.canvases[t].blit(self.axes[t][a].bbox)
            #canvas.blit(None)
    

    def create_spans(self):
        """Crea gli span usati durante il cropping dell'acquisizione.
        
        Viene richiamato all'apertura dell'acquisizione (quando
        vengono creati i grafici) e ogni volta che vengono spostati gli span
        da parte dell'utente.
        Poiche' non esiste una funzione per aggiornare l'estensione degli span,
        conviene ricrearli ogni volta che vengono spostati.
        
        """
        # Resetta la lista degli span:
        self.spans = self.empty_list_for_tabs()
        
        # Crea gli span e li salva nella lista:            
        for r in range(self.n_tabs):     # cicla fra le tab
            for c in range(len(self.axes[r])):   # cicla fra i plot di ogni tab
                self.spans[r].append(self.axes[r][c].axvspan(cfg.crop_start_frame,
                                                             cfg.crop_end_frame,
                                                             facecolor='g',
                                                             alpha=0.15,
                                                             animated=True,
                                                             visible=cfg.is_cropping))
                     
                
    def updt_curs_and_lab_in_tab(self, frm, ke_tot, v_stroke, a_stroke):
        """Aggiorna cursori e label nella tab attiva."""
        # TODO: sarebbe da unire la if, ma occorre unire v_stroke e a_stroke
        t = cfg.active_tab
        if t == 0:
            # Update cursor positions:
            self.cursors[t][0].set_data(frm, ke_tot)
            self.cursors[t][1].set_data(frm, v_stroke[0])
            self.cursors[t][2].set_data(frm, v_stroke[1])
            self.cursors[t][3].set_data(frm, v_stroke[2])
            self.cursors[t][4].set_data(frm, v_stroke[3])
            
            # Update text labels positions:
            self.labels[t][0].set_position([frm, ke_tot])
            self.labels[t][1].set_position([frm, v_stroke[0]])
            self.labels[t][2].set_position([frm, v_stroke[1]])
            self.labels[t][3].set_position([frm, v_stroke[2]])
            self.labels[t][4].set_position([frm, v_stroke[3]])
            
            # Update text labels strings:
            self.labels[t][0].set_text('x=%d\ny=%.3f'%(frm, ke_tot))
            self.labels[t][1].set_text('x=%d\ny=%.3f'%(frm, v_stroke[0]))
            self.labels[t][2].set_text('x=%d\ny=%.3f'%(frm, v_stroke[1]))
            self.labels[t][3].set_text('x=%d\ny=%.3f'%(frm, v_stroke[2]))
            self.labels[t][4].set_text('x=%d\ny=%.3f'%(frm, v_stroke[3]))
        
        elif t == 1:
            # Update cursor positions:
            self.cursors[t][0].set_data(frm, ke_tot)
            self.cursors[t][1].set_data(frm, a_stroke[0])
            self.cursors[t][2].set_data(frm, a_stroke[1])
            self.cursors[t][3].set_data(frm, a_stroke[2])
            self.cursors[t][4].set_data(frm, a_stroke[3])
            
            # Update text labels positions:
            self.labels[t][0].set_position([frm, ke_tot])
            self.labels[t][1].set_position([frm, a_stroke[0]])
            self.labels[t][2].set_position([frm, a_stroke[1]])
            self.labels[t][3].set_position([frm, a_stroke[2]])
            self.labels[t][4].set_position([frm, a_stroke[3]])
            
            # Update text labels strings:
            self.labels[t][0].set_text('x=%d\ny=%.3f'%(frm, ke_tot))
            self.labels[t][1].set_text('x=%d\ny=%.3f'%(frm, a_stroke[0]))
            self.labels[t][2].set_text('x=%d\ny=%.3f'%(frm, a_stroke[1]))
            self.labels[t][3].set_text('x=%d\ny=%.3f'%(frm, a_stroke[2]))
            self.labels[t][4].set_text('x=%d\ny=%.3f'%(frm, a_stroke[3]))
        
        # Renderizza i background salvati con sopra gli artist aggiornati:
        self.restore_bgs_and_draw_artists()
                  
                  




class PlotNotebook(wx.Panel):
    """Crea il panel di tipo AUI Notebook con schede."""
    def __init__(self, parent, id_num=wx.ID_ANY):
        wx.Panel.__init__(self, parent, id=id_num)
        self.nbook = wx.aui.AuiNotebook(parent=self,
                                        style=#wx.aui.AUI_NB_TAB_SPLIT |
                                        wx.aui.AUI_NB_TAB_MOVE |
                                        wx.aui.AUI_NB_WINDOWLIST_BUTTON |
                                        wx.aui.AUI_NB_TAB_EXTERNAL_MOVE)
        sizer = wx.BoxSizer()
        sizer.Add(self.nbook, 1, wx.EXPAND)
        self.SetSizer(sizer)
    
    def add_tab(self, name="plot"):
        """Aggiunge una tab al notebook dei plot."""
        tab = PlotTab(self.nbook)
        self.nbook.AddPage(tab, name)
        return tab


class PlotTab(wx.Panel):
    """Crea una tab contenente l'oggetto figure di matplotlib."""
    def __init__(self, parent, id_num=wx.ID_ANY, **kwargs):
        wx.Panel.__init__(self, parent, id=id_num, **kwargs)
        self.figure = mpl.figure.Figure()
        self.canvas = Canvas(parent=self, id=wx.ID_ANY, figure=self.figure)
        self.toolbar = VSToolbar(self.canvas)
        self.toolbar.Realize()
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.canvas, 1, wx.EXPAND)
        sizer.Add(self.toolbar, 0 , wx.LEFT | wx.EXPAND)
        self.SetSizer(sizer)


class VSToolbar(Toolbar):
    """Toolbar con pulsanti ed eventi customizzati.""" 
    def __init__(self, canvas):
        Toolbar.__init__(self, canvas)
        # Rimuovo i pulsanti inutili dalla toolbar:
        # back, forward e ridimensiona subplot:
        self.DeleteToolByPos(1)
        self.DeleteToolByPos(1) # senza senso
        self.DeleteToolByPos(4) # dovrebbe essere il 5, strano.
         
    def redraw_plots_blit(self):
        """Ritraccia i plot col metodo blit."""
        # Albero dei parent:
        # 1 parent = PlotTab;
        # 2 parent = AuiNotebook;
        # 3 parent = PlotNotebook;
        # 4 parent = PlotFrame.
        self.Parent.Parent.Parent.Parent.draw_canvas_and_save_bgs()
        self.Parent.Parent.Parent.Parent.restore_bgs_and_draw_artists()
   
    def home(self, *args):
        """L'utente preme il tasto home per resettare la vista dei plot.

        Ritraccia i grafici col solito metodo blit.
        
        """
        # Metodo originale:
        super(VSToolbar, self).home(*args)    
        # Aggiunta necessaria per animazione blit.
        self.redraw_plots_blit()
        
    def release_pan(self, event):
        """The release mouse button callback in pan/zoom mode.

        Ritraccia i grafici col solito metodo blit.
        
        """
        # Metodo originale:
        super(VSToolbar, self).release_pan(event) 
        # Aggiunta necessaria per animazione blit.
        self.redraw_plots_blit()

    def release_zoom(self, event):
        """The release mouse button callback in zoom to rect mode.

        Ritraccia i grafici col solito metodo blit.
        
        """
        # Metodo originale:
        super(VSToolbar, self).release_zoom(event) 
        # Aggiunta necessaria per animazione blit.
        self.redraw_plots_blit()










