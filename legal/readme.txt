Virtual Sensei Lite - User Tracker Viewer
Copyright (c) 2011 Alessandro Timmi.

The GNU LGPL license in this folder refers only to the module
"vsltrk.exe", not to the rest of the software Virtual Sensei Lite.
This module is a modified version of a sample provided by
the OPENNI libraries (licensed under the LGPL v.3).

You can find the modified source code of the module here:

http://www.virtualsensei.it/lite/src/vsltrk_src.rar

The license (LICENSE.txt) of the software Virtual Sensei Lite
is located in the main folder (root) of the installed software
or at the following url:

http://www.virtualsensei.it/lite/LICENSE.txt