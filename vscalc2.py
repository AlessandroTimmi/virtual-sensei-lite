'''
Created on 17/mag/2011

@author: Alessandro Timmi
'''

from __future__ import division
import numpy as np
# TODO: TOGLIERE, SERVE SOLO PER PLOT PROVA
#import matplotlib.pyplot as plt
from scipy import interpolate   
from scipy import signal
    
import anthrop
import cfg
  
class ResultsData():
    """Lettura coordinate e orientamenti da file ASCII dell'acquisizione.
    
    Attributi:
    good_frames_percentage = percentuale di frame buoni sul totale;
    n_frm_interp = numero di fotogrammi con framerate di interpolazione; 
    XYZj = coordinate XYZ dei vari giunti;
    v_stroke = valore assoluto delle velocita' di mani e piedi;
    a_stroke = valore assoluto delle accelerazioni di mani e piedi;
    ke_tot = energia cinetica totale;
    XYZGbody = coordinate del centro di massa (COM) del corpo umano;
    floor_data_interp = coordinate pavimento (framerare di interpolazione).
    
    """
    def __init__(self):
        # Vengono saltate le prime righe (header) che contengono le info su
        # atleta e tecnica eseguita. Inoltre viene creato un array masked che
        # contiene un True per ogni valore mancante dal file csv letto.
        # Questo permette in seguito di individuare ed ottenere per interpolazione
        # i valori mancanti. Al momento non serve per le coordinate XYZ, le cui
        # celle sono tutte piene, ma servira' in futuro quando verrano usate le
        # rotazioni.
        data = np.genfromtxt(cfg.mocap_file_play, delimiter=',',
                             skip_header=cfg.this_mocap_file_total_header_lines,
                             usemask=False)
        
        # Numero di frames costituenti l'acquisizione:
        n_frames_acquisition = data.shape[0]
        
        # Numero di colonne dell'array contenente i dati dell'acquisizione:
        n_cols_data = data.shape[1]
            
        # Numero di colonne del file ASCII relative ai giunti:
        n_cols_skel_data = cfg.N_JOINTS * cfg.N_PARAMETERS_PER_JOINT
            
        # Copia la parte dei dati relativa ai giunti:
        skel_data = data[:, 0:n_cols_skel_data].copy()
        
        # Copia la parte dei dati relativa all'individuazione del pavimento:
        floor_data=data[:, n_cols_skel_data:n_cols_skel_data+cfg.N_PARAMETERS_FLOOR].copy()
          
        # TODO: modificare questa parte in modo che legga l'indice di affidabilita' delle coordinate.  
        # Conteggio dei frame buoni e di quelli dedotti.
        # Lo scopo e' dare un indice di qualita' dell'acquisizione in base alla percentuale
        # di frame dedotti sul totale (buoni + dedotti):
        total_good_frames = 1.0
        total_missing_frames = 0.0
        # Calcolo della percentuale di frame buoni sul totale:
        self.good_frames_percentage = total_good_frames / (total_missing_frames + total_good_frames)
             
        # Array booleano contenente un True in corrispondenza dell'indice di ogni colonna
        # contenente le coordinate XYZ dei giunti ed un False in corrispondenza delle
        # colonne contenenti gli elementi delle matrici di rotazione.
        # Il contatore avanza di giunto in giunto e imposta le prime 3 colonne su True:
        cols_type = np.zeros(n_cols_skel_data, bool)
        for c in range(0, n_cols_skel_data, cfg.N_PARAMETERS_PER_JOINT):
            cols_type[c:c+3] = True
                
        # Poiche' per ottenere valori corretti in fase di derivazione (velocita', accelerazioni) 
        # occorre impostare la giusta scala temporale, bisogna convertire il tempo da fotogrammi
        # a secondi. Calcolo la durata dell'acquisizione in secondi:
        acq_duration = n_frames_acquisition * cfg.DT_CAPTURE
        
        # Lista dei frame in secondi con framerate di acquisizione: uso arange
        # perche' range funziona solo con step di tipo int:
        seconds_list_acquisition = np.arange(0, acq_duration, cfg.DT_CAPTURE)
        
        # Lista dei frame in secondi con framerate di interpolazione e riproduzione:
        # ATTENZIONE: si rischia di estrapolare creando poi picchi mostruosi negli array interpolati tramite spline!
        # L'array del tempo espresso in framerate di interpolazione va fermato all'ultimo valore temporale di quello
        # espresso con framerate di acquisizione: 
        #seconds_list_interpolation=np.arange(0,acq_duration,1/cfg.FRAMERATE_INTERPOLATION) # NO!
        seconds_list_interpolation = np.arange(0, seconds_list_acquisition[-1],
                                               1/cfg.FRAMERATE_INTERPOLATION)
        
        # Numero di frame in caso di interpolazione a 100 fps:
        self.n_frm_interp = seconds_list_interpolation.size
                  
        ####################################################################################################
        # FILTRAGGIO, INTERPOLAZIONE E DERIVAZIONE DELLE COORDINATE XYZ DEI GIUNTI
        # Si utilizza una rappresentazione tramite spline della generica coordinata nel tempo
        # e se ne calcolano le derivate, utilizzando un fattore di smoothing s proporzionale al
        # numero di frame dell'acquisizione.
        
        # Preallocazione degli array contenenti coordinate dei giunti e loro derivate nel tempo:
        # la dimensione degli array e' relativa al framerate di riproduzione:
        self.XYZj = np.zeros((self.n_frm_interp, 3*cfg.N_JOINTS))     # coordinate giunti
        XYZjp = np.zeros(self.XYZj.shape)                     # velocita' giunti
        XYZjpp = np.zeros(self.XYZj.shape)                    # accelerazioni giunti
           
        # Contatore delle colonne relative alle sole traslazioni dei giunti:
        c2 = 0
        for c1 in range(n_cols_skel_data):
            # Se la colonna corrente dei dati contiene una traslazione:
            if cols_type[c1]:
                # Per comodita' copia la colonna corrente dei dati in y:
                y = skel_data[:, c1].copy()
                              
                # Filtri:
                appo = signal.medfilt(y, cfg.MEDIAN_FILTER_SPAN)
                y_filt=savitzky_golay(appo, window_size=cfg.SGOLAY_SPAN,
                                      order=cfg.SGOLAY_ORD, deriv=0)
                     
                # Costruisce la spline: per default l'ordine della
                # spline e' 3 come consigliato nella documentazione (k=3).
                tck = interpolate.splrep(seconds_list_acquisition, y_filt,
                                         s=cfg.SMOOTH * n_frames_acquisition)
            
                # DA QUESTO PUNTO SI ABBANDONA IL FRAMERATE DELL'ACQUISIZIONE E SI
                # USA QUELLO DI RIPRODUZIONE.
                
                # Valuta la spline interpolante:
                self.XYZj[:, c2] = interpolate.splev(seconds_list_interpolation, tck,
                                                     der=0)
                # Valuta la derivata prima della spline:
                XYZjp[:, c2] = interpolate.splev(seconds_list_interpolation, tck,
                                                 der=1)
                # Valuta la derivata seconda della spline:
                XYZjpp[:, c2] = interpolate.splev(seconds_list_interpolation, tck,
                                                  der=2)
               
                
    #            # TODO: togliere grafico di prova   
    #            if  c2 == cfg.HEAD * 3 + 1:
    #                #provaFiltMedian = signal.medfilt(y)               
    #                plt.plot(seconds_list_acquisition, y)
    #                plt.plot(seconds_list_acquisition, prova_butter)
    #                plt.plot(seconds_list_acquisition, y_filt)
    #                #plt.plot(seconds_list_interpolation, XYZj[:,c2])
    #                #plt.plot(seconds_list_acquisition, provaFiltMedian)
    #                #plt.legend(('30fps', '30fps filt', '100fps filt', '30fps Median'), loc='best') 
    #                plt.legend(('30fps', '30fps butter', '30fps median+sgolay'), loc='best')
    #                plt.show()
                    
                # Aumenta di uno il contatore delle colonne relative alle sole traslazioni dei giunti: 
                c2 = c2 + 1
        
        ##########################################################################       
        # Modulo della velocita' totale di mani e piedi ad ogni frame;
        # questi valori assumono il significato di velocita' del colpo eseguito.
        # v_stroke(j)=[vLHand(j),vRHand(j),vLFoot(j),vRFoot(j)]
        self.v_stroke = np.zeros((self.n_frm_interp, 4))
        for f in range(self.n_frm_interp):
            self.v_stroke[f,0] = np.linalg.norm([XYZjp[f, 3 * cfg.L_HAND:3 * cfg.L_HAND + 3]])
            self.v_stroke[f,1] = np.linalg.norm([XYZjp[f, 3 * cfg.R_HAND:3 * cfg.R_HAND + 3]])
            self.v_stroke[f,2] = np.linalg.norm([XYZjp[f, 3 * cfg.L_FOOT:3 * cfg.L_FOOT + 3]])
            self.v_stroke[f,3] = np.linalg.norm([XYZjp[f, 3 * cfg.R_FOOT:3 * cfg.R_FOOT + 3]])
          
        ##########################################################################  
        # Modulo dell'accelerazione totale di mani e piedi ad ogni frame;
        # questi valori assumono il significato di accelerazione del colpo
        # eseguito.
        # a_stroke(j)=[a_lhand(j),v_rhand(j),a_lfoot(j),a_rfoot(j)]
        self.a_stroke = np.zeros((self.n_frm_interp, 4))
        for f in range(self.n_frm_interp):
            self.a_stroke[f,0] = np.linalg.norm([XYZjpp[f, 3 * cfg.L_HAND:3 * cfg.L_HAND + 3]])
            self.a_stroke[f,1] = np.linalg.norm([XYZjpp[f, 3 * cfg.R_HAND:3 * cfg.R_HAND + 3]])
            self.a_stroke[f,2] = np.linalg.norm([XYZjpp[f, 3 * cfg.L_FOOT:3 * cfg.L_FOOT + 3]])
            self.a_stroke[f,3] = np.linalg.norm([XYZjpp[f, 3 * cfg.R_FOOT:3 * cfg.R_FOOT + 3]])
            
            
        ##########################################################################
        # CALCOLO DELLE GRANDEZZE ANTROPOMETRICHE
        # Calcolo delle masse dei vari segmenti articolari:
        m = anthrop.calc_anthropometry(cfg.genderPlay, cfg.weightPlay,
                                       cfg.height_cmPlay)
        
        ##########################################################################
        # CALCOLO DELL'ENERGIA CINETICA
        # Attualmente viene calcolata solo l'energia cinetica traslazionale. 
        # Viene usata la velocita' dei giunti NITE e la massa dei vari segmenti
        # articolari ripartita tra i suddetti giunti.
        # Calcolo dell'energia cinetica traslazionale di ogni segmento articolare
        # ad ogni frame:
        ke_tr = np.zeros((self.n_frm_interp, cfg.N_JOINTS))
        for f in range(self.n_frm_interp):
            for j in range(cfg.N_JOINTS):
                # vettore velocita' del giunto j-esimo al frame f:
                rp = XYZjp[f, 3 * j:3 * j + 3].copy()
                # energia cinetica del giunto j-esimo al frame f:
                ke_tr[f, j] = 0.5 * m[j] * np.dot(rp, rp)
                   
        # Calcolo dell'energia cinetica totale dell'intero corpo umano ad ogni
        # frame, somma dei contributi dei vari segmenti articolari:   
        self.ke_tot = np.sum(ke_tr, axis=1)
    
        #############################################################################
        # CALCOLO della POSIZIONE DEL CENTRO DI MASSA (COM) dell'intero corpo umano
        # Preallocazione array 2d delle coordinate del baricentro del corpo umano:
        # XYZGbody = XGbody(1), YGbody(1), ZGbody(1);
        #            XGbody(2), YGbody(2), ZGbody(2);
        #            ...
        #            XGbody(nFrames), YGbody(nFrames), ZGbody(nFrames);
        self.XYZGbody = np.zeros((self.n_frm_interp, 3))
               
        # Calcolo delle coordinate del baricentro del corpo umano:
        # Centro di massa (baricentro) di un sistema di corpi (b va da 1 a nRB):
        # XGbody = sommatoria(m(b) * XG(b)) / sommatoria(m(b))   (similmente per YGbody e ZGbody)
        # Cicla prima sui frame:
        for f in range(self.n_frm_interp):
            # poi cicla sulla coordinata (X,Y,Z) di ogni corpo e del COM:
            for c in range(3):
                # Questo array contiene gli indici delle sole X, Y o Z in base al
                # contatore:
                coord_index = range(c, 3 * cfg.N_JOINTS, 3)     
                # prende una successione (con step 3) che parte dalla coordinata
                # corrente e arriva al numero di giunti.
                self.XYZGbody[f, c] = sum(m * self.XYZj[f, coord_index]) / sum(m)
        
        
        ##############################################################################
        # FILTRAGGIO E INTERPOLAZIONE DEI DATI DEL PAVIMENTO
        # Preallocazione array contenenti le coordinate di un punto appartenente al
        # pavimento e i coseni direttori della normale ad esso:
        self.floor_data_interp = np.zeros((self.n_frm_interp, cfg.N_PARAMETERS_FLOOR))
            
        for c in range(cfg.N_PARAMETERS_FLOOR):
            # Per comodita' copia la colonna corrente dei dati in y:
            y = floor_data[:, c].copy()
                          
            # Filtro median:
            y_filt = signal.medfilt(y, cfg.MEDIAN_FILTER_SPAN)
                         
            # Costruisce la spline: per default l'ordine della
            # spline e' 3 come consigliato nella documentazione (k=3):
            tck = interpolate.splrep(seconds_list_acquisition, y_filt,
                                     s=cfg.SMOOTH*n_frames_acquisition)
        
            # DA QUESTO PUNTO SI ABBANDONA IL FRAMERATE DELL'ACQUISIZIONE E SI USA QUELLO DI RIPRODUZIONE.
            
            # Valuta la spline interpolante:
            self.floor_data_interp[:, c] = interpolate.splev(seconds_list_interpolation,
                                                       tck, der=0)

    
     


def savitzky_golay(y, window_size, order, deriv=0):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techhniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv]
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m, y, mode='valid')

   
