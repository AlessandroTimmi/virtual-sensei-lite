'''
Created on 27/mag/2011

@author: Alessandro Timmi
'''
# TODO:
# verifica se e' meglio usare os.sep oppure os.path.join
#-imposta cartella di default per salvare i mocap file in documenti/virtual sensei lite;
#-TOGLI plugin email before download e anche download monitor? 
#-cerca di sistemare annotation sul 4 tutorial
#-pubblica filmati unlisted (dimostrazione 30 sec)
#-vedi se e' il caso di definire tutti gli attributi di self dentro l'init
#-Spostare classe crop dialog in vsutils?
#-span non prende grafico velocita' a sinistra?
#-creare un pulsante che apre i link per installazione openni
#-migliorare macro elaborazione intera cartella inserendo creazione cartelle (pdf, png, speed ecc)
# e controllo file esportato gia' esistente. Inoltre cercare di sfruttare le funzioni 
# di export gia' esistenti invece di riscrivere il codice.


# MEDIO TERMINE:
#-Kinect4Win: Studia questo esempio animazione: http://www.youtube.com/watch?feature=player_embedded&v=9u8d_BZ0cN0#!
#-risolvere problema resize plot: poiche' l'evento e' collegato ad ogni canvas,
# quando ridimensiono la finestra la relativa funzione viene richiamata tante volte
# quanti sono i canvas. Forse occorre usare l'evento di wx invece di mpl, ma sembra dare problemi.
#-resize plot solo a rilascio mouseclick per velocizzare resize.
#-mettere annotation su tecnica "best" 
#-aggiungi stesso header del mocap file anche ai file csv esportati.
#-metti spunta per attivare/disattivare grafici a piacere
#-Test con accelerometro per verifica accelerazioni;
#-ripristina plot e export accelerazioni dopo test con accelerometri;
#-salvataggio dei plot (pdf, png ecc) only_active_tab / all_tabs;
#-modifica c++ in modo che l'output sia in una dialog testuale apposita
# piani antropometrici locali come qui: http://smartlifting.org/2011/08/08/bang-project-piani-antropometrici-istantanei/
#-Problema in Subprocess: se si preme rec e l'exe non trova il kinect, occorre cmq
# premere stop. Stessa cosa, dopo aver premuto la x della finestra dell'exe, occorre comunque premere stop.
# Ho provato con Popen.wait() e funziona ma blocca tutta la GUI.
#-acquisizione senza opengl ma dati inviati in tempo reale a manichino 3d python
#-subprocess: mettere output vslitetracker.exe in log file invece di console
#-mettere nuova valutazione bonta' acquisizione, basata su affidabilita' fotogrammi
#-C++: far stampare affidabilita' vicino coordinate: se <0.5, il giunto diventa rosso durante riproduzione e il plot anche
#-nel comando export, convertire la data in modo che mostri il mese in lettere invece di numeri (Sept invece di 9)
#-ingrandire carattere font degli static text delle intestazioni delle due sezioni della gui
#-vedi skeleton 3dsmax http://www.reubenfleming.co.uk/tuts.html


# LUNGO TERMINE
#-funzione ghost usando non scheletro ma mappa 3d come in questo video http://youtu.be/5ltF3fkLv1U
#-aggiungi proiezione a terra baricentro: NON FATTIBILE perche' conosco solo la normale al pavimento (3 elementi della matrice di rotaz).
#-provare animazione tramite unity o panda3D;
#-Possibilita' di leggere file BVH
# aggiungi pulsante che apre multichoice per scegliere semitrasparenza link, da regolare con opacity di vpython
#-aggiungi possibilita' confronto 2 acquisizioni, con time shifting manuale per sincronizzare le tecniche.
#-rendi tutti i button trasparenti e quando li clicchi diventano blu per un attimo tramite apposita funzione
#-vedi per cattura video da kin
#-studia emipolari per sincronizzare 2 kin
#-validazione con accelerometro
#-avvia pratica partita iva
#
#-creare controlli tramite gesture
#-vedere per acquisizione + users


#-TESTING PER VENDIBILITA':
#-FASE 1) test nostro su kinect + VSLite: mettersi nei panni dell'utente;
#-FASE 2) prendere 10 utenti e farglielo provare da setup a utilizzo
# (prendere feedback su tutto: setup, comprensione e utilita' dell'output);
#-FASE 3) studio dei feedback e modifiche al programma;
#-Capire il tipo di utente tipo e capire le caratteristiche che cerca in questo programma


# Giornata test 1
#-schermata principale con modalita': esercitati, visualizza risultati, tutorial con i campioni, ranking mondiale
#-creare database internazionale coinvolgendo anche Jesse e altri atleti internazionali
#-rifare interfaccia in maniera + intuitiva;
#-in modalita' rec, mettere menu a cascata con tecniche preimpostate + acquisizione libera;
#-per ogni tecnica preimpostata, mostrare dialog (tips) con immagine e testo che spiegano come disporsi per acquisire;
#-convertire i file delle acquisizione in binario e permettere i download solo dei primi 10;
#-mettere possibilita' di segnalare acquisizioni inappropriate.
#-standardizzare lunghezza acquisizioni a 10 s;
#-riduci numero grafici visualizzati contemporaneamente per velocizzare riproduzione (es solo pugni o calci);



# Librerie standard:
import os
# libreria per eseguire i file .exe:
import subprocess
# libreria per formattare stringhe, usata per la funzione browse:
import textwrap
import urllib
import json
from datetime import datetime
import webbrowser
import glob

# Librerie di terze parti:
import wx
import  wx.lib.colourselect as csel
# Importa la libreria per la creazione di dialog con testo scorrevole:
from wx.lib import dialogs
from wx.lib.wordwrap import wordwrap

# Moduli di Virtual Sensei Lite:
import cfg
import vsutils
import vscalc2
import vsplot2
import vsanim



class VSMenuBar(wx.MenuBar):
    """Crea la menubar della GUI."""
    def __init__(self):
        wx.MenuBar.__init__(self)
                 
        # NOTA: SE si vuole aggiungere una bitmap ad un item del menu,
        # non basta usare la sintassi menu.append(id, label, message)
        # e poi fare item.SetBitmap(bmp), ma occorre creare manualmente
        # l'item col comando wx.MenuItem (vedi sotto).
                
        # Creazione dei vari menu:
        file_menu = wx.Menu()
        edit = wx.Menu()
        self.view = wx.Menu()
        help_menu = wx.Menu()
        social = wx.Menu()

        #----------------------------------------------
       
        # Creazione degli elementi del menu File:
        self.item_save_as = wx.MenuItem(file_menu, wx.ID_SAVEAS,
                                      'Save &As', 'Save new recording as')
        self.item_record = wx.MenuItem(file_menu, 102,
                                      '&Record', 'Record a new mocap file')
        self.item_stop = wx.MenuItem(file_menu, wx.ID_STOP,
                                    '&Stop', 'Stop recording')
        self.item_browse = wx.MenuItem(file_menu, 104,
                                      '&Browse', 'Browse mocap files')
        self.item_open = wx.MenuItem(file_menu, wx.ID_OPEN,
                                    '&Open', 'Open the selected mocap file')
        self.item_open_folder = wx.MenuItem(file_menu, 105,
                                    'Open &folder',
                                    'Open and process an entire folder')
        self.item_close = wx.MenuItem(file_menu, wx.ID_CLOSE,
                                     '&Close',
                                     'Close the currently loaded mocap file')
        self.item_quit = wx.MenuItem(file_menu, wx.ID_EXIT,
                                    '&Exit', 'Terminate the program')

        # Creazione delle icone del menu file:
        pic_item_save_as = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'saveAs16.png',
                                    wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_record = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'capture16.png',
                                   wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_stop = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'stop16.png',
                                 wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_browse = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'search16.png',
                                   wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_open = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'open16.png',
                                 wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_close = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'close16.png',
                                  wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_quit = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'quit16.png',
                                 wx.BITMAP_TYPE_PNG).ConvertToBitmap()
       
        # Aggiunta delle icone al menu file:
        self.item_save_as.SetBitmaps(pic_item_save_as)
        self.item_record.SetBitmap(pic_item_record)
        self.item_stop.SetBitmaps(pic_item_stop)
        self.item_browse.SetBitmaps(pic_item_browse)
        self.item_open.SetBitmaps(pic_item_open)
        self.item_close.SetBitmaps(pic_item_close)
        self.item_quit.SetBitmaps(pic_item_quit)
        
        # Aggiunta degli item al menu file:
        file_menu.AppendItem(self.item_save_as)
        file_menu.AppendItem(self.item_record)
        file_menu.AppendItem(self.item_stop)
        file_menu.AppendSeparator()
        file_menu.AppendItem(self.item_browse)
        file_menu.AppendItem(self.item_open)
        file_menu.AppendItem(self.item_open_folder)
        file_menu.AppendItem(self.item_close)
        file_menu.AppendSeparator()
        file_menu.AppendItem(self.item_quit)   
        
        #-------------------------------------------------
        
        # Creazione degli elementi del menu Edit.
        # Per gli ITEM_CHECK occorre fornire anche un nome da salvare in "self",
        # per consentire successivamente di richiamarli e modificarne lo stato
        # di selezione:
        # NOTA: gli ITEM CHECK non possono avere un'icona.
        self.check_labels = wx.MenuItem(edit, 201, 'Plot &labels',
                                        'Show/hide plot labels', wx.ITEM_CHECK)
        self.item_export = wx.MenuItem(edit, 202, 'E&xport plots/data',
                                       'Export plots/data')
        self.item_trails = wx.MenuItem(edit, 203, '&Joints &trails',
                                      'Show/hide joints trails')
        self.check_com = wx.MenuItem(edit, 204, '&COM',
                                     'Show/hide body center of mass',
                                    wx.ITEM_CHECK)
        self.check_loop = wx.MenuItem(edit, 205, '&Loop',
                                      'Enable/disable loop mode',
                                      wx.ITEM_CHECK)
        self.item_crop = wx.MenuItem(edit, 206, '&Crop',
                                     'Crop currently loaded mocap file')
        self.item_preferences = wx.MenuItem(edit, 207, '&Preferences',
                                            'Edit program preferences')
        
        # Creazione delle icone del menu Edit:
        pic_item_export = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep +
                                   'export16.png',
                                   wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_trails = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep +
                                   'iconTrail16.png', 
                                   wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_crop = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep +
                                 'crop16.png',
                                 wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_preferences = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep +
                                        'preferences16.png',
                                        wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        
        # Agginta delle icone al menu Edit:
        self.item_export.SetBitmap(pic_item_export)
        self.item_trails.SetBitmaps(pic_item_trails)
        self.item_crop.SetBitmaps(pic_item_crop)
        self.item_preferences.SetBitmaps(pic_item_preferences)
        
        # Aggiunta degli item al menu Edit:
        edit.AppendItem(self.check_labels)
        edit.AppendItem(self.item_export)
        edit.AppendItem(self.item_trails)
        edit.AppendItem(self.check_com)
        edit.AppendItem(self.check_loop)
        edit.AppendItem(self.item_crop)
        edit.AppendSeparator()
        edit.AppendItem(self.item_preferences)      
        
        #---------------------------------------------------
        
        # Aggiunta degli elementi al menu View.
        # In questo menu non servono le icone quindi posso usare il metodo
        # rapido per creare gli item:
        self.item_cam_xp = self.view.Append(301, 'Cam +X', 'Activate Cam +X',
                                            wx.ITEM_RADIO)
        self.item_cam_xm = self.view.Append(302, 'Cam -X', 'Activate Cam -X',
                                            wx.ITEM_RADIO)
        self.item_cam_yp = self.view.Append(303, 'Cam +Y', 'Activate Cam +Y',
                                            wx.ITEM_RADIO)
        self.item_cam_ym = self.view.Append(304, 'Cam -Y', 'Activate Cam -Y',
                                            wx.ITEM_RADIO)
        self.item_cam_zp = self.view.Append(305, 'Cam +Z', 'Activate Cam +Z',
                                            wx.ITEM_RADIO)
        self.item_cam_zm = self.view.Append(306, 'Cam -Z', 'Activate Cam -Z',
                                            wx.ITEM_RADIO)
        self.view.AppendSeparator()
        self.check_floor_transp = self.view.Append(307, '&Transparent floor',
                                                   'Make floor transparent', wx.ITEM_CHECK)
              
        #----------------------------------------------------
        
        # Creazione degli elementi del menu Help:
        item_tutorials = wx.MenuItem(help_menu, 400, '&Video tutorials',
                                     'Watch online video tutorials')
        item_guide = wx.MenuItem(help_menu, wx.ID_HELP, '&Guide',
                                 'Open program online help')
        item_check_updates = wx.MenuItem(help_menu, 402, '&Check for updates',
                                         'Check for updates')
        item_downloads = wx.MenuItem(help_menu, 403, '&Downloads',
                                     'Virtual Sensei downloads page')
        item_changelog = wx.MenuItem(help_menu, 404, '&Changelog',
                                     'Virtual Sensei changelog page')
        item_license = wx.MenuItem(help_menu, 405, '&License',
                                   'View software license')
        item_about = wx.MenuItem(help_menu, wx.ID_ABOUT, '&About',
                                 'See credits and license')
        
        # Creazione delle icone del menu Help:
        pic_item_tutorials = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'tutorials16.png',
                                      wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_guide = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'guide16.png',
                                  wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_check_updates = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'updates16.png',
                                          wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_downloads = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'down16.png',
                                      wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_changelog = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'changelog16.png',
                                      wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_license = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'license16.png',
                                    wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_about = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'about16.png',
                                  wx.BITMAP_TYPE_PNG).ConvertToBitmap()
       
        # Agginta delle icone al menu Help:
        item_tutorials.SetBitmaps(pic_item_tutorials)
        item_guide.SetBitmaps(pic_item_guide)
        item_check_updates.SetBitmaps(pic_item_check_updates)
        item_downloads.SetBitmaps(pic_item_downloads)
        item_changelog.SetBitmaps(pic_item_changelog)
        item_license.SetBitmaps(pic_item_license)
        item_about.SetBitmaps(pic_item_about)
        
        # Aggiunta degli item al menu Help:
        help_menu.AppendItem(item_tutorials)
        help_menu.AppendItem(item_guide)
        help_menu.AppendSeparator()
        help_menu.AppendItem(item_check_updates)
        help_menu.AppendItem(item_downloads)
        help_menu.AppendItem(item_changelog)
        help_menu.AppendItem(item_license)
        help_menu.AppendSeparator()
        help_menu.AppendItem(item_about)     
        
        #-------------------------------------------------------
        
        # Creazione degli elementi del menu Social:
        item_website = wx.MenuItem(help_menu, 500, '&Website',
                                  'Go to Virtual Sensei Lite website')
        item_facebook = wx.MenuItem(help_menu, 501, '&Facebook Page',
                                   'Go to Virtual Sensei Facebook Page')
        item_youtube = wx.MenuItem(help_menu, 502, '&Youtube channel',
                                  'Go to Virtual Sensei Youtube channel')
        item_twitter = wx.MenuItem(help_menu, 503, '&Twitter',
                                  'Follow Virtual Sensei on Twitter')
        item_gplus = wx.MenuItem(help_menu, 504, '&Google+',
                                'Follow Virtual Sensei on Google+')
        
        # Creazione delle icone del menu Social:
        pic_item_website = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'website16.png',
                                  wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_facebook = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'FB16.gif',
                                   wx.BITMAP_TYPE_GIF).ConvertToBitmap()
        pic_item_youtube = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'youtube16.png',
                                  wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_twitter = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'twitter16.png',
                                  wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        pic_item_gplus = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'gplus16.png',
                                wx.BITMAP_TYPE_PNG).ConvertToBitmap()
       
        # Agginta delle icone al menu Social:
        item_website.SetBitmaps(pic_item_website)
        item_facebook.SetBitmap(pic_item_facebook)
        item_youtube.SetBitmaps(pic_item_youtube)
        item_twitter.SetBitmap(pic_item_twitter)
        item_gplus.SetBitmaps(pic_item_gplus)
        
        # Aggiunta degli item al menu Social:
        social.AppendItem(item_website)
        social.AppendItem(item_facebook)
        social.AppendItem(item_youtube)
        social.AppendItem(item_twitter)
        social.AppendItem(item_gplus)       
       
        #----------------------------------------------------------   
                      
        # Aggiunta dei menu alla menubar
        self.Append(file_menu, '&File')
        self.Append(edit, '&Edit')
        self.Append(self.view, '&View')
        self.Append(help_menu, '&Help')
        self.Append(social, '&Social')
        
         
class VSFrame(wx.Frame):
    """GUI di Virtual Sensei Lite.
    
    Attributes:
    activation_dict = contains the activation data;
    config_dict = contains the program settings;
    ...
    
    """
    def __init__(self, parent, id_num, title):
        
        # Inizializzazione della finestra della GUI:
        wx.Frame.__init__(self, parent, id_num, title=title,
                          size=cfg.VS_GUI_SIZE)
        
        # Se non esiste gia', crea una cartella che sia accessibile al
        # software in scrittura, per contenere alcuni dati del programma
        if not os.path.exists(cfg.SW_DATA_FOLDER):
            os.makedirs(cfg.SW_DATA_FOLDER)
            
        # Crea l'activation_dict, inizialmente vuoto:
        self.activation_dict = {}    
        
        # Crea il config_dict: se gia' esiste un file di configurazione,
        # lo legge e lo salva nel dict
        if os.path.isfile(cfg.CONFIG_FILE_NAME):
            self.config_dict = vsutils.json2dict(cfg.CONFIG_FILE_NAME, parent=self)
        else:
            # altrimenti crea un dict cone le impostazioni di default:
            self.config_dict = cfg.default_config_dict.copy()
                            
        # Centra la GUI nello schermo
        self.Centre()   
                
        # Imposta le dimensioni minime della GUI, pari a quelle iniziali
        self.SetMinSize(cfg.VS_GUI_SIZE)    
        
        # Icona del programma
        icon = wx.Icon(cfg.SW_GRAPHICS_FOLDER+os.sep+cfg.SW_ICON,
                       wx.BITMAP_TYPE_ICO)
        self.SetIcon(icon)             

        # Creazione pannello interno alla finestra
        self.panel = wx.Panel(self)
               
        # Crea la menubar dalla classe appositamente customizzata
        self.menubar = VSMenuBar()
        
        # Assegna la menubar creata alla GUI
        self.SetMenuBar(self.menubar)   
        
        #############################################################################
        # CREAZIONE WIDGET SEZIONE "REC" DELLA GUI
        # Questa sezione della GUI gestisce l'eseguibile responsabile della registrazione
        # di un nuovo mocap file.
        # Etichette delle info che verranno scritte nel mocap file:
        st_label_section_rec = wx.StaticText(self.panel,
                                        label=cfg.LABEL_RECORD_NEW_MOCAP_FILE + ":")
        st_label_mocap_file_rec = wx.StaticText(self.panel,
                                          label=cfg.LABEL_SAVE_NEW_RECORDING_AS + ":")
        st_label_athlete_rec = wx.StaticText(self.panel,
                                        label=cfg.LABEL_ATHLETE_NAME + ":")
        st_label_technique_rec = wx.StaticText(self.panel,
                                          label=cfg.LABEL_TECHNIQUE_NAME + ":")
        st_label_weight_rec = wx.StaticText(self.panel,
                                       label=cfg.LABEL_ATHLETE_WEIGHT + ":")
        st_label_height_cm_rec = wx.StaticText(self.panel,
                                          label=cfg.LABEL_ATHLETE_HEIGHT + ":")
        st_label_gender_rec = wx.StaticText(self.panel,
                                       label=cfg.LABEL_ATHLETE_GENDER + ":")
        st_label_tilt = wx.StaticText(self.panel,
                                  label=cfg.LABEL_TILT + ":")
        

        # Save as button:
        self.button_save_as = wx.Button(self.panel, wx.ID_SAVEAS)
        self.button_save_as.SetToolTip(wx.ToolTip(cfg.TOOLTIP_SAVE_AS))

        # Mocap file path+name
        # L'utente puo' scrivere manualmente il nome o aprire una dialog per la scelta della destinazione di salvataggio:
        self.text_mocap_file_rec = wx.TextCtrl(self.panel, wx.ID_ANY, "", style=wx.TE_MULTILINE)
        
        # Athlete's name:
        self.textAthleteRec = wx.TextCtrl(self.panel, wx.ID_ANY, cfg.athleteRec)
        
        # Technique:
        self.textTechniqueRec = wx.TextCtrl(self.panel, wx.ID_ANY, cfg.techniqueRec)
        
        # Weight [kg]
        self.spinWeightRec = wx.SpinCtrl(self.panel, wx.ID_ANY, "")
        # Si vuole che la variabile sia float per evitare problemi di divisione per zero.
        self.spinWeightRec.SetRange(30.0, 500.0)
        # Imposta il valore di default del peso all'avvio:
        self.spinWeightRec.SetValue(cfg.weightRec)
        
        # Height [cm]
        self.spinHeight_cmRec = wx.SpinCtrl(self.panel, wx.ID_ANY, "")
        # Si vuole che la variabile sia float per evitare problemi di divisione per zero.
        self.spinHeight_cmRec.SetRange(70.0, 300.0)
        # Imposta il valore di default del peso all'avvio:
        self.spinHeight_cmRec.SetValue(cfg.height_cmRec)
        
        # Sesso (male/female)
        # Lista delle possibili scelte:
        self.genderList = ['m', 'f']
        self.choiceGenderRec = wx.Choice(self.panel, wx.ID_ANY, choices = self.genderList)
                
        # Rec button:
        picRec = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'capture.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picRecDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'captureDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.button_rec = wx.BitmapButton(self.panel, wx.ID_ANY, picRec)
        self.button_rec.SetBitmapDisabled(picRecDisabled)
        self.button_rec.SetToolTip(wx.ToolTip(cfg.TOOLTIP_REC))
        
        # Stop button:
        picStop = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'stop.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picStopDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'stopDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonStop = wx.BitmapButton(self.panel, wx.ID_STOP, picStop)
        self.buttonStop.SetBitmapDisabled(picStopDisabled)
        self.buttonStop.SetToolTip(wx.ToolTip(cfg.TOOLTIP_STOP))
                      
        # Spin ctrl che regola il tilt del Kinect:
        self.spinTilt = wx.SpinCtrl(self.panel, wx.ID_ANY, str(cfg.KINECT_ZERO_TILT_DEG))
        self.spinTilt.SetRange(cfg.KINECT_LOWER_TILT_LIMIT_DEG,cfg.KINECT_UPPER_TILT_LIMIT_DEG)
              
        #############################################################################
        # CREAZIONE WIDGET SEZIONE "PLAY" DELLA GUI
        # Etichette delle info lette dal mocap file:
        stLabelSectionPlay = wx.StaticText(self.panel, label=cfg.LABEL_PLAY_RECORDED_MOCAP_FILE + ":")
        stLabelMocapFilePlay = wx.StaticText(self.panel, label=cfg.LABEL_CHOOSE_MOCAP_FILE + ":")
        stLabelAthletePlay = wx.StaticText(self.panel, label=cfg.LABEL_ATHLETE_NAME + ":")
        stLabelTechniquePlay = wx.StaticText(self.panel, label=cfg.LABEL_TECHNIQUE_NAME + ":")
        stLabelWeightPlay = wx.StaticText(self.panel, label=cfg.LABEL_ATHLETE_WEIGHT + ":")
        stLabelHeight_cmPlay = wx.StaticText(self.panel, label=cfg.LABEL_ATHLETE_HEIGHT + ":")
        stLabelGenderPlay = wx.StaticText(self.panel, label=cfg.LABEL_ATHLETE_GENDER + ":")
        
        # Browse button
        self.buttonBrowse = wx.Button(self.panel, label=cfg.LABEL_BROWSE, size=(-1, -1))
        self.buttonBrowse.SetToolTip(wx.ToolTip(cfg.TOOLTIP_BROWSE))
        
        # Info lette dal mocap file:
        # Mocap file da riprodurre (path+name):
        self.stMocapFilePlay = wx.StaticText(self.panel, wx.ID_ANY, cfg.mocap_file_play)
        # Athlete's name:
        self.stAthletePlay = wx.StaticText(self.panel, wx.ID_ANY, cfg.athletePlay)
        # Technique:
        self.stTechniquePlay = wx.StaticText(self.panel, wx.ID_ANY, cfg.techniquePlay)
        # Weight [kg]:
        self.stWeightPlay = wx.StaticText(self.panel, wx.ID_ANY, cfg.weightPlay)
        # Height [cm]:
        self.stHeight_cmPlay = wx.StaticText(self.panel, wx.ID_ANY, cfg.height_cmPlay)
        # Sesso (male/female):
        self.stGenderPlay = wx.StaticText(self.panel, wx.ID_ANY, cfg.genderPlay)
        
        # Open button
        self.buttonOpen = wx.Button(self.panel, wx.ID_OPEN)
        self.buttonOpen.SetToolTip(wx.ToolTip(cfg.TOOLTIP_OPEN))
        
        # Close button
        self.buttonClose = wx.Button(self.panel, wx.ID_CLOSE)
        self.buttonClose.SetToolTip(wx.ToolTip(cfg.TOOLTIP_CLOSE))    
         
        # Display/hide text labels on plots
        picLabels = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'labels.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picLabelsDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'labelsDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonLabels = wx.BitmapButton(self.panel, wx.ID_ANY, picLabels)
        self.buttonLabels.SetBitmapDisabled(picLabelsDisabled)
        # Per far comportare questo button come un toggle button, la bitmap e' trasparente
        # e il colore viene cambiato in base allo stato di selezione:
        self.buttonLabels.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)
        self.buttonLabels.SetToolTip(wx.ToolTip(cfg.TOOLTIP_LABELS))
        
        # Export plots (.pdf, .eps, .png), kinetic energy and speed (.csv):
        picExport = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'export.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picExportDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'exportDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonExport = wx.BitmapButton(self.panel, wx.ID_ANY, picExport)
        self.buttonExport.SetBitmapDisabled(picExportDisabled)
        self.buttonExport.SetToolTip(wx.ToolTip(cfg.TOOLTIP_EXPORT))
        
        # Trails: open multiChoice dialog:
        picTrails = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'iconTrail.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picTrailsDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'iconTrailDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonTrails = wx.BitmapButton(self.panel, wx.ID_ANY, picTrails)
        self.buttonTrails.SetBitmapDisabled(picTrailsDisabled)
        self.buttonTrails.SetToolTip(wx.ToolTip(cfg.TOOLTIP_TRAILS))
        
        # COM: show/hide the body center of mass:
        picCOM = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'COM.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picCOMDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER+os.sep+'COMDisabled.png',wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonCOM = wx.BitmapButton(self.panel, wx.ID_ANY, picCOM)
        self.buttonCOM.SetBitmapDisabled(picCOMDisabled)
        # Per far comportare questo button come un toggle button, la bitmap e' trasparente
        # e il colore viene cambiato in base allo stato di selezione:
        self.buttonCOM.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)
        self.buttonCOM.SetToolTip(wx.ToolTip(cfg.TOOLTIP_COM))
        
        # Loop:
        picLoop = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'loop.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picLoopDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'loopDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonLoop = wx.BitmapButton(self.panel, wx.ID_ANY, picLoop)
        self.buttonLoop.SetBitmapDisabled(picLoopDisabled)
        # Per far comportare questo button come un toggle button, la bitmap e' trasparente
        # e il colore viene cambiato in base allo stato di selezione:
        self.buttonLoop.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)
        self.buttonLoop.SetToolTip(wx.ToolTip(cfg.TOOLTIP_LOOP))
        
        # Crop mocap file:
        picCrop = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'crop.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picCropDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'cropDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.button_crop = wx.BitmapButton(self.panel, wx.ID_ANY, picCrop)
        self.button_crop.SetBitmapDisabled(picCropDisabled)
        self.button_crop.SetToolTip(wx.ToolTip(cfg.TOOLTIP_CROP))
                
        # Static text "Current frame":
        self.st_cur_frame = wx.StaticText(self.panel, wx.ID_ANY, 'Current frame: ')
        # Spin ctrl che visualizza il frame corrente e permette di modificarlo:
        self.spin_goto = wx.SpinCtrl(self.panel, wx.ID_ANY, "0", size=(55, -1))
        # Static text with the end frame of the currently loaded mocap file:
        self.st_end_frame = wx.StaticText(self.panel, wx.ID_ANY, ' / 0')
        
        # Static text "Play speed":
        self.playSpeedText = wx.StaticText(self.panel, wx.ID_ANY, 'Play speed [%]: ')
        # Spin ctrl "Play speed":
        self.spinPlaySpeed = wx.SpinCtrl(self.panel, wx.ID_ANY, "", size=(50, -1))
        # Si vuole che la variabile sia float per evitare problemi di divisione per zero.
        self.spinPlaySpeed.SetRange(10, 200)
        # Imposta il valore di default della velocita' di riproduzione all'avvio:
        self.spinPlaySpeed.SetValue(cfg.play_speed_percentage)
                           
        # Go to start
        picStart = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'start.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picStartDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'startDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonStart = wx.BitmapButton(self.panel, wx.ID_ANY, picStart)
        self.buttonStart.SetBitmapDisabled(picStartDisabled)
        
        # Step backward
        picStepB = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'stepB.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picStepBDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'stepBDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonStepB = wx.BitmapButton(self.panel,wx.ID_ANY,picStepB)
        self.buttonStepB.SetBitmapDisabled(picStepBDisabled)
        
        # Play
        picPlay = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'play.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picPlayDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'playDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonPlay = wx.BitmapButton(self.panel, wx.ID_ANY, picPlay)
        self.buttonPlay.SetBitmapDisabled(picPlayDisabled)
        #self.buttonPlay.setDefault()  #???
        # Per far comportare questo button come un toggle button, la bitmap e' trasparente
        # e il colore viene cambiato in base allo stato di selezione:
        self.buttonPlay.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)
                            
        # Step forward
        picStepF = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'stepF.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picStepFDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'stepFDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonStepF = wx.BitmapButton(self.panel, wx.ID_ANY, picStepF)
        self.buttonStepF.SetBitmapDisabled(picStepFDisabled)
                   
        # Go to end
        picEnd = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'end.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        picEndDisabled = wx.Image(cfg.SW_GRAPHICS_FOLDER + os.sep + 'endDisabled.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.buttonEnd = wx.BitmapButton(self.panel, wx.ID_ANY, picEnd)
        self.buttonEnd.SetBitmapDisabled(picEndDisabled)
                        
        # sliderSeek del tempo con fondoscala e step di default: il fondoscala e lo step reale
        # verranno definiti quando verra' aperta l'acquisizione:
        self.sliderSeek = wx.Slider(self.panel, wx.ID_ANY)
        
        
        ############################################################################################
        # CREAZIONE STATUS BAR
        self.sb = self.CreateStatusBar()
        # Suddivide la status bar in due campi:
        self.sb.SetFieldsCount(2)
        # Imposta le proporzioni tra i due campi della status bar
        # (richiede numeri interi):
        self.SetStatusWidths([-4, -3])
        # Scrive la stringa seguente nel secondo riquadro della status bar:
        self.SetStatusText("Mocap file quality:", 1)
        
        
        #############################################################
        # Inizialmente alcuni item sono disabilitati in base alle
        # variabili di caricamento mocap file registrazione in corso:
        self.enable_disable_items()
                
        
        #############################################################
        # BINDING WIDGET-METODI:
        # Questo evento si verifica quando l'utente chiude la
        # finestra principale premendo la x della finestra:
        self.Bind(wx.EVT_CLOSE, self.OnCloseGui)
        
        # Menubar
        # Menu "File":
        self.Bind(wx.EVT_MENU, self.on_button_save_as_rec, id=wx.ID_SAVEAS)
        self.Bind(wx.EVT_MENU, self.OnButtonRec,    id=102)
        self.Bind(wx.EVT_MENU, self.on_button_stop,   id=wx.ID_STOP)
        self.Bind(wx.EVT_MENU, self.on_button_browse, id=104)
        self.Bind(wx.EVT_MENU, self.on_button_open,   id=wx.ID_OPEN)
        self.Bind(wx.EVT_MENU, self.on_menu_open_folder,   id=105)
        self.Bind(wx.EVT_MENU, self.on_button_close,  id=wx.ID_CLOSE)
        self.Bind(wx.EVT_MENU, self.OnCloseGui,     id=wx.ID_EXIT)
        
        # Menu "Edit":
        self.Bind(wx.EVT_MENU, self.OnButtonLabels, id=201)
        self.Bind(wx.EVT_MENU, self.on_button_export, id=202)
        self.Bind(wx.EVT_MENU, self.OnButtonTrails, id=203)
        self.Bind(wx.EVT_MENU, self.OnButtonCOM,    id=204)
        self.Bind(wx.EVT_MENU, self.OnButtonLoop,   id=205)
        self.Bind(wx.EVT_MENU, self.on_button_crop,   id=206)
        self.Bind(wx.EVT_MENU, self.OnPreferences,  id=207)
        
        # Menu "View":
        self.Bind(wx.EVT_MENU, self.on_menu_view, id=301)
        self.Bind(wx.EVT_MENU, self.on_menu_view, id=302)
        self.Bind(wx.EVT_MENU, self.on_menu_view, id=303)
        self.Bind(wx.EVT_MENU, self.on_menu_view, id=304)
        self.Bind(wx.EVT_MENU, self.on_menu_view, id=305)
        self.Bind(wx.EVT_MENU, self.on_menu_view, id=306)
        self.Bind(wx.EVT_MENU, self.OnMenuTranspFloor, id=307)
                
        # Menu "Help":
        self.Bind(wx.EVT_MENU, self.on_menu_tutorials, id=400)
        self.Bind(wx.EVT_MENU, self.on_menu_guide, id=wx.ID_HELP)
        self.Bind(wx.EVT_MENU, self.OnMenuCheckUpdates, id=402)
        self.Bind(wx.EVT_MENU, self.on_menu_download, id=403)
        self.Bind(wx.EVT_MENU, self.on_menu_changelog, id=404)
        self.Bind(wx.EVT_MENU, self.OnMenuLicense, id=405)
        self.Bind(wx.EVT_MENU, self.OnMenuAbout, id=wx.ID_ABOUT)
                  
        # Menu "Social":
        self.Bind(wx.EVT_MENU, self.on_menu_website, id=500)  
        self.Bind(wx.EVT_MENU, self.on_menu_facebook, id=501)
        self.Bind(wx.EVT_MENU, self.on_menu_youtube, id=502)
        self.Bind(wx.EVT_MENU, self.on_menu_twitter, id=503)
        self.Bind(wx.EVT_MENU, self.on_menu_gplus, id=504)
         
        
        # Sezione "REC" della GUI:
        self.Bind(wx.EVT_BUTTON, self.on_button_save_as_rec, self.button_save_as)
                
        self.Bind(wx.EVT_TEXT, self.OnTextMocapFileRecChanged, self.text_mocap_file_rec)
        self.Bind(wx.EVT_TEXT, self.OnTextAthleteRecChanged, self.textAthleteRec)
        self.Bind(wx.EVT_TEXT, self.OnTextTechniqueRecChanged, self.textTechniqueRec)
        self.Bind(wx.EVT_SPINCTRL, self.OnSpinWeightRecChanged, self.spinWeightRec)
        self.Bind(wx.EVT_TEXT, self.OnSpinHeight_cmRecChanged, self.spinHeight_cmRec)
        self.Bind(wx.EVT_CHOICE, self.OnChoiceGenderRec, self.choiceGenderRec)
        self.Bind(wx.EVT_BUTTON, self.OnButtonRec, self.button_rec)
        self.Bind(wx.EVT_BUTTON, self.on_button_stop, self.buttonStop) 
        self.Bind(wx.EVT_SPINCTRL, self.OnSpinTiltChanged, self.spinTilt)
        
        # Sezione "PLAY" della GUI:
        self.Bind(wx.EVT_BUTTON, self.on_button_browse, self.buttonBrowse)
        self.Bind(wx.EVT_BUTTON, self.on_button_open, self.buttonOpen)
        self.Bind(wx.EVT_BUTTON, self.on_button_close, self.buttonClose)
        self.Bind(wx.EVT_BUTTON, self.OnButtonLabels, self.buttonLabels) 
        self.Bind(wx.EVT_BUTTON, self.on_button_export, self.buttonExport)
        self.Bind(wx.EVT_BUTTON, self.OnButtonTrails, self.buttonTrails)
        self.Bind(wx.EVT_BUTTON, self.OnButtonCOM, self.buttonCOM)
        self.Bind(wx.EVT_BUTTON, self.OnButtonLoop, self.buttonLoop)
        self.Bind(wx.EVT_BUTTON, self.on_button_crop, self.button_crop)
        self.Bind(wx.EVT_SPINCTRL, self.on_spin_goto_changed, self.spin_goto)
        self.Bind(wx.EVT_SPINCTRL, self.on_spin_play_speed_changed, self.spinPlaySpeed)
        self.Bind(wx.EVT_BUTTON, self.OnButtonStart, self.buttonStart)
        self.Bind(wx.EVT_BUTTON, self.OnButtonStepB, self.buttonStepB)
        self.Bind(wx.EVT_BUTTON, self.OnButtonPlay, self.buttonPlay)
        self.Bind(wx.EVT_BUTTON, self.OnButtonStepF, self.buttonStepF)
        self.Bind(wx.EVT_BUTTON, self.OnButtonEnd, self.buttonEnd)        
        self.Bind(wx.EVT_SLIDER, self.OnSeek, self.sliderSeek)

        # Quando il mouse passa su alcuni dei widget della GUI appare la
        #relativa didascalia nel secondo riquadro della status bar:
        self.button_save_as.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonSaveAs)
        self.button_rec.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonRec)
        self.buttonStop.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonStop)
        self.buttonBrowse.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonBrowse)
        self.buttonOpen.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonOpen)
        self.buttonClose.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonClose)
        self.buttonLabels.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonLabels)
        self.buttonExport.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonExport)
        self.buttonTrails.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonTrails)
        self.buttonCOM.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonCOM)
        self.buttonLoop.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonLoop)
        self.button_crop.Bind(wx.EVT_ENTER_WINDOW, self.EnterButtonCrop)
        
        # Quando il mouse esce dai suddetti widget della GUI, la relativa
        # didascalia nel secondo riquadro della status bar scompare:
        self.button_save_as.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.button_rec.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonStop.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonBrowse.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonOpen.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonClose.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonLabels.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonExport.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonTrails.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonCOM.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.buttonLoop.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
        self.button_crop.Bind(wx.EVT_LEAVE_WINDOW, self.LeaveButton)
               
        
        ######################################################################
        # SIZER DELLA GUI
        # Creazione GridBagSizer: questo sizer gestisce l'intera GUI.
        self.gb_sizer = wx.GridBagSizer(13, 5)
                       
        # Sezione "REC" della GUI
        self.gb_sizer.Add(st_label_section_rec, pos=(0, 0), span=(1, 2),
                          flag=wx.ALIGN_CENTRE | wx.ALL, border=cfg.EMPTY_BORDER)
        # Colonna 0:
        self.gb_sizer.Add(st_label_mocap_file_rec, pos=(1, 0),
                          flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL |
                          wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.button_save_as, pos=(2, 0),
                          flag=wx.ALIGN_RIGHT |  wx.ALIGN_TOP | wx.RIGHT |
                          wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(st_label_athlete_rec, pos=(3, 0),
                          flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL |
                          wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(st_label_technique_rec, pos=(4, 0),
                          flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL |
                          wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(st_label_weight_rec, pos=(5, 0),
                          flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL |
                          wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(st_label_height_cm_rec, pos=(6, 0),
                          flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL |
                          wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(st_label_gender_rec, pos=(7, 0),
                          flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL |
                          wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(st_label_tilt, pos=(8, 0), flag=wx.ALIGN_RIGHT |
                          wx.ALIGN_CENTRE_VERTICAL | wx.RIGHT | wx.LEFT,
                          border=cfg.EMPTY_BORDER)
        
        # Colonna 1:  
        self.gb_sizer.Add(self.text_mocap_file_rec, pos=(1,1), span=(2, 1),  flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.textAthleteRec,   pos=(3, 1),              flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.textTechniqueRec, pos=(4, 1),              flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.spinWeightRec,    pos=(5, 1),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.spinHeight_cmRec, pos=(6, 1),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.choiceGenderRec,  pos=(7, 1),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.spinTilt,         pos=(8, 1),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
               
        # Crea un box sizer orizzontale che conterra' i pulsanti Rec e Stop:
        hboxRecord=wx.BoxSizer(wx.HORIZONTAL)
        hboxRecord.Add(self.button_rec,  proportion=0)
        hboxRecord.Add(self.buttonStop, proportion=0)
        # Aggiunge il box sizer al GridBagSizer:
        self.gb_sizer.Add(hboxRecord, pos=(9, 0), span=(1, 2), flag=wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        
        # Aggiunta LOGO VSLITE al sizer:
        vsLogo = wx.StaticBitmap(self.panel, bitmap=wx.Bitmap(cfg.SW_GRAPHICS_FOLDER+os.sep+cfg.SW_LOGO))
        self.gb_sizer.Add(vsLogo, pos=(10, 0), span=(3, 2), flag=wx.ALIGN_CENTRE)
        
        # Creazione static line verticale di separazione tra sezione REC e sezione PLAY:
        vStLine = wx.StaticLine(self.panel, wx.ID_ANY, style=wx.LI_VERTICAL)
        # Aggiunta static line al sizer:
        self.gb_sizer.Add(vStLine, pos=(0, 2), span=(13, 1), flag=wx.EXPAND)
        
        
        # Sezione "PLAY" della GUI
        self.gb_sizer.Add(stLabelSectionPlay,    pos=(0, 3), span=(1,2), flag=wx.ALIGN_CENTRE | wx.ALL, border=cfg.EMPTY_BORDER)
        # Colonna 3:
        self.gb_sizer.Add(stLabelMocapFilePlay,  pos=(1, 3),  flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL | wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.buttonBrowse,     pos=(2, 3),  flag=wx.ALIGN_RIGHT | wx.ALIGN_TOP | wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(stLabelAthletePlay,    pos=(3, 3),  flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL | wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(stLabelTechniquePlay,  pos=(4, 3),  flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL | wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(stLabelWeightPlay,     pos=(5, 3),  flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL | wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(stLabelHeight_cmPlay,  pos=(6, 3),  flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL | wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(stLabelGenderPlay,     pos=(7, 3),  flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL | wx.RIGHT | wx.LEFT, border=cfg.EMPTY_BORDER)
        
        # Colonna 4:
        self.gb_sizer.Add(self.stMocapFilePlay,      pos=(1, 4),  span=(2, 1), flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.stAthletePlay,        pos=(3, 4),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.stTechniquePlay,      pos=(4, 4),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.stWeightPlay,         pos=(5, 4),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)              
        self.gb_sizer.Add(self.stHeight_cmPlay,      pos=(6, 4),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(self.stGenderPlay,         pos=(7, 4),              flag=wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)    
       
        # Creazione BoxSizer orizzontale per button Open e Close:
        hbox_open_close=wx.BoxSizer(wx.HORIZONTAL)
        hbox_open_close.Add(self.buttonOpen,  proportion=0)
        hbox_open_close.Add(self.buttonClose, proportion=0)
        # Aggiunge il box sizer orizzontale dei tasti open e close alla GUI:
        self.gb_sizer.Add(hbox_open_close, pos=(8, 4),  flag=wx.ALIGN_RIGHT | wx.ALIGN_BOTTOM | wx.RIGHT, border=cfg.EMPTY_BORDER)
    
        # Sezione "PLAYER" della GUI
        # Creazione box sizer orizzontale per button strumenti (labels, export, ecc.):
        hbox_tools = wx.BoxSizer(wx.HORIZONTAL)
        hbox_tools.Add(self.buttonLabels,    proportion=0)
        hbox_tools.Add(self.buttonExport,    proportion=0)
        hbox_tools.Add(self.buttonTrails,    proportion=0)
        hbox_tools.Add(self.buttonCOM,       proportion=0)
        hbox_tools.Add(self.buttonLoop,      proportion=0)
        hbox_tools.Add(self.button_crop,      proportion=0)
        
        # Creazione box sizer orizzontale per visualizzazione/selezione frame corrente:
        hbox_cur_frame=wx.BoxSizer(wx.HORIZONTAL)
        hbox_cur_frame.Add(self.st_cur_frame,   proportion=0, flag=wx.ALIGN_CENTRE)
        hbox_cur_frame.Add(self.spin_goto,     proportion=0, flag=wx.ALIGN_CENTRE)
        hbox_cur_frame.Add(self.st_end_frame,   proportion=0, flag=wx.ALIGN_CENTRE)
        
        # Creazione box sizer orizzontale per velocita' di riproduzione:
        hbox_play_speed=wx.BoxSizer(wx.HORIZONTAL)
        hbox_play_speed.Add(self.playSpeedText, proportion=0, flag=wx.ALIGN_CENTRE)
        hbox_play_speed.Add(self.spinPlaySpeed, proportion=0, flag=wx.ALIGN_CENTRE)
        
        # Creazione box sizer orizzontale per button gestione animazione (play, step forward, ecc.):
        hbox_player=wx.BoxSizer(wx.HORIZONTAL)
        hbox_player.Add(self.buttonStart,    proportion=0)
        hbox_player.Add(self.buttonStepB,    proportion=0)
        hbox_player.Add(self.buttonPlay,     proportion=0)
        hbox_player.Add(self.buttonStepF,    proportion=0)
        hbox_player.Add(self.buttonEnd,      proportion=0)
                
        # Aggiunge i controlli del player alla GUI:
        self.gb_sizer.Add(hbox_tools,     pos=(9,3),  span=(1, 2),    flag=wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(hbox_cur_frame,  pos=(10,3),                flag=wx.ALIGN_RIGHT  | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        self.gb_sizer.Add(hbox_play_speed, pos=(10,4),                flag=wx.ALIGN_RIGHT  | wx.ALIGN_CENTRE_VERTICAL | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        # Lo sliderSeek si allunga quando viene allargato il frame:
        self.gb_sizer.Add(self.sliderSeek,   pos=(11,3), span=(1, 2),     flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        # Aggiunge il box sizer con i pulsanti play, ecc al sizer della GUI:
        self.gb_sizer.Add(hbox_player,    pos=(12,3), span=(1, 2),     flag=wx.ALIGN_CENTRE | wx.LEFT | wx.RIGHT, border=cfg.EMPTY_BORDER)
        
        # Rende le colonne contenenti i TextCtrl allargabili con proporzione 1:
        self.gb_sizer.AddGrowableCol(1,1)
        self.gb_sizer.AddGrowableCol(4,1)
        # Rende la riga con i nomi dei mocap file allungabile in verticale:
        self.gb_sizer.AddGrowableRow(2,1)
        
        # Assegna il GridBagSizer al pannello della GUI:
        self.panel.SetSizerAndFit(self.gb_sizer)
        
        

       
       
    #################################################################################################
    # METODI
    def on_startup(self):
        """Operazioni effettuate all'avvio del programma prima del mainloop."""
        # Verifica se sono disponibili aggiornamenti in maniera silenziosa
        self.OnMenuCheckUpdates(None, is_silent=True)
        # Verifica se l'utente e' registrato:
        self.is_activated()
        
    
    def is_activated(self):
        """Controlla presenza e validita' del file di attivazione.
        
        La presenza serve in caso di utente non registrato,
        la validita' serve per verificare se la chiave prodotto e' corretta.
        """
        
        # Se il file di attivazione esiste
        if os.path.isfile(cfg.ACTIVATION_FILE_NAME):
            self.SetStatusText("Activation file is present", 0)
            # ne copia il contenuto nell'activation_dict
            self.activation_dict = vsutils.json2dict(cfg.ACTIVATION_FILE_NAME,
                                                     self) 
            # Verifica la validita' della chiave       
            mail = self.activation_dict['mail']
            key = self.activation_dict['product_key']
            if not vsutils.is_verified_key(mail, key):
                self.SetStatusText("Product key is not valid", 0)
                # apre la dialog di registrazione utente
                vsutils.VsActivation(self)
            else:
                self.SetStatusText("Product key is valid", 0)
        
        # Se il file di attivazione non esiste
        else:
            self.SetStatusText("Activation file is not present", 0)
            # apre la dialog di registrazione utente
            vsutils.VsActivation(self)    
       
          
    def enable_disable_items(self):
        # Questa funzione abilita/disabilta alcuni item della menu bar a seconda che l'utente abbia aperto
        # o meno un'acquisizione o stia o meno registrandone una nuova. Le due variabili is_recording e
        # is_loaded_mocap_file possono essere (allo stato attuale del software) entrambe false, o una falsa e una vera.
        
        # 1) Valuta lo stato di caricamento di un'acquisizione precedentemente salvata e agisce
        # solo sui widget relativi alla sezione "PLAY":
        if not cfg.is_recording and not cfg.is_loaded_mocap_file:
            # MENUBAR "REC":
            self.menubar.item_save_as.Enable(True)
            self.menubar.item_record.Enable(True)
            self.menubar.item_stop.Enable(False)
            # MENUBAR "PLAY":
            self.menubar.item_browse.Enable(True)
            self.menubar.item_open.Enable(True)
            self.menubar.item_close.Enable(False)
            self.menubar.check_labels.Enable(False)
            self.menubar.item_export.Enable(False)
            self.menubar.item_trails.Enable(False)
            self.menubar.check_com.Enable(False)
            self.menubar.check_loop.Enable(False)
            self.menubar.item_crop.Enable(False)         
            self.menubar.item_cam_xp.Enable(False)
            self.menubar.item_cam_xm.Enable(False)
            self.menubar.item_cam_yp.Enable(False)
            self.menubar.item_cam_ym.Enable(False)
            self.menubar.item_cam_zp.Enable(False)
            self.menubar.item_cam_zm.Enable(False)
            self.menubar.check_floor_transp.Enable(False)
            # Sezione "REC" della GUI:
            self.button_save_as.Enable(True)
            self.text_mocap_file_rec.Enable(True)
            self.textAthleteRec.Enable(True)
            self.textTechniqueRec.Enable(True)
            self.spinWeightRec.Enable(True)
            self.spinHeight_cmRec.Enable(True)
            self.choiceGenderRec.Enable(True)
            self.button_rec.Enable(True)
            self.buttonStop.Enable(False)
            self.spinTilt.Enable(False)
            # Sezione "PLAY" della GUI:
            self.buttonBrowse.Enable(True)
            self.buttonOpen.Enable(True)
            self.buttonClose.Enable(False)
            self.buttonLabels.Enable(False)
            self.buttonExport.Enable(False)
            self.buttonTrails.Enable(False)
            self.buttonCOM.Enable(False)
            self.buttonLoop.Enable(False)
            self.button_crop.Enable(False)
            self.spin_goto.Enable(False)
            self.spinPlaySpeed.Enable(False)
            self.sliderSeek.Enable(False)
            self.buttonStart.Enable(False)
            self.buttonStepB.Enable(False)
            self.buttonPlay.Enable(False)
            self.buttonStepF.Enable(False)
            self.buttonEnd.Enable(False)

        elif cfg.is_recording and not cfg.is_loaded_mocap_file:
            # MENUBAR "REC":
            self.menubar.item_save_as.Enable(False)
            self.menubar.item_record.Enable(False)
            self.menubar.item_stop.Enable(True)
            # MENUBAR "PLAY":
            self.menubar.item_browse.Enable(False)
            self.menubar.item_open.Enable(False)
            self.menubar.item_close.Enable(False)
            self.menubar.check_labels.Enable(False)
            self.menubar.item_export.Enable(False)
            self.menubar.item_trails.Enable(False)
            self.menubar.check_com.Enable(False)
            self.menubar.check_loop.Enable(False)
            self.menubar.item_crop.Enable(False)         
            self.menubar.item_cam_xp.Enable(False)
            self.menubar.item_cam_xm.Enable(False)
            self.menubar.item_cam_yp.Enable(False)
            self.menubar.item_cam_ym.Enable(False)
            self.menubar.item_cam_zp.Enable(False)
            self.menubar.item_cam_zm.Enable(False)
            self.menubar.check_floor_transp.Enable(False)
            # Sezione "REC" della GUI:
            self.button_save_as.Enable(False)
            self.text_mocap_file_rec.Enable(False)
            self.textAthleteRec.Enable(False)
            self.textTechniqueRec.Enable(False)
            self.spinWeightRec.Enable(False)
            self.spinHeight_cmRec.Enable(False)
            self.choiceGenderRec.Enable(False)
            self.button_rec.Enable(False)
            self.buttonStop.Enable(True)
            self.spinTilt.Enable(True)
            # Sezione "PLAY" della GUI:
            self.buttonBrowse.Enable(False)
            self.buttonOpen.Enable(False)
            self.buttonClose.Enable(False)
            self.buttonLabels.Enable(False)
            self.buttonExport.Enable(False)
            self.buttonTrails.Enable(False)
            self.buttonCOM.Enable(False)
            self.buttonLoop.Enable(False)
            self.button_crop.Enable(False)
            self.spin_goto.Enable(False)
            self.spinPlaySpeed.Enable(False)
            self.sliderSeek.Enable(False)
            self.buttonStart.Enable(False)
            self.buttonStepB.Enable(False)
            self.buttonPlay.Enable(False)
            self.buttonStepF.Enable(False)
            self.buttonEnd.Enable(False)
            
        elif not cfg.is_recording and cfg.is_loaded_mocap_file:
            # MENUBAR "REC":
            self.menubar.item_save_as.Enable(False)
            self.menubar.item_record.Enable(False)
            self.menubar.item_stop.Enable(False)
            # MENUBAR "PLAY":
            self.menubar.item_browse.Enable(False)
            self.menubar.item_open.Enable(False)
            self.menubar.item_close.Enable(True)
            self.menubar.check_labels.Enable(True)
            self.menubar.item_export.Enable(True)
            self.menubar.item_trails.Enable(True)
            self.menubar.check_com.Enable(True)
            self.menubar.check_loop.Enable(True)
            self.menubar.item_crop.Enable(True)         
            self.menubar.item_cam_xp.Enable(True)
            self.menubar.item_cam_xm.Enable(True)
            self.menubar.item_cam_yp.Enable(True)
            self.menubar.item_cam_ym.Enable(True)
            self.menubar.item_cam_zp.Enable(True)
            self.menubar.item_cam_zm.Enable(True)
            self.menubar.check_floor_transp.Enable(True)
            # Sezione "REC" della GUI:
            self.button_save_as.Enable(False)
            self.text_mocap_file_rec.Enable(False)
            self.textAthleteRec.Enable(False)
            self.textTechniqueRec.Enable(False)
            self.spinWeightRec.Enable(False)
            self.spinHeight_cmRec.Enable(False)
            self.choiceGenderRec.Enable(False)
            self.button_rec.Enable(False)
            self.buttonStop.Enable(False)
            self.spinTilt.Enable(False)
            # Sezione "PLAY" della GUI:
            self.buttonBrowse.Enable(False)
            self.buttonOpen.Enable(False)
            self.buttonClose.Enable(True)
            self.buttonLabels.Enable(True)
            self.buttonExport.Enable(True)
            self.buttonTrails.Enable(True)
            self.buttonCOM.Enable(True)
            self.buttonLoop.Enable(True)
            self.button_crop.Enable(True)
            self.spin_goto.Enable(True)
            self.spinPlaySpeed.Enable(True)
            self.sliderSeek.Enable(True)
            self.buttonStart.Enable(True)
            self.buttonStepB.Enable(True)
            self.buttonPlay.Enable(True)
            self.buttonStepF.Enable(True)
            self.buttonEnd.Enable(True)
            
    
    # Metodi della MENUBAR      
    def on_menu_view(self, event):
        """"Scelta del punto di vista della scena 3D"""
        # Verifica se l'acquisizione e' caricata:
        if cfg.is_loaded_mocap_file:
            # Usare la funzione isChecked invece di event.GetId() permette di usare questa funzione
            # non solo quando si clicca sugli item, ma anche quando essi sono gia' cliccati,
            # per aggiornare la scena:
            if self.menubar.view.IsChecked(301): # Cam +X
                self.anim.scene.forward = (1, 0, 0) 
            
            elif self.menubar.view.IsChecked(302): # Cam -X
                self.anim.scene.forward = (-1, 0, 0)
            
            elif self.menubar.view.IsChecked(303): # Cam +Y
                self.anim.scene.forward = (0, 1, 0)
            
            elif self.menubar.view.IsChecked(304): # Cam -Y
                self.anim.scene.forward = (0, -1, 0)
            
            elif self.menubar.view.IsChecked(305): # Cam +Z
                self.anim.scene.forward = (0, 0, 1)
                
            elif self.menubar.view.IsChecked(306): # Cam -Z
                self.anim.scene.forward = (0, 0, -1)
            
            # Metodo alternativo, ma funziona solo quando si verifica l'evento
            # cioe' quando l'utente clicca su uno dei menu item:
#            if event.GetId() == 301: # Cam +X
#                self.animScene.forward=(1,0,0) 
#            
#            elif event.GetId() == 302: # Cam -X
#                self.animScene.forward=(-1,0,0)
#            
#            elif event.GetId() == 303: # Cam +Y
#                self.animScene.forward=(0,1,0)
#            
#            elif event.GetId() == 304: # Cam -Y
#                self.animScene.forward=(0,-1,0)
#            
#            elif event.GetId() == 305: # Cam +Z
#                self.animScene.forward=(0,0,1)
#                
#            elif event.GetId() == 306: # Cam -Z
#                self.animScene.forward=(0,0,-1)

    # TODO: fare controllo all'avvio su trasparenza floor come per COM
    def OnMenuTranspFloor(self,event):
        """Changes the floor transparency in the 3d scene."""
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            # Verifica lo stato della variabile booleana che controlla la trasparenza del Floor:
            if not cfg.is_transp_floor:
                # ne inverte lo stato:
                cfg.is_transp_floor = True
                # rende trasparente l'oggetto Floor:
                self.anim.floor.opacity=cfg.OPACITY_FLOOR_TRANSPARENT
                # e mette il check sul relativo item della menubar:
                self.menubar.check_floor_transp.Check(True)
            # e viceversa: 
            elif cfg.is_transp_floor:
                cfg.is_transp_floor = False
                self.anim.floor.opacity=1
                self.buttonCOM.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON) # TODO: errore non c'entra niente il COM qui
                self.menubar.check_floor_transp.Check(False)
    
    def on_menu_tutorials(self, event):
        """"Apre il browser sulla pagina dei tutorial."""
        webbrowser.open(cfg.SW_ONLINE_TUTORIALS)

    def on_menu_guide(self, event):
        """"Apre il browser sulla pagina dell'help."""
        webbrowser.open(cfg.SW_ONLINE_HELP)
    
    


        
    
    def OnMenuCheckUpdates(self, event, is_silent=False):
        """"Controlla se esiste una versione piu' recente del software ed eventualmente la scarica,
        avvia il setup e chiude il programma."""
        # is_silent=True: non mostra messaggi se non vengono trovate versioni piu' recenti.
        def update_dload_progress_dlg(count, blockSize, totalSize):
            percent = int(count * blockSize * 100 / totalSize)
            # Aggiorna la barra di avanzamento ad ogni pezzo di file scaricato
            # Ad ogni chiamata restituisce due True: se l'utente ha premuto cancel
            # nella dialog, il primo argomento restituito diventa False:
            (is_continue, is_skip) = dloadProgressDlg.Update(percent)
            if not is_continue:
                # Se l'utente preme cancel solleva un'eccezione ad hoc: lo scopo e' fermare il download
                # che altrimenti continuerebbe comunque fino al completamento.
                # Ho messo la KeyboardInterrupt exception perche' mi sembra la piu'
                # assonante con il tipo di eccezione, senza nessun motivo tecnico.
                raise KeyboardInterrupt(cfg.STR_DOWNLOAD_CANCELLED_USER)
                               
        
        # 1) Tenta di scaricare il file JSON con i dati sulla versione corrente del software:
        try:
            # salva il file letto nella cartella temp di windows e restituisce filename e header:
            (json_filename, json_header) = urllib.urlretrieve(cfg.URL_JSON_CUR_VERS)
        except IOError, exc_inst: # exception instance: contiene dettagli sull'eccezione
            if not is_silent:
                # se manca la connessione ad internet o l'url richiesto e' scritto male (es: htttp://):
                vsutils.err_dlg(cfg.STR_ERROR, exc_inst.__str__(),
                               parent=self, suggestion=cfg.STR_CHECK_YOUR_INTERNET_CONNECTION)
            return
        
        # 2) Apre il file scaricato in sola lettura:
        # TODO: vorrei usare la funzione vsutils.json2dict ma c'e' il
        # problema del return in caso di errore
        with open(json_filename,'r') as f:
            # prova a leggere il file JSON convertendolo in un dict, contenente oggetti unicode:
            try: # TODO: usando il with serve ancora il try dentro? provare!
                json_dict = json.load(f)
            except ValueError, exc_inst:
                if not is_silent:
                    # se il file scaricato non corrisponde ad un file json:
                    vsutils.err_dlg(cfg.STR_ERROR, exc_inst.__str__(), parent=self)
                return
        
        # 3) Legge la versione corrente del software dal dict creato:
        cur_vers = json_dict["version"]
        # Se la versione installata e' uguale alla corrente, non occorre aggiornare:
        if cfg.SW_VERSION == cur_vers:
            # Mostra un messaggio solo se non si e' in modalita' silent:
            if not is_silent:
                vsutils.msg_dlg(cfg.STR_INFO, cfg.SW_NAME+' is up-to-date', parent=self) 

        # altrimenti chiede all'utente conferma prima di scaricare la versione aggiornata:
        else:
            newVersFoundDlg = wx.MessageDialog(self,
                                               'New version of ' + cfg.SW_NAME +
                                               ' found (' + cur_vers +
                                               ').\nDo you want to update (program will be closed)?',
                                               'New release found',
                                               wx.YES_NO | wx.ICON_QUESTION)
            # salvo la risposta in una variabile ed elimino subito la dialog in modo da 
            # non doverla eliminare in punti diversi dopo a seconda di cosa succede:
            userWantsToUpdate = newVersFoundDlg.ShowModal()
            newVersFoundDlg.Destroy()
               
            # Se l'utente ha deciso di aggiornare:
            if userWantsToUpdate == wx.ID_YES:
                # Crea dialog con barra di avanzamento download; si nasconde da
                # sola quando la barra raggiunge il 100% ma va comunque
                # cancellata dalla memoria:        
                dloadProgressDlg = wx.ProgressDialog('Downloading...',
                                                     'Downloading '+cfg.SW_NAME+' v.'+cur_vers+'...',
                                                     maximum=100,
                                                     parent=self,
                                                     style= wx.PD_CAN_ABORT | wx.PD_AUTO_HIDE | wx.GA_SMOOTH |
                                                     wx.PD_REMAINING_TIME | wx.PD_ELAPSED_TIME | wx.PD_ESTIMATED_TIME
                                                     )
                # legge il link da cui scaricare il nuovo setup:
                cur_vers_setup_url = json_dict["setup_url"]
                # Scarica il setup e lo mette nella cartella temp di windows.
                # "reporthook" richiama la funzione di aggiornamento della
                # progress dialog ogni volta che viene scaricato un pezzo del
                # file, passandogli tre numeri (blocchi scaricati, dimensione
                # blocco e dimensione totale):
                try:
                    (setup_filename, setup_header) = urllib.urlretrieve(cur_vers_setup_url,
                                                                        reporthook=update_dload_progress_dlg)
                except IOError, exc_inst:
                    # se non puo' essere eseguita la connessione:
                    vsutils.err_dlg(cfg.STR_ERROR, exc_inst.__str__(), parent=self,
                                   suggestion=cfg.STR_CHECK_YOUR_INTERNET_CONNECTION) 
                    return                
                except KeyboardInterrupt, exc_inst:
                    # se l'utente ha premuto Cancel nella progress dialog
                    # (vedi funzione update_dload_progress_dlg):
                    vsutils.msg_dlg(cfg.STR_INFO, exc_inst.__str__(), parent=self)             
                    return
                else:
                    # se il download e' stato completato, invia l'evento a
                    # google analytics:
                    vsutils.ga_track_event('Downloads', cur_vers)
                finally:
                    # finally viene eseguito in ogni caso, anche dopo un return:
                    dloadProgressDlg.Destroy()
                    
                    
                try:
                    # Finito il download, uso il comando "Popen" invece di
                    # quello di alto livello "call", perche' il secondo
                    # attende per default la fine del processo, invece io
                    # voglio lanciare il setup e chiudere il programma.
                    # Il parametro "close_fds" permette di non ereditare
                    # handles dal subprocess, consentendo l'effettiva chiusura
                    # del programma, che altrimenti resterebbe aperto anche
                    # se invisibile:
                    subprocess.Popen(setup_filename, close_fds=True)
                except OSError, exc_inst:
                    # Se il file .exe non viene trovato mostra una dialog di
                    # errore:
                    vsutils.err_dlg(cfg.STR_ERROR, exc_inst.__str__(),
                                    parent=self)
                else:
                    # Se non viene sollevata alcuna eccezione, chiude il
                    # programma cancellandolo dalla memoria,
                    # senza chiedere conferma:
                    self.OnCloseGuiSilently(None)                 
                    
            
        
    def on_menu_download(self, event):  #IGNORE:R0201
        """Apre il browser sulla pagina dei download."""
        webbrowser.open(cfg.SW_DOWNLOAD_PAGE)
    
    def on_menu_changelog(self, event): #IGNORE:R0201
        """Apre il browser sulla pagina del changelog."""
        webbrowser.open(cfg.SW_CHANGELOG_PAGE)
           
    def OnMenuLicense(self, event):
        """Mostra la licenza del software.
        
        Questa finestra contenente la licenza e' stata creata per
        non appesantire troppo la finestra about.
        """
        # Se il file della licenza esiste:
        if os.path.isfile(cfg.SW_LICENSE_FILE):
            # Lo apre:
            with open(cfg.SW_LICENSE_FILE, 'r') as lic_file:
                # Legge tutto il file:
                lic = lic_file.read()
                # Fuori dal with il file viene chiuso automaticamente.
                    
            # Crea la dialog che conterra' il testo:
            dialogs.scrolledMessageDialog(self, lic, 'Virtual Sensei Lite EULA') # TODO: serve veramente l'import dialogs?
        
        # Se invece il file della licenza e' stato cancellato:
        else:
            webbrowser.open(cfg.SW_ONLINE_LICENSE_FILE)
                                
    def OnMenuAbout(self,event):
        """Mostra la dialog about del software."""
        # First we create and fill the info object:
        info = wx.AboutDialogInfo()
        # Se non si attribuisce un icona con info.SetIcon, viene usata quella del programma.
        #info.SetIcon(wx.Icon('icons/CHOOSE_ICON.png', wx.BITMAP_TYPE_PNG))
        info.Name = cfg.SW_NAME
        info.Version = cfg.SW_VERSION
        info.Copyright = cfg.SW_COPYRIGHT
        info.Description = wordwrap(
            "Virtual Sensei is a software for sports motion analysis, "
            "based on the evaluation of kinetic energy developed by the athletes. "
            "It was tested on elite karate athletes, but can be "
            "extended to other sport disciplines."
            
            "\n\nThis lite version of Virtual Sensei, based on Kinect, "
            "is intended for all those enthusiasts who want to adopt "
            "such an innovative tool, that is easy, portable and affordable "
            "at the same time."
            "\n\nThis time the elite athlete... is You! Therefore, enjoy and..."
            "\nDiscover your kinetic energy, everywhere!\n\n",
            500, wx.ClientDC(self))
        info.WebSite = cfg.SW_URL
        info.AddDeveloper('Alessandro Timmi')
        info.AddDeveloper("Ettore Pennestri'")
        info.AddDeveloper('Pier Paolo Valentini')
        info.AddDeveloper('Andrea Pilia')
                        
#        licenseText=wordwrap(
#                             "SCRIVERE TESTO LICENZA",
#                             500, wx.ClientDC(self)
#                             )              
           
        #info.License = wordwrap(licenseText, 500, wx.ClientDC(self))

        # Then we call wx.AboutBox giving it that info object
        wx.AboutBox(info)
        
        
    def on_menu_website(self, event):    #IGNORE:R0201
        # Apre la homepage di Virtual Sensei Lite:
        webbrowser.open(cfg.SW_URL)  
         
    def on_menu_facebook(self, event):   #IGNORE:R0201
        # Apre la pagina Facebook di Virtual Sensei:
        webbrowser.open(cfg.SW_FACEBOOK_PAGE)
        
    def on_menu_youtube(self, event):    #IGNORE:R0201
        # Apre il canale Youtube di Virtual Sensei:
        webbrowser.open(cfg.SW_YOUTUBE_CHANNEL)  
                  
    def on_menu_twitter(self, event):    #IGNORE:R0201
        # Apre il canale Twitter di Virtual Sensei:
        webbrowser.open(cfg.SW_TWITTER_CHANNEL) 
            
    def on_menu_gplus(self, event):      #IGNORE:R0201
        # Apre la pagina Google+ di Virtual Sensei:
        webbrowser.open(cfg.SW_GOOGLE_PLUS_PAGE)


    def OnPreferences(self, event):
        VSPreferencesDlg(parent=self,
                         title=cfg.STR_EDIT_PREFERENCES)    
    
    # Metodi della sezione "REC" della GUI
    def on_button_save_as_rec(self, event):
        """Scelta del percorso di salvataggio dell'acquisizione."""
        save_as_dlg = wx.FileDialog(self, message=cfg.STR_SAVE_AS,
            defaultDir=os.getcwd(), 
            defaultFile=cfg.athleteRec + '_' + cfg.techniqueRec,
            wildcard=cfg.STR_WILDCARD,
            style=wx.SAVE | wx.FD_OVERWRITE_PROMPT
            #| wx.CHANGE_DIR questo da problemi perche' modifica la directory corrente
            )
        
        if save_as_dlg.ShowModal() == wx.ID_OK:
            # Salva il percorso+nomefile nella variabile mocapFile:
            cfg.mocap_file_rec = save_as_dlg.GetPath()
            # aggiorna il text ctrl del mocap file da aprire:
            self.text_mocap_file_rec.SetValue(cfg.mocap_file_rec)

        save_as_dlg.Destroy()          
        
    
    def OnButtonRec(self, event):
        # La registrazione di una nuova acquisizione puo' partire solo se non ne e' gia'
        # in corso una e se l'utente non ha aperto un'acquisizione pre-esistente:
        if not cfg.is_recording and not cfg.is_loaded_mocap_file:
            # Se l'utente ha dimenticato di selezionare il file dove salvare la registrazione compare un messaggio di errore:
            if not cfg.mocap_file_rec:
                vsutils.err_dlg(cfg.STR_ERROR, cfg.STR_FILE_NOT_SELECTED, parent=self,
                               suggestion=cfg.STR_CLICK_ON_SAVE_AS)
                return
            
            # Se l'utente ha dimenticato di selezionare il sesso dell'atleta compare un messaggio di errore:
            if not cfg.genderRec:
                vsutils.err_dlg(cfg.STR_ERROR, "Athlete's gender not selected",
                                parent=self)
                return
            
            # Se il nome file selezionato e' gia' esistente significa che l'utente sta effettuando una nuova
            # registrazione lasciando il nome file usato per quella precedente. Pertanto il programma chiede
            # conferma prima di sovrascriverlo o arresta la registrazione:
            if os.path.isfile(cfg.mocap_file_rec):
                # Crea una dialog di richiesta sovrascrittura file: 
                overwriteDlg=wx.MessageDialog(self, cfg.STR_REQUEST_OVERWRITE,
                                              cfg.STR_CONFIRM, wx.YES_NO |
                                              wx.ICON_QUESTION)
                               
                answ = overwriteDlg.ShowModal() 
                overwriteDlg.Destroy()
                if  answ == wx.ID_NO:
                    return
                
            # Crea la lista di argomenti da passare al programma di acquisizione.
            # Il percorso del file exe va specificato rispetto al main di VSLite:
            trackerArgs = [cfg.SW_MODULES_FOLDER + os.sep + cfg.TRACKING_MODULE_NAME,
                           cfg.mocap_file_rec,
                           cfg.athleteRec,
                           cfg.techniqueRec,
                           str(cfg.weightRec),
                           str(cfg.height_cmRec),
                           cfg.genderRec] 
                        
            # Apre l'eseguibile per la registrazione di una nuova acquisizione.
            # Oltre alla lista di argomenti, viene definita la
            # current working directory dell'exe, perche' diversa da quella 
            # del main di VSLite. In questo modo il subprocess di tracciamento riesce a
            # trovare il file xml di configurazione e la .dll di opengl:
            try:         
                self.rec_process = subprocess.Popen(trackerArgs,
                                                    cwd=cfg.SW_MODULES_FOLDER)                        
                
            # Se il file .exe non viene trovato:
            except OSError, exc_inst:
                # La variabile exc_inst e' un'istanza dell'eccezione: contiene
                # il messaggio di errore in forma di stringa.
                vsutils.err_dlg(cfg.STR_ERROR, exc_inst.__str__(), parent=self)
            else:
                # Cambia lo stato della variabile booleana di registrazione:
                cfg.is_recording = True
                # Disabilita e abilita alcuni elementi della GUI:
                self.enable_disable_items()
                
                
                
    def on_button_stop(self, event):
        """Callback del pulsante stop durante registrazione mocap file."""
        if cfg.is_recording:
            # Possono esserci due possibilita': se il kinect e' stato correttamente avviato,
            # il processo e' partito e puo' essere terminato normalmente:
            try:
                # Chiude l'eseguibile aperto per la registrazione di una nuova acquisizione
                subprocess.Popen.terminate(self.rec_process)
            # Se invece il processo non e' partito perche' ad esempio il kinect non e'
            # collegato al computer:
            except:
                # non occorre fare null'altro:
                pass
            finally:
                # Cambia lo stato della variabile booleana di registrazione:
                cfg.is_recording=False
                # Disabilita e abilita alcuni elementi della GUI:
                self.enable_disable_items()
            
        
    def OnSpinTiltChanged(self, event):
        if cfg.is_recording:
            # Angolo in gradi letto dallo spinTilt:
            tiltAngleDeg=self.spinTilt.GetValue()
            # Parametri di input del modulo di gestione del motore di tilt del kinect:
            # il primo parametro e' il percorso dell'exe, il secondo e' l'angolo di tilt.
            inputArgs = [cfg.SW_MODULES_FOLDER+os.sep+cfg.TILT_MOTOR_MODULE_NAME, str(tiltAngleDeg)]
            # Il seguente parametro serve ad evitare l'apertura della console ad ogni esecuzione dell'exe:
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags = subprocess.STARTF_USESHOWWINDOW
            # Avvio del processo: qui non serve modificare la current working directory, uso il comando semplice:
            self.tiltProcess=subprocess.call(inputArgs,startupinfo=startupinfo)
      
        
        
    # Metodi della sezione "PLAY" della GUI        
    def on_button_browse(self, event):
        """Apre una dialog per scegliere l'acquisizione."""
        open_dlg = wx.FileDialog(
            self, message=cfg.LABEL_CHOOSE_MOCAP_FILE,
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=cfg.STR_WILDCARD,
            style=wx.OPEN #| wx.CHANGE_DIR questo da problemi perche' modifica la directory corrente
            )
        # Se la dialog viene chiusa premendo OK:
        if open_dlg.ShowModal() == wx.ID_OK:
            # Se il file selezionato esiste:
            if os.path.isfile(open_dlg.GetPath()):
                # Prova a leggere le info dal file selezionato:
                if self.read_info_from_mocap_file(open_dlg.GetPath()):  
                    # Se riesce a leggerle, salva il percorso + nomefile nella variabile mocap_file_play:
                    cfg.mocap_file_play = open_dlg.GetPath()
                    # Aggiorna il text ctrl del mocap file da aprire:
                    self.stMocapFilePlay.SetLabel(textwrap.fill(cfg.mocap_file_play, 32))
                    # Aggiorna il contenuto dei vari campi testuali con le info sul mocap file:
                    self.UpdateStaticTexts()
                
        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        open_dlg.Destroy()
        
    def read_info_from_mocap_file(self, mocap_file_to_read):
        with open(mocap_file_to_read, 'r') as f:
            try:
                # Vecchio metodo
#                # Legge solo le righe dell'intestazione per evitare di leggere inutilmente tutto il mocap file:
#                line0=f.readline()
#                cfg.athletePlay=vsutils.extract_string_from_csv_line(line0,1)
#                line1=f.readline()
#                cfg.techniquePlay=vsutils.extract_string_from_csv_line(line1,1)
#                line2=f.readline()
#                cfg.weightPlay=float(vsutils.extract_string_from_csv_line(line2,1))
#                line3=f.readline()
#                cfg.height_cmPlay=float(vsutils.extract_string_from_csv_line(line3,1))
#                line4=f.readline()
#                cfg.genderPlay=vsutils.extract_string_from_csv_line(line4,1)
                
                # Nuovo metodo di riconoscimento stringhe nell'header del mocap file.
                # Lo scopo rendere flessibile la lettura anche in caso di aggiunta futura di nuovi
                # campi nell'header del mocap file. In questo modo il programma legge il nome dei vari campi
                # e salva il relativo valore nella corretta variabile.
                # Inizializza il contatore delle righe lette:
                n = 0
                # loop che avanza leggendo una intera riga alla volta:
                for line in f:
                    # Legge l'etichetta (1a colonna) dalla riga corrente:
                    line_label=vsutils.extract_string_from_csv_line(line, 0)
                    # Legge il valore (2a colonna) dalla riga corrente:
                    line_value=vsutils.extract_string_from_csv_line(line, 1)
                    # Confronta l'etichetta con quelle attese:
                    if line_label == cfg.LABEL_TRACKING_MODULE_VERSION:
                        cfg.this_trk_module_version=line_value
                    elif line_label == cfg.LABEL_ATHLETE_NAME:
                        cfg.athletePlay=line_value
                    elif line_label == cfg.LABEL_TECHNIQUE_NAME:
                        cfg.techniquePlay=line_value
                    # Nota: le stringhe di peso e altezza vanno convertite in float:
                    elif line_label == cfg.LABEL_ATHLETE_WEIGHT:
                        cfg.weightPlay=float(line_value)
                    elif line_label == cfg.LABEL_ATHLETE_HEIGHT:
                        cfg.height_cmPlay=float(line_value)
                    elif line_label == cfg.LABEL_ATHLETE_GENDER:
                        cfg.genderPlay=line_value
                    elif line_label == cfg.LABEL_START_MOCAP_DATA:
                        # Nel caso venga incontrata la stringa di inizio dati di tracciamento,
                        # il numero di righe lette viene salvato, aggiungendo 2 (linea corrente +
                        # linea di intestazione delle coordinate):
                        cfg.this_mocap_file_total_header_lines = n + 2
                        # ferma il ciclo di lettura dell'header:
                        break
                    n = n + 1                   
                
                infoRead = True
           
            except (ValueError, IndexError), exc_inst:
                # exc_inst e' un istanza dell'eccezione: contiene la stringa dell'errore.
                # ValueError: l'header del file ha un errore di valore (es: lettere al posto di numeri);
                # IndexError: l'header del file ha un errore di indice (es: una riga con meno di due colonne).
                vsutils.err_dlg(cfg.STR_MOCAP_FILE_HEADER_ERROR, exc_inst.__str__(), parent=self,
                               suggestion=cfg.STR_CHECK_MOCAP_FILE_HEADER)
                infoRead = False
            
        return infoRead

        
    def UpdateStaticTexts(self):
        """legge le informazioni dal mocap file.
        
        Viene richiamata quando l'utente seleziona un acquisizione da
        riprodurre. I dati numerici vengono prima convertiti in stringhe.
        """ 
        self.stAthletePlay.SetLabel(cfg.athletePlay)
        self.stTechniquePlay.SetLabel(cfg.techniquePlay)
        self.stWeightPlay.SetLabel(str(cfg.weightPlay))
        self.stHeight_cmPlay.SetLabel(str(cfg.height_cmPlay))
        self.stGenderPlay.SetLabel(cfg.genderPlay)
        
    def OnTextMocapFileRecChanged(self, event):
        cfg.mocap_file_rec = self.text_mocap_file_rec.GetValue()
    
    def OnTextAthleteRecChanged(self, event):
        cfg.athleteRec = self.textAthleteRec.GetValue()
        
    def OnTextTechniqueRecChanged(self, event):
        cfg.techniqueRec = self.textTechniqueRec.GetValue()   
        
    def OnSpinWeightRecChanged(self, event):
        # IMPORTANTE: occorre rendere float l'input dell'utente per evitare il rischio di
        # divisione per zero:
        cfg.weightRec=float(self.spinWeightRec.GetValue())
        
    def OnSpinHeight_cmRecChanged(self, event):
        # IMPORTANTE: occorre rendere float l'input dell'utente per evitare il rischio di
        # divisione per zero:
        cfg.height_cmRec=float(self.spinHeight_cmRec.GetValue())
    
    def OnChoiceGenderRec(self, event):
        # NOTA: GetSelection restituisce l'indice della selezione dell'utente,
        # ma a noi serve la stringa relativa:
        cfg.genderRec=self.genderList[self.choiceGenderRec.GetSelection()]




    ##########################################################################
    # Metodi relativi alla sezione PLAY della GUI                
    def on_button_open(self, event):
        """Avvia la funzione che esegue calcoli, plot e animazione."""
        # Se l'utente ha dimenticato di selezionare l'acquisizione o ne ha selezionata una non esistente compare
        # un messaggio di errore
        if os.path.isfile(cfg.mocap_file_play):
            # Apre il mocap file e calcola i risultati.
            # L'eccezione serve a gestire il caso in cui il mocap file sia
            # vuoto, cioe' contenga solo l'intestazione ma non le coordinate.
            try:
                self.results = vscalc2.ResultsData()                                                                  
         
            except (IOError, IndexError, TypeError, ValueError), exc_inst:
                # IOError: il mocap file e' vuoto;
                # IndexError: la matrice delle coordinate ha meno di due dimensioni;
                # TypeError: la matrice delle coordinate nel mocap file ha, ad esempio, dimensione errata;
                # ValueError: sono state cancellate alcune celle della matrice delle coordinate nel mocap file,
                # lasciando singole righe o colonne di dimensione errata;
                vsutils.err_dlg(cfg.STR_MOCAP_FILE_DATA_ERROR, exc_inst.__str__(), parent=self,
                               suggestion=cfg.STR_CHOOSE_ANOTHER_MOCAP_FILE)
                return      
            
            # Cambia il valore della variabile booleana dello stato di caricamento dell'acquisizione:
            cfg.is_loaded_mocap_file=True
            
            # TODO: giudizio bonta' acquisizione momentaneamente sospeso
            # Fornisce un giudizio sulla bonta' dell'aquisizione:
#            if self.good_frames_percentage>cfg.EXCELLENT_MOCAP_DATA:
#                acqQuality='excellent'
#            elif self.good_frames_percentage > cfg.GOOD_MOCAP_DATA and self.good_frames_percentage < cfg.EXCELLENT_MOCAP_DATA:
#                acqQuality='good'
#            elif self.good_frames_percentage > cfg.FAIR_MOCAP_DATA and self.good_frames_percentage < cfg.GOOD_MOCAP_DATA:
#                acqQuality='fair'
#            elif self.good_frames_percentage > cfg.POOR_MOCAP_DATA and self.good_frames_percentage < cfg.FAIR_MOCAP_DATA:
#                acqQuality='poor'
#            elif self.good_frames_percentage > cfg.POOR_MOCAP_DATA:
#                acqQuality='very poor'
                
            # Aggiorna la status bar visualizzando il giudizio sul mocap file nel secondo riquadro:
#           self.SetStatusText("Mocap file quality: %s (%.2f)"%(acqQuality,self.good_frames_percentage), 1)
            self.SetStatusText("Mocap file quality: unavailable", 1)
            
            # Abilita gli elementi della menubar inizialmente disabilitati:
            self.enable_disable_items()
            
            # Imposta il tempo iniziale a zero:
            self.f = 0
            
            # Crea il timer:
            self.timer = wx.Timer(self)
            self.Bind(wx.EVT_TIMER, self.TimerUpdate, self.timer)
            
            # Ultimo frame dell'acquisizione, pari al totale dei frame meno 1, perche' e' zero-based:
            self.last_frame = self.results.n_frm_interp - 1
            # Imposta il range dello spin ctrl del frame corrente:
            self.spin_goto.SetRange(0, self.last_frame)
            # Aggiorna lo static text contenente il numero dell'ultimo frame nella GUI:
            self.st_end_frame.SetLabel(" / %d"%self.last_frame)
            
            # Imposta il valore massimo dello sliderSeek pari al numero dell'ultimo frame (zero based):
            self.sliderSeek.SetMax(self.last_frame)
            # Reimposta i frame iniziale e finale di default di crop:
            cfg.crop_start_frame = 0
            cfg.crop_end_frame = self.last_frame
                            
            # Creazione scena 3D dell'animazione:
            # TODO: come input il file delle preferenze utente:
            self.anim = vsanim.Vs3dScene(parent=self)
            
            # Crea la finestra dei plot:
            self.plot_frame = vsplot2.PlotFrame(parent=self,
                                                id_num=wx.ID_ANY,
                                                title=cfg.SW_NAME +' - Plots',
                                                frm=self.f,
                                                ke_tot=self.results.ke_tot,
                                                v_stroke=self.results.v_stroke,
                                                a_stroke=self.results.a_stroke)        
            
            # Binding degli eventi del frame dei plot:
            self.plot_frame.Bind(wx.EVT_CLOSE, self.on_button_close)
            # TODO: Attualmente vpython non permette la gestione di un evento di chiusura, pertanto non
            # e' possibile collegare la chiusura dei grafici a quelle della scena 3d o viceversa.
            # occorre chiudere le due cose manualmente e separatamente.                                
                                                            
            # This function triggers the update of:
            # 1) the sliderSeek in the GUI;
            # 2) the spin ctrl that shows the current frame of the mocap file in the GUI;
            # 3) the 3D scene rendering.
            # 4) the plots;
            self.update_all()
            
            # Imposta il viewpoint correntemente selezionato nel menu view:
            self.on_menu_view(None)
                       
        # Se l'utente ha dimenticato di selezionare il mocap file o il file selezionato non esiste:    
        else:
            vsutils.err_dlg(cfg.STR_ERROR, cfg.STR_MOCAP_FILE_NOT_SELECTED_OR_EXISTENT, parent=self)
     

    def on_button_close(self, event):
        """Chiude l'acquisizione aperta e le relative finestre."""
        if cfg.is_loaded_mocap_file:
            # 1) Cambia lo stato di caricamento dell'acquisizione: in questo modo si evita che la funzione 
            # per il resizing dei plot cerchi ancora la figura che tra poco verra' cancellata.
            cfg.is_loaded_mocap_file = False
            # 2) Se sta scorrendo, viene fermato il timer:
            if self.timer.IsRunning():
                self.timer.Stop()
            # 3) Viene chiusa la finestra dei grafici:           
            self.plot_frame.Destroy()
            # 4) Vengono azzerati lo spin ctrl del frame corrente, lo static text dell'ultimo frame e ll sliderSeek:
            self.ResetSliderAndCurFrame()
            # 5) Viene resa invisibile la scena 3d: solo dopo di cio' e' possibile cancellarla dalla memoria:
            self.anim.scene.visible = False
            del self.anim.scene
            # 6) Aggiorna la status bar:
            self.SetStatusText("Mocap file quality:", 1)
            # 7) Cambia lo stato di abilitazione di alcuni item della GUI:
            self.enable_disable_items()
            # 8) Se il button play e' attivato, rimette il colore dello stato "disattivato":
            self.buttonPlay.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)
            # 9) Se l'utene aveva avviato la procedura di crop:
            if cfg.is_cropping:
                # resetta la relativa variabile di status:
                cfg.is_cropping=False
                # e distrugge la dialog di crop:
                self.cropDlg.Destroy()
                # non serve aggiornare i grafici perche' sono gia' stati distrutti.
            
  
    def OnCloseGui(self, event):
        """Chiude il programma e le sue sottoparti chiedendo prima conferma."""
        closeDlg = wx.MessageDialog(self, cfg.STR_REQUEST_EXIT, cfg.STR_CONFIRM, wx.OK | wx.CANCEL | wx.ICON_QUESTION)
        result = closeDlg.ShowModal()
        closeDlg.Destroy()
        if result == wx.ID_OK:
            # Prima chiude plot e animazione, se aperti:
            self.on_button_close(event)
            # Poi termina il processo di registrazione, se avviato:
            self.on_button_stop(None)
            # Poi chiude la finestra principale della GUI.
            self.Destroy()
            
    def OnCloseGuiSilently(self, event):
        """Chiude il programma e le sue sottoparti senza chiedere conferma."""
        # Prima chiude plot e animazione, se aperti:
        self.on_button_close(event)
        # Poi termina il processo di registrazione, se avviato:
        self.on_button_stop(None)
        # Poi chiude la finestra principale della GUI.
        self.Destroy()   

    
    # Metodi della sezione "PLAYER" della GUI:
    def ResetSliderAndCurFrame(self):
        # 1) Resetta a zero la posizione dello sliderSeek:
        self.sliderSeek.SetValue(0)
        # 2) Resetta a zero il valore dello spin ctrl contenente il frame corrente:
        self.spin_goto.SetValue(0)
        # 3) Resetta a zero il valore dello static text contenente l'ultimo frame:
        self.st_end_frame.SetLabel(' / 0')
    
    def update_all(self):
        """Aggiorna la GUI ad ogni fotogramma.
        
        This function trigger the update of:
        1) the sliderSeek in the GUI;
        2) the spin ctrl that shows the current frame in the GUI;
        3) the 3D scene rendering;
        4) the plots.
        """
        # 1) Aggiorna la posizione dello sliderSeek:
        self.sliderSeek.SetValue(self.f)
        # 2) Aggiorna lo spin ctrl contenente il tempo corrente:
        self.spin_goto.SetValue(self.f)
        # 3) Aggiorna l'animazione:
        self.anim.render_anim_frame(self.results.XYZj[self.f][:],
                                    self.results.XYZGbody[self.f][:],
                                    self.results.floor_data_interp[self.f][:])
        # 4) Aggiorna i cursori e le label sui plot:
        self.update_cursors_and_labels()
    
    
    def OnButtonLabels(self,event):
        """Cambia lo stato di visibilita' delle label."""
        if cfg.is_loaded_mocap_file:
            if not cfg.is_labels_visible:
                cfg.is_labels_visible = True
                self.buttonLabels.SetBackgroundColour(cfg.COLOR_SELECTED_BUTTON)
                self.menubar.check_labels.Check(True)
                # rende tutte le label visibili (e' una lista 2D):
                for tab in self.plot_frame.labels:
                    for label in tab:
                        label.set_visible(True)
                       
            elif cfg.is_labels_visible:
                cfg.is_labels_visible = False
                self.buttonLabels.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)
                self.menubar.check_labels.Check(False)
                # rende tutte le label invisibili (e' una lista 2D):
                for tab in self.plot_frame.labels:
                    for label in tab:
                        label.set_visible(False)
                
            # Aggiorna le label sui plot:
            self.update_cursors_and_labels()

    def on_button_export(self, event):
        """Permette di esportare i grafici e i dati dell'acquisizione."""
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            # Per prima cosa blocca il timer se sta scorrendo:
            if self.timer.IsRunning():
                self.timer.Stop()
                 
            # Possibili scelte
            exportOptions=['plots (.PDF)','plots (.EPS)','plots (.PNG)',
                           'kinetic energy data (.CSV)','speed data (.CSV)']
#                           'acceleration data (.CSV)']
            choiceDlg = wx.MultiChoiceDialog(self, "Choose what to export", "Export", exportOptions)
            
            if choiceDlg.ShowModal() == wx.ID_OK:
                selections = choiceDlg.GetSelections()
            else:
                selections = False               
            choiceDlg.Destroy()
            
            # Verifica se l'utente ha selezionato qualcosa da esportare:
            if selections:               
                # Se si', scelta percorso salvataggio:
                dir_dlg = wx.DirDialog(self, "Choose a directory to save:",
                          style=wx.DD_DEFAULT_STYLE
                           #| wx.DD_DIR_MUST_EXIST
                           #| wx.DD_CHANGE_DIR
                           )

                # If the user selects OK, then we process the dialog's data.
                # This is done by getting the path data from the dialog BEFORE
                # we destroy it. 
                if dir_dlg.ShowModal() == wx.ID_OK:
                    # Aggiunge alla fine del path il separatore del sistema
                    # operativo corrente (\ per windows):
                    save_dir = dir_dlg.GetPath()+os.sep
                else:
                    save_dir = False
                dir_dlg.Destroy()
                
                # Se l'utente ha scelto una directory in cui esportare:
                if save_dir:
                    # Data e ora correnti:
                    now = datetime.now()
                    # Nome di default per i file esportati:
                    export_file_name = (cfg.athletePlay + '_' +
                                      cfg.techniquePlay +
                                      '_%d-%d-%d_%d.%d.%d'%(now.day, now.month,
                                                            now.year, now.hour,
                                                            now.minute,
                                                            now.second))
                    
                    # Effettua gli opportuni salvataggi in relazione alle scelte fatte:
                    for s in selections:
                        # plots (.PDF)
                        if s == 0:
                            # Rende non animati cursori e label altrimenti non vengono stampati:
                            self.unanimate_cursors_labels()
                            # Esporta i grafici in un file .pdf:    
                            self.plot_frame.canvases[cfg.active_tab].print_figure(save_dir + export_file_name + '_plots.pdf',
                                                                                  format='pdf')
                            # Ripristina lo stato originario di animazione di cursori e label:
                            self.animate_cursors_labels()
                        
                        # plots (.EPS)
                        elif s == 1:
                            # Rende non animati cursori e label altrimenti non vengono stampati:
                            self.unanimate_cursors_labels()
                            # Esporta i grafici in un file .EPS:    
                            self.plot_frame.canvases[cfg.active_tab].print_figure(save_dir + export_file_name + '_plots.eps',
                                                                                  format='eps')
                            # Ripristina lo stato originario di animazione di cursori e label:
                            self.animate_cursors_labels()
                                
                        # plot (.PNG) 
                        elif s == 2:
                            # Rende non animati cursori e label altrimenti non vengono stampati:
                            self.unanimate_cursors_labels()
                            # Esporta i grafici in un file .png:    
                            self.plot_frame.canvases[cfg.active_tab].print_figure(save_dir + export_file_name + '_plots.png',
                                                                                  format='png')
                            # Ripristina lo stato originario di animazione di cursori e label:
                            self.animate_cursors_labels()
    
                        # kinetic energy data (.CSV)   
                        elif s == 3:
                            # Apre un file ASCII in scrittura, con estensione .csv:
                            fid1=open(save_dir + export_file_name + '_KE.csv', 'w')
                            fid1.write('Total kinetic energy [J]\n')
                            for value in self.results.ke_tot:
                                fid1.write('%.4f\n'%value)
                                
                            # Chiude il file:
                            fid1.close()
                        
                        # speed data (.CSV)     
                        elif s == 4:
                            # Apre un file ASCII in scrittura, con estensione .csv:
                            fid2 = open(save_dir + export_file_name + '_speed.csv', 'w')
                            fid2.write('LH speed [m/s],RH speed [m/s],LF speed [m/s],RF speed [m/s]\n')
                            for f in range(self.results.n_frm_interp):
                                fid2.write('%.4f,%.4f,%.4f,%.4f\n'%(self.results.v_stroke[f, 0],
                                                                    self.results.v_stroke[f, 1],
                                                                    self.results.v_stroke[f, 2],
                                                                    self.results.v_stroke[f, 3]))
                        # acceleration data (.CSV)     
                        elif s == 5:
                            # Apre un file ASCII in scrittura, con estensione .csv:
                            fid2=open(save_dir + export_file_name + '_accel.csv', 'w')
                            fid2.write('LH accel [m/s^2],RH accel [m/s^2],LF accel [m/s^2],RF accel [m/s^2]\n')
                            for f in range(self.results.n_frm_interp):
                                fid2.write('%.4f,%.4f,%.4f,%.4f\n'%(self.results.a_stroke[f, 0],
                                                                    self.results.a_stroke[f, 1],
                                                                    self.results.a_stroke[f, 2],
                                                                    self.results.a_stroke[f, 3]))
                                
                            # Chiude il file:
                            fid2.close()
                    
                    # Messaggio di conferma avvenuta esportazione:
                    vsutils.msg_dlg(cfg.STR_INFO, 'Correctly exported', parent=self)
    

    def OnButtonTrails(self,event):
        """Apre la Multichoice dialog per l'attivazione delle trail.""" 
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
                               
            # Crea la multiChoice dialog:
            jointsDlg = wx.MultiChoiceDialog(self, 
                                       "Select the joint of which you want to show the trajectories:",
                                       "Joints trajectories", cfg.JOINTS_NAMES)
                      
            # Mette il tick nelle caselle della dialog corrispondenti ai giunti
            # gia' selezionati:
            jointsDlg.SetSelections(cfg.joints_trail_on)
            
            # Quando l'utente preme OK l'array boolean e i giunti vengono comunque ricreati,
            # anche se non ha modificato le caselle scelte:
            if jointsDlg.ShowModal() == wx.ID_OK:
                # Legge la lista degli indici selezionati dall'utente dopo la
                # pressione del tasto OK:
                cfg.joints_trail_on = jointsDlg.GetSelections()
                        
                # Attiva/disattiva le trail secondo la scelta dell'utente.
                self.anim.joints_trails_switch()         

                # Se l'utente ha attivato la trail del COM, ma esso non e' visibile:
                if cfg.COM in cfg.joints_trail_on and not cfg.is_visible_com:
                    # Simula la pressione del tasto COM:
                    self.OnButtonCOM(None)
                    # In questa maniera, se il COM era gia' visibile, non
                    # avviene nulla perche' non si entra nell'if.
                    # Idem se l'utente disattiva la trail del COM.
                    # Se invece l'utente nasconde il COM, la trail resta
                    # visibile, ma va bene perche' e' una sua scelta.
                            
            # Chiude la multiChoice dialog:
            jointsDlg.Destroy()
   
    
    def OnButtonCOM(self,event):
        """Show/hide the COM (center of mass) in the 3d scene."""
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            # Verifica lo stato della variabile booleana che controlla la visibilita' del COM:
            if not cfg.is_visible_com:
                # ne inverte lo stato:
                cfg.is_visible_com = True
                # rende visibile il COM:
                self.anim.joints[cfg.COM].visible=True
                # Cambia colore del button: 
                self.buttonCOM.SetBackgroundColour(cfg.COLOR_SELECTED_BUTTON)
                # e mette il check sul relativo item della menubar:
                self.menubar.check_com.Check(True)
            # e viceversa: 
            elif cfg.is_visible_com:
                cfg.is_visible_com = False
                self.anim.joints[cfg.COM].visible = False
                self.buttonCOM.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)
                self.menubar.check_com.Check(False) 

          
                
    def OnButtonLoop(self, event):
        if not cfg.isLoop:
            cfg.isLoop = True
            # Cambia colore del button: 
            self.buttonLoop.SetBackgroundColour(cfg.COLOR_SELECTED_BUTTON)
            # e mette il check sul relativo item della menubar:
            self.menubar.check_loop.Check(True)  
        elif cfg.isLoop:
            cfg.isLoop = False
            # Cambia colore del button: 
            self.buttonLoop.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)    
            # e toglie il check sul relativo item della menubar:
            self.menubar.check_loop.Check(False)
            
    # TODO: cambia gestione visibilita' span      
    def on_button_crop(self, event):
        """Apre la dialog di crop."""
        if cfg.is_loaded_mocap_file:
            # Se l'utente non ha gia' avviato la procedura di cropping:
            if not cfg.is_cropping:
                # Cambia lo status di cropping:
                cfg.is_cropping = True
                
                # Crea un'istanza della classe crop dialog e la apre:
                self.cropDlg = VSCropDialog(parent=self, id_num=wx.ID_ANY)
                self.cropDlg.CenterOnScreen()
                self.cropDlg.Show()                                
                
            
    def on_spin_goto_changed(self,event):
        if cfg.is_loaded_mocap_file:
            # L'utente sceglie il frame corrente da visualizzare nello spin ctrl:
            self.f = self.spin_goto.GetValue()
            # Aggiorna GUI, animazione e plot al frame corrente:
            self.update_all()
     
            
    def on_spin_play_speed_changed(self,event):
        # Legge il valore immesso dall'utente per la velocita' di riproduzione:
        cfg.play_speed_percentage=self.spinPlaySpeed.GetValue()
        # Aggiorna lo step di avanzamento dei frame ad ogni scatto del timer:
        cfg.frames_step_at_timer_update=cfg.TIMER_PERIOD_FRAMES*cfg.play_speed_percentage/100.0
                
    
    # TODO: terribile, cercare di fare una funzione unica e a che serve skip? 
    def EnterButtonSaveAs(self, event): 
        # Visualizza la didascalia del pulsante Save As nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_SAVE_AS, 0)
        event.Skip()

    def EnterButtonRec(self, event):
        # Visualizza la didascalia del pulsante Rec nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_REC, 0)
        event.Skip() 
    
    def EnterButtonStop(self, event):
        # Visualizza la didascalia del pulsante Stop nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_STOP, 0)
        event.Skip()    
                
    def EnterButtonBrowse(self, event):
        # Visualizza la didascalia del pulsante open nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_BROWSE, 0)
        event.Skip()            
    
    def EnterButtonOpen(self, event):
        # Visualizza la didascalia del pulsante open nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_OPEN, 0)
        event.Skip()    
        
    def EnterButtonClose(self, event):
        # Visualizza la didascalia del pulsante close nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_CLOSE, 0)
        event.Skip()  
                                  
    def EnterButtonLabels(self, event):
        # Visualizza la didascalia del pulsante labels nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_LABELS, 0)
        event.Skip()
        
    def EnterButtonExport(self, event):
        # Visualizza la didascalia del pulsante export nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_EXPORT, 0)
        event.Skip()
        
    def EnterButtonTrails(self, event):
        # Visualizza la didascalia del pulsante trails nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_TRAILS, 0)
        event.Skip()
        
    def EnterButtonCOM(self, event):
        # Visualizza la didascalia del pulsante COM nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_COM, 0)
        event.Skip()  
        
    def EnterButtonLoop(self, event):
        # Visualizza la didascalia del pulsante loop nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_LOOP, 0)
        event.Skip()
        
    def EnterButtonCrop(self, event):
        # Visualizza la didascalia del pulsante crop nel primo riquadro della status bar:
        self.SetStatusText(cfg.TOOLTIP_CROP, 0)
        event.Skip()
            
    def LeaveButton(self, event):
        """"Quando il mouse esce dai widget, la didascalia relativa scompare dal primo riquadro della status bar."""
        self.SetStatusText('', 0)
        event.Skip()        
            
               
                       
    def unanimate_cursors_labels(self):
        """Rende non animati cursori e label nella tab corrente.
        
        Usata nell'esportazione dei grafici.
        """
        for c in self.plot_frame.cursors[cfg.active_tab]:
            c.set_animated(False)
        for l in self.plot_frame.labels[cfg.active_tab]:
            l.set_animated(False)
            
    def animate_cursors_labels(self):
        """Rende animati cursori e label nella tab corrente.

        Usata nell'esportazione dei grafici.
        """
        for c in self.plot_frame.cursors[cfg.active_tab]:
            c.set_animated(True)
        for l in self.plot_frame.labels[cfg.active_tab]:
            l.set_animated(True)
                
    
    def OnButtonStart(self, event):
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            self.f = 0
            # Aggiorna GUI, animazione e plot al frame corrente:
            self.update_all()
    
    def OnButtonStepB(self, event):
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            if self.f-1 >= 0:
                self.f = self.f - 1
                # Aggiorna GUI, animazione e plot al frame corrente:
                self.update_all()
        
    def OnButtonPlay(self, event):
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            if not self.timer.IsRunning():       
                # Avvia il timer:
                self.timer.Start(cfg.TIMER_PERIOD_MS)
                # Cambia colore del button: 
                self.buttonPlay.SetBackgroundColour(cfg.COLOR_SELECTED_BUTTON)
            else:
                # Arresta il timer:
                self.timer.Stop()
                # Cambia colore del button:
                self.buttonPlay.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON)         
    
    def OnButtonStepF(self, event):
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            if self.f+1 <= self.last_frame:
                self.f = self.f + 1
                # Aggiorna GUI, animazione e plot al frame corrente:
                self.update_all()
            
    def OnButtonEnd(self, event):
        # Verifica lo stato di caricamento dell'acqusizione:
        if cfg.is_loaded_mocap_file:
            self.f = self.last_frame
            # Aggiorna GUI, animazione e plot al frame corrente:
            self.update_all()
    
      
    def TimerUpdate(self, event):
        """Funzione eseguita ad ogni tick del timer."""
        # Se la modalita' loop non e' attiva e il tempo corrente e' >= del'ultimo frame
        # meno uno scatto del timer... 
        if not cfg.isLoop and self.f >= self.last_frame - cfg.frames_step_at_timer_update:
            # ... va direttamente all'ultimo frame: 
            self.f = self.last_frame
            self.timer.Stop()
            #self.buttonPlay.SetBitmapLabel(self.picPlay)
            # Cambia il colore del button Play:
            self.buttonPlay.SetBackgroundColour(cfg.COLOR_DESELECTED_BUTTON) 
            
        else:
            # Se invece stiamo in loop mode, usando l'operatore modulus (%, pari al resto
            # della divisione fra interi) il frame viene aumentato di uno scatto del timer.
            # Nel caso particolare di frame corrente=ultimo frame, il
            # risultato dell'operatore modulus e' 0, quindi il contatore ricomincia dall'inizio:
            curTime = self.f + cfg.frames_step_at_timer_update
            self.f = curTime % self.last_frame  
        # Aggiorna GUI, animazione e plot al frame corrente:
        self.update_all()        
        
        
    def OnSeek(self, event):
        """Verifica lo stato di caricamento dell'acqusizione"""
        if cfg.is_loaded_mocap_file:
            self.f = self.sliderSeek.GetValue()
            # 1) Aggiorna lo spin ctrl contenente il tempo corrente:
            self.spin_goto.SetValue(self.f)
            # 3) Aggiorna l'animazione:
            self.anim.render_anim_frame(self.results.XYZj[self.f][:],
                                        self.results.XYZGbody[self.f][:],
                                        self.results.floor_data_interp[self.f][:])
            # 3) Aggiorna i cursori e le label sui plot:
            self.update_cursors_and_labels()
                   
    
    def update_cursors_and_labels(self):
        """Aggiorna i cursori e le label dei plot.
        
        E' una funzione di comodo: evita di dover scrivere tutti gli argomenti
        ovunque occorra aggiornare la posizione di questi artist.
        In questo modo la manutenzione del codice e' semplificata.
        
        """
        # Aggiorna i cursori e le label dei plot nella tab visibile:
        self.plot_frame.updt_curs_and_lab_in_tab(self.f,
                                                 self.results.ke_tot[self.f],
                                                 self.results.v_stroke[self.f, :],
                                                 self.results.a_stroke[self.f, :])  
        
        
        
    def on_menu_open_folder(self, event):
        """Apre ed elabora un'intera cartella di mocap file."""
        # Scelta cartella con file da elaborare:
        dir_dlg = wx.DirDialog(self, "Choose the mocap files folder:",
                  style=wx.DD_DEFAULT_STYLE
                   #| wx.DD_DIR_MUST_EXIST
                   #| wx.DD_CHANGE_DIR
                   )
        
        if dir_dlg.ShowModal() == wx.ID_OK:
            folder = dir_dlg.GetPath()            
        else:
            folder = False
        dir_dlg.Destroy()
        
        if folder:
            files_list = glob.glob(os.path.join(folder, '*.csv'))     
            
            for infile in files_list:
                # Prova a leggere le info dal file selezionato:
                if self.read_info_from_mocap_file(infile):  
                    # Se riesce a leggerle, salva il percorso + nomefile nella variabile mocap_file_play:
                    cfg.mocap_file_play = infile
                    # Aggiorna il text ctrl del mocap file da aprire:
                    self.stMocapFilePlay.SetLabel(textwrap.fill(cfg.mocap_file_play, 32))
                    # Aggiorna il contenuto dei vari campi testuali con le info sul mocap file:
                    self.UpdateStaticTexts()
                    
                    # Apre il file selezionato
                    self.on_button_open(None)
                    
                    # Esporta i dati elaborati:
                    # Per prima cosa blocca il timer se sta scorrendo:
                    if self.timer.IsRunning():
                        self.timer.Stop()
                                                                        
                    # Nome di default per i file esportati:
                    export_file_name = (cfg.athletePlay + '_' +
                                      cfg.techniquePlay)
                        
#                    # plots (.PDF) 
#                    # Rende non animati cursori e label altrimenti non vengono stampati:
#                    self.unanimate_cursors_labels()
#                    # Esporta i grafici in un file .pdf:    
#                    self.plot_frame.canvases[cfg.active_tab].print_figure(folder + os.sep + 'PDF' + os.sep + export_file_name + '_plots.pdf',
#                                                                          format='pdf')
#                    # Ripristina lo stato originario di animazione di cursori e label:
#                    self.animate_cursors_labels()
#                    
#                    # plots (.EPS)
#                    # Rende non animati cursori e label altrimenti non vengono stampati:
#                    self.unanimate_cursors_labels()
#                    # Esporta i grafici in un file .EPS:    
#                    self.plot_frame.canvases[cfg.active_tab].print_figure(folder + os.sep + 'EPS' + os.sep + export_file_name + '_plots.eps',
#                                                                          format='eps')
#                    # Ripristina lo stato originario di animazione di cursori e label:
#                    self.animate_cursors_labels()
#                        
#                    # plot (.PNG) 
#                    # Rende non animati cursori e label altrimenti non vengono stampati:
#                    self.unanimate_cursors_labels()
#                    # Esporta i grafici in un file .png:    
#                    self.plot_frame.canvases[cfg.active_tab].print_figure(folder + os.sep + 'PNG' + os.sep + export_file_name + '_plots.png',
#                                                                          format='png')
#                    # Ripristina lo stato originario di animazione di cursori e label:
#                    self.animate_cursors_labels()

                    # kinetic energy data (.CSV)
                    # Apre un file ASCII in scrittura, con estensione .csv:
                    fid1=open(folder + os.sep + 'KE' + os.sep + export_file_name + '_KE.csv', 'w')
                    fid1.write('Total kinetic energy [J]\n')
                    for value in self.results.ke_tot:
                        fid1.write('%.4f\n'%value)
                        
                    # Chiude il file:
                    fid1.close()
                    
                    # speed data (.CSV)     
                    # Apre un file ASCII in scrittura, con estensione .csv:
                    fid2 = open(folder + os.sep + 'speed' + os.sep + export_file_name + '_speed.csv', 'w')
                    fid2.write('LH speed [m/s],RH speed [m/s],LF speed [m/s],RF speed [m/s]\n')
                    for f in range(self.results.n_frm_interp):
                        fid2.write('%.4f,%.4f,%.4f,%.4f\n'%(self.results.v_stroke[f, 0],
                                                            self.results.v_stroke[f, 1],
                                                            self.results.v_stroke[f, 2],
                                                            self.results.v_stroke[f, 3]))
#                    # acceleration data (.CSV)     
#                    # Apre un file ASCII in scrittura, con estensione .csv:
#                    fid2=open(folder + os.sep + 'acc' + os.sep + export_file_name + '_accel.csv', 'w')
#                    fid2.write('LH accel [m/s^2],RH accel [m/s^2],LF accel [m/s^2],RF accel [m/s^2]\n')
#                    for f in range(self.results.n_frm_interp):
#                        fid2.write('%.4f,%.4f,%.4f,%.4f\n'%(self.results.a_stroke[f, 0],
#                                                            self.results.a_stroke[f, 1],
#                                                            self.results.a_stroke[f, 2],
#                                                            self.results.a_stroke[f, 3]))
                        
                    # Chiude il file csv:
                    fid2.close()
                    
                    # Chiude il mocap file:
                    self.on_button_close(None)
                else:
                    # Lettura info mocap file non riuscita:
                    print ['Problema lettura info in ' + infile + '.']
                                    
                
            
         
                
        

##############################################################################
class VSPreferencesDlg(wx.Dialog):
    """Classe per la creazione della dialog delle preferenze utente"""
    def __init__(self, parent, id_num=wx.ID_ANY, title='Preferences'):
        wx.Dialog.__init__(self, parent, id_num, title)
        
        # Copia il config_dict corrente in uno temporaneo. Serve perche'
        # se l'utente chiude la dialog delle preferenze, viene ripristinato
        # il config_dict iniziale
        self.temp_config_dict = self.Parent.config_dict.copy() 

        st_trail_retain = wx.StaticText(self, label="Set trajectories lenght:")
        st_bg_color = wx.StaticText(self, label="Set background color:")
        st_links_color = wx.StaticText(self, label="Set links color:")
        st_joints_color = wx.StaticText(self, label="Set joints color:")
        st_ends_color = wx.StaticText(self, label="Set ends color:")
        st_com_color = wx.StaticText(self, label="Set COM color:")
        st_reset_default = wx.StaticText(self,
                                         label="Reset default preferences:")
        
        # Poiche' il trail_retain e' una variabile che non si usa mai nelle
        # divisioni, va bene che sia int:
        self.spin_trail_retain = wx.SpinCtrl(self, wx.ID_ANY, '',
                                             size=(50, -1))
        self.spin_trail_retain.SetRange(10, 100)
        # Imposta il valore di default visualizzato nello spin ctrl all'avvio:
        self.spin_trail_retain.SetValue(self.temp_config_dict["trail_retain"])
        
        # Creazione buttons per scelta colori animazione 3D:
        # I colori in vpython sono rappresentati da tuple in formato RGB
        # aritmetico (3 float compresi tra 0 e 1). Per poterli usare nelle
        # librerie wx occorre prima convertirli in formato RGB digitale 8 bit
        # (3 interi compresi tra 0 e 255):
        self.btn_bg_color = csel.ColourSelect(self, wx.ID_ANY, "",
                                              vsutils.arithm_2_dig_rgb(self.temp_config_dict["color_bg"]))
        self.btn_links_color = csel.ColourSelect(self, wx.ID_ANY, "",
                                                 vsutils.arithm_2_dig_rgb(self.temp_config_dict["color_links"]))
        self.btn_joints_color = csel.ColourSelect(self, wx.ID_ANY, "",
                                                  vsutils.arithm_2_dig_rgb(self.temp_config_dict["color_joints"]))
        self.btn_ends_color = csel.ColourSelect(self, wx.ID_ANY, "",
                                                vsutils.arithm_2_dig_rgb(self.temp_config_dict["color_ends"]))
        self.btn_com_color = csel.ColourSelect(self, wx.ID_ANY, "",
                                               vsutils.arithm_2_dig_rgb(self.temp_config_dict["color_com"]))
         
        # Buttons
        btn_ok = wx.Button(self, wx.ID_OK)
        btn_default = wx.Button(self, label='Default')
        
        # Crea il sizer principale che regola la disposizione dei widget nella finestra:
        pref_sizer = wx.GridBagSizer(8, 2)
        
        pref_sizer.Add(st_trail_retain, pos=(0, 0), flag=wx.ALL |
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(st_bg_color, pos=(1, 0), flag=wx.ALL | wx.ALIGN_RIGHT |
                       wx.ALIGN_CENTRE_VERTICAL, border=cfg.EMPTY_BORDER)
        pref_sizer.Add(st_links_color, pos=(2, 0), flag=wx.ALL |
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(st_joints_color, pos=(3, 0), flag=wx.ALL |
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(st_ends_color, pos=(4, 0), flag=wx.ALL |
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(st_com_color, pos=(5, 0), flag=wx.ALL |
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(st_reset_default, pos=(6, 0), flag=wx.ALL |
                       wx.ALIGN_RIGHT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)

        pref_sizer.Add(self.spin_trail_retain, pos=(0, 1), flag=wx.ALL |
                       wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(self.btn_bg_color, pos=(1, 1), flag=wx.ALL |
                       wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(self.btn_links_color, pos=(2, 1), flag=wx.ALL |
                       wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(self.btn_joints_color, pos=(3, 1), flag=wx.ALL |
                       wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(self.btn_ends_color, pos=(4, 1), flag=wx.ALL |
                       wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(self.btn_com_color, pos=(5, 1), flag=wx.ALL |
                       wx.ALIGN_LEFT | wx.ALIGN_CENTRE_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        pref_sizer.Add(btn_default, pos=(6, 1), flag=wx.ALL | wx.ALIGN_LEFT |
                       wx.ALIGN_CENTRE_VERTICAL, border=cfg.EMPTY_BORDER)
              
        pref_sizer.Add(btn_ok, pos=(7, 0), span=(1, 2), flag=wx.ALL |
                       wx.ALIGN_CENTRE, border=cfg.EMPTY_BORDER)
          
        # Assegna il GridBagSizer alla finestra di dialog delle preferenze:
        self.SetSizerAndFit(pref_sizer)
        
        # Binding degli eventi del menu preferences:
        self.Bind(wx.EVT_SPINCTRL, self.on_spin_trail_retain_changed,
                  self.spin_trail_retain)
        self.Bind(csel.EVT_COLOURSELECT, self.on_select_color_background,
                  self.btn_bg_color)
        self.Bind(csel.EVT_COLOURSELECT, self.on_select_color_links,
                  self.btn_links_color)
        self.Bind(csel.EVT_COLOURSELECT, self.on_select_color_joints,
                  self.btn_joints_color)
        self.Bind(csel.EVT_COLOURSELECT, self.on_select_color_ends,
                  self.btn_ends_color)
        self.Bind(csel.EVT_COLOURSELECT, self.on_select_color_com,
                  self.btn_com_color)
        self.Bind(wx.EVT_BUTTON, self.on_button_default, btn_default)
        self.Bind(wx.EVT_BUTTON, self.on_button_ok, btn_ok)
        
        self.CenterOnScreen()
        self.ShowModal()
               
    
    def on_spin_trail_retain_changed(self, event):
        """Seleziona la lunghezza delle trail dei giunti."""
        self.temp_config_dict["trail_retain"] = self.spin_trail_retain.GetValue()
        
    def on_select_color_background(self, event):
        """Seleziona il colore dello sfondo della scena 3D."""
        # I colori scelti vanno convertiti dal formato RGB digitale 8 bit
        # (0-255) a quello aritmetico (0-1) per essere letti da vpython.
        # Non so perche' ma GetValue() restituisce un quarto numero (inutile)
        # pari sempre a 255, oltre ai 3 canali RGB.
        self.temp_config_dict["color_bg"] = vsutils.dig_2_arithm_rgb(event.GetValue())
        
    def on_select_color_links(self, event):
        """Seleziona il colore dei link del manichino."""
        self.temp_config_dict["color_links"] = vsutils.dig_2_arithm_rgb(event.GetValue())

    def on_select_color_joints(self, event):
        """Seleziona il colore dei giunti del manichino."""
        self.temp_config_dict["color_joints"] = vsutils.dig_2_arithm_rgb(event.GetValue())

    def on_select_color_ends(self, event):
        """Seleziona il colore delle estremita' del manichino."""
        self.temp_config_dict["color_ends"] = vsutils.dig_2_arithm_rgb(event.GetValue())

    def on_select_color_com(self, event):
        """Seleziona il colore del COM."""
        self.temp_config_dict["color_com"] = vsutils.dig_2_arithm_rgb(event.GetValue())
 
    def on_button_default(self, event):
        """Ripristina i valori di default delle preferenze."""
        # Imposta i widget della dialog
        self.spin_trail_retain.SetValue(cfg.default_config_dict["trail_retain"])      
        self.btn_bg_color.SetValue(vsutils.arithm_2_dig_rgb(cfg.default_config_dict["color_bg"]))
        self.btn_links_color.SetValue(vsutils.arithm_2_dig_rgb(cfg.default_config_dict["color_links"]))
        self.btn_joints_color.SetValue(vsutils.arithm_2_dig_rgb(cfg.default_config_dict["color_joints"]))
        self.btn_ends_color.SetValue(vsutils.arithm_2_dig_rgb(cfg.default_config_dict["color_ends"]))
        self.btn_com_color.SetValue(vsutils.arithm_2_dig_rgb(cfg.default_config_dict["color_com"]))
        
        # Scrive i valori di default nel config_dict. Nota: occorre usare il comando copy()
        # altrimenti si crea un binding tra i due oggetti e non una copia: 
        self.temp_config_dict = cfg.default_config_dict.copy()        
        

    def on_button_ok(self, event):
        """Aggiorna le impostazioni e chiude la dialog."""
        # Copia il config dict temporaneo in quello principale
        self.Parent.config_dict = self.temp_config_dict.copy()
        # scrive il dict aggiornato nel config file
        vsutils.dict2json(self.Parent.config_dict, cfg.CONFIG_FILE_NAME)
        
        # Se aperta, aggiorna l'animazione:
        if cfg.is_loaded_mocap_file:
            self.Parent.anim.set_anim_from_config_file()
        self.Destroy()
        
        
##############################################################################
class VSCropDialog(wx.Dialog):
    """ Dialog per il cropping dell'acquisizione.
    
    Attributi:
    spinStartFrame
    spinEndFrame
    """
    def __init__(self, parent, id_num):
        wx.Dialog.__init__(self, parent, id_num, title='Drag to crop', size=wx.DefaultSize)
                       
        # Creazione widget della dialog:
        # Static text:
        textStartFrame = wx.StaticText(self, wx.ID_ANY, 'Start frame:')
        textEndFrame = wx.StaticText(self, wx.ID_ANY, 'End frame:')
        
        # Spin ctrl start frame:
        self.spinStartFrame = wx.SpinCtrl(self, wx.ID_ANY, "", size=(75, -1))
        self.spinStartFrame.SetRange(0, self.Parent.last_frame)
        # Mostra il valore di default dello start frame nello spin ctrl:
        self.spinStartFrame.SetValue(cfg.crop_start_frame)
    
        # Spin ctrl end frame:
        self.spinEndFrame = wx.SpinCtrl(self, wx.ID_ANY, "", size=(75, -1))
        self.spinEndFrame.SetRange(0, self.Parent.last_frame)
        # Mostra il valore di default dell'end frame nello spin ctrl::
        self.spinEndFrame.SetValue(cfg.crop_end_frame)
        
        # Buttons
        button_save_as = wx.Button(self, wx.ID_SAVEAS)
        
        # Sizer:
        crop_sizer = wx.GridBagSizer(3, 2)
        crop_sizer.Add(textStartFrame, pos=(0, 0), flag=wx.ALL |
                       wx.ALIGN_CENTRE | wx.ALIGN_CENTER_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        crop_sizer.Add(self.spinStartFrame, pos=(1, 0), flag=wx.ALL |
                       wx.ALIGN_CENTRE | wx.ALIGN_CENTER_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        crop_sizer.Add(textEndFrame, pos=(0, 1), flag=wx.ALL |
                       wx.ALIGN_CENTRE| wx.ALIGN_CENTER_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        crop_sizer.Add(self.spinEndFrame, pos=(1, 1), flag=wx.ALL |
                       wx.ALIGN_CENTRE | wx.ALIGN_CENTER_VERTICAL,
                       border=cfg.EMPTY_BORDER)        
        crop_sizer.Add(button_save_as, pos=(2, 0), span=(1,2), flag=wx.ALL |
                       wx.ALIGN_CENTRE | wx.ALIGN_CENTER_VERTICAL,
                       border=cfg.EMPTY_BORDER)
        # Assegna il GridBagSizer alla dialog:
        self.SetSizerAndFit(crop_sizer)
        
        # Binding degli eventi:
        self.Bind(wx.EVT_SPINCTRL, self.on_spin_start_frame_changed, self.spinStartFrame)
        self.Bind(wx.EVT_SPINCTRL, self.on_spin_end_frame_changed, self.spinEndFrame) 
        self.Bind(wx.EVT_BUTTON, self.on_button_save_as_crop, button_save_as)   
        self.Bind(wx.EVT_CLOSE, self.on_close_crop_dialog)
        for canvas in self.Parent.plot_frame.canvases:
            canvas.mpl_connect('button_press_event', self.on_click_to_crop)
            canvas.mpl_connect('motion_notify_event', self.on_drag_to_crop)
            canvas.mpl_connect('button_release_event', self.on_release_to_crop)
        
        # Mostra gli span sui plot:
        self.update_spans_and_redraw_plots()
        
        
    def interp2capture_frame(self, interpFrame):
        """Converte i frame dal framerate di interpolaz. a quello di cattura.
        
        Serve nella procedura di crop, poiche' l'utente seleziona i frame
        espressi in framerate di interpolazione, ma il cropping viene fatto
        sui file csv che sono espressi in quello di cattura.
        Il frame risultante va arrotondato e poi convertito a numero intero.
        
        """
        captureFrame = int(round(interpFrame * cfg.FRAMERATE_CAPTURE / cfg.FRAMERATE_INTERPOLATION))
        
        return captureFrame
          
     
    def on_close_crop_dialog(self, event):
        # Quando l'utente chiude la dialog di crop, la relativa variabile di stato viene impostata su false:
        cfg.is_cropping = False
        self.Destroy()
        # e gli span vengono aggiornati, o meglio in questo caso nascosti:
        self.update_spans_and_redraw_plots()
           
           
    def refresh_crop_spins(self):
        self.spinStartFrame.SetValue(cfg.crop_start_frame)
        self.spinEndFrame.SetValue(cfg.crop_end_frame)
        
    
    def update_spans_and_redraw_plots(self):
        """Ricrea gli span e ritraccia i plot."""
        # Poiche' non esiste una funzione per aggiornare l'estensione degli
        # span, conviene ricrearli ogni volta che vengono spostati:
        self.Parent.plot_frame.create_spans()
        self.Parent.plot_frame.restore_bgs_and_draw_artists()
    
     
    def on_click_to_crop(self, event):
        """L'utente clicca in un axes col primo button del mouse."""
        if cfg.is_cropping and event.inaxes in self.Parent.plot_frame.axes[cfg.active_tab] and event.button == 1:
            # Legge il frame in cui l'utente ha cliccato:
            frame = int(round(event.xdata))
            # Se l'utente ha cliccato in un frame maggiore dell'ultimo:
            if frame > self.Parent.last_frame:
                # Il frame selezionato diventa l'ultimo:
                frame = self.Parent.last_frame
            # Salva il frame selezionato come frame di inizio crop:
            cfg.crop_start_frame = frame
            
            # Aggiorna i valori mostrati dagli spin ctrl della dialog:
            self.refresh_crop_spins()
            # Aggiorna gli span:
            self.update_spans_and_redraw_plots()
            
            
    def on_drag_to_crop(self, event):
        """L'utente clicca e trascina in un axes col primo button del mouse."""
        if cfg.is_cropping and event.inaxes in self.Parent.plot_frame.axes[cfg.active_tab] and event.button == 1:
            # Legge il frame su cui l'utente sta trascinando il mouse:
            frame = int(round(event.xdata))
            # Se il frame selezionato e' maggiore dell'ultimo:
            if frame > self.Parent.last_frame:
                # Il frame selezionato diventa l'ultimo:
                frame = self.Parent.last_frame
            # Salva il frame selezionato come frame di fine crop: se anche fosse minore di quello
            # di inizio non c'e' problema perche' gli span sul plot gestiscono automaticamente il problema,
            # mentre i valori vengono invertiti al momento del rilascio del mouse:
            cfg.crop_end_frame = frame
            # Aggiorna gli span del crop nei plot durante il trascinamento:
            self.update_spans_and_redraw_plots()
 
 
    def on_release_to_crop(self,event):
        # Funzione avviata durante il cropping:
        if cfg.is_cropping:
            # Se il rilascio del primo button del mouse avviene dentro uno degli axes:
            if event.inaxes in self.Parent.plot_frame.axes[cfg.active_tab] and event.button == 1:
                # Legge il frame del punto di rilascio:
                frame = int(round(event.xdata))
                # Se l'utente ha trascinato il mouse su un frame maggiore dell'ultimo:
                if frame > self.Parent.last_frame:
                    # Il frame selezionato diventa l'ultimo:
                    frame = self.Parent.last_frame               
                # Se e' >= del frame di inizio crop:
                if frame >= cfg.crop_start_frame:
                    # il frame di rilascio diventa quello di fine crop:
                    cfg.crop_end_frame = frame
                else:
                    # Altrimenti il frame piu' grande diventa il finale:
                    cfg.crop_end_frame = cfg.crop_start_frame
                    # e quello di rilascio diventa l'iniziale:
                    cfg.crop_start_frame = frame
                    
            # Se invece il rilascio avviene fuori degli axes:
            else:
                # Non fa niente e usa come frame finale l'ultimo buono ottenuto dal trascinamento,
                # a meno che, se il frame finale e' maggiore dell'iniziale li inverte:
                if cfg.crop_end_frame < cfg.crop_start_frame:
                    appo=cfg.crop_end_frame
                    cfg.crop_end_frame = cfg.crop_start_frame
                    cfg.crop_start_frame = appo
     
            # Aggiorna i valori mostrati dagli spin ctrl della dialog:
            self.refresh_crop_spins()
            # Aggiorna gli span:
            self.update_spans_and_redraw_plots()
    
       
    def on_spin_start_frame_changed(self, event):
        # Quando l'utente modifica il frame di inizio crop, legge il valore inserito:
        frame = self.spinStartFrame.GetValue()
        # Se il frame selezionato e' maggiore dell'ultimo:
        if frame > self.Parent.last_frame:
            # Il frame selezionato diventa l'ultimo:
            frame = self.Parent.last_frame
        # Salva il frame selezionato come frame di inizio crop:
        cfg.crop_start_frame = frame
        
        # Se il frame di inizio crop e' maggiore del frame di fine crop:
        if cfg.crop_start_frame > cfg.crop_end_frame:
            # Imposta il frame di fine crop pari a quello di inizio crop
            cfg.crop_end_frame = cfg.crop_start_frame
            # e aggiorna entrambi gli spin ctrl:
            self.refresh_crop_spins()  
        # Aggiorna gli span:
        self.update_spans_and_redraw_plots()
        
        
    def on_spin_end_frame_changed(self, event):
        # Quando l'utente modifica il frame di fine crop, legge il valore inserito:
        frame = self.spinEndFrame.GetValue()
        # Se il frame selezionato e' maggiore dell'ultimo:
        if frame > self.Parent.last_frame:
            # Il frame selezionato diventa l'ultimo:
            frame = self.Parent.last_frame
        # Salva il frame selezionato come frame di fine crop:
        cfg.crop_end_frame = frame
        # Se il frame di fine crop e' minore del frame di inizio crop:
        if cfg.crop_end_frame < cfg.crop_start_frame:
            # Imposta il frame di inizio crop pari a quello di fine crop
            cfg.crop_start_frame = cfg.crop_end_frame
            # e aggiorna entrambi gli spin ctrl:
            self.refresh_crop_spins() 
        # Aggiorna gli span:
        self.update_spans_and_redraw_plots()
        
        
    def on_button_save_as_crop(self, event):
        """Scelta del percorso di salvataggio dell'acquisizione ritagliata."""
        save_as_dlg = wx.FileDialog(self, message=cfg.STR_SAVE_AS,
            defaultDir=os.getcwd(), 
            defaultFile=cfg.mocap_file_play,
            wildcard=cfg.STR_WILDCARD,
            style=wx.SAVE | wx.FD_OVERWRITE_PROMPT
            #| wx.CHANGE_DIR questo da problemi perche' modifica la directory corrente
            )
        
        if save_as_dlg.ShowModal() == wx.ID_OK:
            # Salva il percorso+nomefile nella variabile mocapFile:
            cropped_file = save_as_dlg.GetPath()
            # Richiama la funzione che effettivamente scrive il file ritagliato:
            self.save_cropped_file(cropped_file)

        save_as_dlg.Destroy()   
        
        
        #TODO: MODIFICARE: legge tutto il file a buffo
    def save_cropped_file(self, cropped_file):
        # Apre il file dell'acquisizione originale in modalita' lettura
        # (il comando with e' comodo perche' chiude automaticamente il
        # file aperto, anche se dovesse verificarsi un errore).
        # La variabile mocap_file_play e' globale perche' viene automaticamente
        # sovrascritta all'apertura di un nuovo mocap file:
        with open(cfg.mocap_file_play, 'r') as f1:
                # Legge tutto il file salvando le righe come lista di stringhe:
                lines = f1.readlines()

        # Apre un nuovo file nella destinazione scelta dall'utente in
        # modalita' scrittura. La variabile cropped_file e' locale per
        # evitare indesiderate sovrascritture dei mocap file.
        with open(cropped_file, 'w') as f2:
            # Scrive in esso l'intestazione copiandola dal file originale:
            f2.writelines(lines[0:cfg.this_mocap_file_total_header_lines])
            # Occorre tenere conto delle linee riservate all'intestazione per
            # effettuare il cropping del file originale. Inoltre occorre
            # convertire i frame di inizio e fine crop scelti dall'utente dal
            # framerate di interpolazione a quello di cattura:
            startLine = (cfg.this_mocap_file_total_header_lines +
                         self.interp2capture_frame(cfg.crop_start_frame))
            endLine = (cfg.this_mocap_file_total_header_lines +
                       self.interp2capture_frame(cfg.crop_end_frame))
            # Scrive le righe selezionate dall'utente durante il cropping:
            f2.writelines(lines[startLine:endLine])
     


####################################################################
# MAIN che esegue la GUI principale di VIRTUAL SENSEI LITE  
if __name__ == '__main__':    
    #False evita che stderr e stdout vadano in una finestra separata:
    app = wx.App(False) 
    # Costruisce il frame della GUI usando la classe VSGui:
    vs_main_frame = VSFrame(parent=None, id_num=wx.ID_ANY,
                            title=cfg.SW_NAME+' v.'+cfg.SW_VERSION)
    # Mostra il frame costruito:
    vs_main_frame.Show(True)
    # Esegue alcune operazioni necessarie all'avvio:
    vs_main_frame.on_startup()   
    # Avvia il ciclo interno dell'applicazione,
    # necessario per far funzionare tutto:    
    app.MainLoop()
    
    
    