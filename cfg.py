'''
Created on 18/lug/2011

@author: Alessandro Timmi
'''
import os

# COSTANTI E VALORI DI DEFAULT
SW_COMPANY_NAME = 'Virtual Sensei'
SW_NAME = 'Virtual Sensei Lite'
SW_AUTHOR = 'Alessandro Timmi'
SW_DESCRIPTION = 'Discover your kinetic energy'
SW_VERSION = '0.2.4'
SW_COPYRIGHT = 'Copyright (C) 2011-2012 Alessandro Timmi'
SW_LICENSE_FILE = 'LICENSE.txt'

SW_EMAIL = 'info@virtualsensei.it'
SW_URL = 'http://www.virtualsensei.it/lite/'
SW_ONLINE_TUTORIALS = 'http://www.virtualsensei.it/tutorials/'
SW_ONLINE_HELP = SW_URL+'help/'
SW_DOWNLOAD_PAGE = 'http://www.virtualsensei.it/downloads/'
SW_CHANGELOG_PAGE = 'http://www.virtualsensei.it/lite/changelog/'
SW_ONLINE_LICENSE_FILE = SW_URL+SW_LICENSE_FILE
URL_JSON_CUR_VERS = SW_URL+'vslite_cur_vers.json'

SW_FACEBOOK_PAGE = 'http://www.facebook.com/virtualsensei'
SW_YOUTUBE_CHANNEL = 'http://www.youtube.com/user/alexkarate'
SW_TWITTER_CHANNEL = 'http://twitter.com/virtualsensei'
SW_GOOGLE_PLUS_PAGE = 'https://plus.google.com/106567231597963880220'

SW_SHORT_NAME = 'VSLite'
SW_ICON = SW_SHORT_NAME+'.ico'
SW_LOGO = SW_SHORT_NAME+'Logo.png'
SW_MAIN_SCRIPT = SW_SHORT_NAME+'.py'
SW_GRAPHICS_FOLDER = 'icons'
SW_MODULES_FOLDER = 'modules'
SW_LEGAL_FOLDER = 'legal'


TRACKING_MODULE_NAME = 'vsltrk.exe'
TILT_MOTOR_MODULE_NAME = 'vsltlt.exe'
SW_README_FILE = 'README.txt'
SW_CHANGES_FILE = 'CHANGES.txt'

# Poiche' la cartella "Program Files" di Windows e' bloccata per utenti non 
# amministratori, serve una cartella accessibile anche da utenti standard,
# in cui il software possa salvare dei file che non siano relativi all'utente,
# ma accessibili da qualunque account del pc.
SW_DATA_FOLDER = os.environ['ALLUSERSPROFILE']+os.sep+SW_NAME

# Py2exe per default dirotta l'output degli errori in un .log file
# con stesso nome del main e nella stessa cartella: tuttavia nel
# nostro caso la cartella dove verra' installato il software non ha
# permessi di scrittura. Pertanto occorre deviare il .log su una cartella
# con permessi di scrittura. Pertanto il seguente file viene utilizzato
# all'interno del modulo boot_common.py di py2exe (appositamente modificato)
# collocato nella cartella py2exe tra i site-packages di python.
STD_ERR_FILE = SW_DATA_FOLDER+os.sep+SW_SHORT_NAME+'.log'


# File delle impostazioni: poiche' deve essere modificato dal software,
# deve essere accessbile in scrittura:
CONFIG_FILE_NAME = SW_DATA_FOLDER+os.sep+'config.json'

# Questo file non deve essere presente all'interno del Setup,
# ma deve essere creato dinamicamente dal software, in modo che al
# momento della disinstallazione non venga rimosso. Inoltre
# deve essere accessbile in scrittura dal software.
ACTIVATION_FILE_NAME = SW_DATA_FOLDER+os.sep+'activation.json'
VS_SMTP_SERVER = 'mail.virtualsensei.it'
VS_SMTP_USER = 'virtualsensei.it32081'
VS_SMTP_PASS = 'und3rc0v3r'


# Property id dell'account analytics:
ANALYTICS_ID = 'UA-15625775-2'
# Pagina in cui si e' originato l'evento, in questo caso e' fittizia perche'
# l'evento e' generato dal software:
ANALYTICS_PAGE = '/software-updater/'


# Kinect tilt: in realta' il limite indicato nelle specifiche tecniche e'
# +/-27 gradi:
KINECT_ZERO_TILT_DEG = 0
KINECT_UPPER_TILT_LIMIT_DEG = 23
KINECT_LOWER_TILT_LIMIT_DEG = -23

# Stringhe delle tooltip che appaiono quando si passa il mouse sui buttons:
TOOLTIP_SAVE_AS = 'Save new recording as'
TOOLTIP_REC = 'Record a new mocap file'
TOOLTIP_STOP = 'Stop current recording'
TOOLTIP_ABOUT = 'About '+SW_NAME
TOOLTIP_BROWSE = 'Browse mocap files'
TOOLTIP_OPEN = 'Open selected mocap file'
TOOLTIP_CLOSE = 'Close currently loaded mocap file'
TOOLTIP_LABELS = 'Show/hide plots labels'
TOOLTIP_EXPORT = 'Export plots data'
TOOLTIP_TRAILS = 'Show/hide joints trails'
TOOLTIP_COM = 'Show/hide body center of mass'
TOOLTIP_LOOP = 'Enable/disable loop mode'
TOOLTIP_CROP = 'Crop currently loaded mocap file'

# Stringhe usate nell'header del mocap file, nella GUI o nei message dialogs:
LABEL_ATHLETE_NAME = "Athlete's name"
LABEL_TECHNIQUE_NAME = "Technique name"
LABEL_ATHLETE_WEIGHT = "Athlete's weight [kg]"
LABEL_ATHLETE_HEIGHT = "Athlete's height [cm]"
LABEL_ATHLETE_GENDER = "Athlete's gender"
LABEL_START_MOCAP_DATA = "Start mocap data"
LABEL_RECORD_NEW_MOCAP_FILE = "Record a new mocap file"
LABEL_SAVE_NEW_RECORDING_AS = "Save new recording as"
LABEL_PLAY_RECORDED_MOCAP_FILE = "Play a recorded mocap file"
LABEL_CHOOSE_MOCAP_FILE = "Choose a mocap file"
LABEL_BROWSE = "Browse"
LABEL_TRACKING_MODULE_VERSION = "Tracking module version"
LABEL_TILT = "Tilt [deg]"

# Messaggi ed errori richiamati dalle varie eccezioni:
STR_ERROR = "Error"
STR_INFO = "Info"
STR_SAVE_AS = "Save as..."
STR_CONFIRM = "Confirm"
STR_MOCAP_FILE_HEADER_ERROR = "Mocap file header error"
STR_CHECK_MOCAP_FILE_HEADER = "Please, check mocap file header."
STR_MOCAP_FILE_DATA_ERROR = "Mocap file data error"
STR_CHOOSE_ANOTHER_MOCAP_FILE = "Please, choose another mocap file."
STR_CHECK_YOUR_INTERNET_CONNECTION = "Please, check your internet connection."
STR_REQUEST_OVERWRITE = "File already exists. Overwrite?"
STR_REQUEST_EXIT = "Do you really want to exit?"
STR_FILE_NOT_SELECTED = "destination file not selected"
STR_CLICK_ON_SAVE_AS = 'Please, click on "Save as..." button.'
STR_WILDCARD = SW_NAME+" mocap data (*.csv)|*.csv"
STR_MOCAP_FILE_NOT_SELECTED_OR_EXISTENT = "Mocap file not selected or not existent"
STR_DOWNLOAD_CANCELLED_USER = "Download cancelled by the user"
STR_EDIT_PREFERENCES = 'Edit preferences'
STR_TRY_AGAIN_LATER = 'Please, try again later.'


# Dimensioni iniziali del frame della GUI:
VS_GUI_SIZE = (680,620)
# Dimensioni iniziali del frame dei plot:
PLOT_FRAME_DEFAULT_SIZE = (600, 600)

# !!!! IMPORTANTE !!!!!
# Framerate di default (30 fps per il Kinect): serve per impostare la corretta
# velocita' di riproduzione dell'animazione e il passo di derivazione:
FRAMERATE_CAPTURE = 30.0
# Periodo tra due frame consecutivi durante l'acquisizione (1/30 s nel Kinect):
DT_CAPTURE = 1 / FRAMERATE_CAPTURE

# Framerate di INTERPOLAZIONE, maggiore di quello di acquisizione:
FRAMERATE_INTERPOLATION = 100.0
# Periodo tra un frame e l'altro in fase di INTERPOLAZIONE (1/100 s):
DT_INTERPOLATION = 1 / FRAMERATE_INTERPOLATION


# Parametri filtri:
MEDIAN_FILTER_SPAN = 5
SGOLAY_ORD = 3
SGOLAY_SPAN = 7


# Parametro di smoothing relativo all'interpolazione tramite spline.
# Dalle prove fatte
# s=1e-9*nFrames_acquisition
# costituisce il limite inferiore: in corrispondenza di questo valore si raggiunge
# il massimo numero di iterazioni nella valutazione della spline:
# Il limite superiore per un corretto smoothing sembra:
# s=1e-5*nFrames_acquisition
# Con questo valore i picchi delle velocita' vengono troppo ammorbiditi (cfr. prove fatte)
# s=1e-6*nFrames_acquisition sembra il valore ideale.
# NOTA: usando il filtro Butterworth (molto smooth) non serve piu' lo smoothing della spline:
SMOOTH = 0.0
#SMOOTH=1e-6

# Periodo del timer (in ms) che gestisce l'aggiornamento dell'animazione e dei plot.
# Non influenza i calcoli e l'interpolazione. Serve al momento della creazione
# del timer, cioe' quando si preme play:
TIMER_PERIOD_MS = 40.0   # 40 ms corrispondono a 25 fps
# Periodo del timer espresso in frame di interpolazione.
# Formula di conversione:
# periodo[frame] = periodo[ms] * (1/1000)[s/ms] * framerate_di_interpolazione[frame/s]
TIMER_PERIOD_FRAMES = TIMER_PERIOD_MS / 1000.0 * FRAMERATE_INTERPOLATION


# Valori di soglia per giudicare la qualita' delle acquisizioni in base al numero di frame buoni
# sul totale (buoni + mancanti):
EXCELLENT_MOCAP_DATA = 0.9
GOOD_MOCAP_DATA = 0.7
FAIR_MOCAP_DATA = 0.6
POOR_MOCAP_DATA = 0.4

# Bordo vuoto intorno ai vari widget:
EMPTY_BORDER = 5

# Rapporto tra lato minore frame dei plot e font dei plot:
PLOT_FONT_DIVISOR = 55

##############################################################################
# SKELETON
# Numero di parametri che definisco posizione e orientamento di ogni giunto
# (3 coordinate + 9 elementi della matrice di rotazione):
N_PARAMETERS_PER_JOINT = 12

# Numero di giunti:
N_JOINTS = 15
# Numero di giunti (compreso il COM):
N_JOINTS_PLUS_COM = 16
# Numero di link costituienti lo scheletro:
N_LINKS = 16

# ID dei giunti (anche il centro di massa (COM) del corpo umano
# e' considerato come un giunto):
HEAD = 0
NECK = 1
TORSO = 2
L_SHOULDER = 3
L_ELBOW = 4
L_HAND = 5
R_SHOULDER = 6
R_ELBOW = 7
R_HAND = 8
L_HIP = 9
L_KNEE = 10
L_FOOT = 11
R_HIP = 12
R_KNEE = 13
R_FOOT = 14
COM = 15

# Lista di nomi dei giunti visualizzata nel multiChoice dialog:   
JOINTS_NAMES = ['Head', 'Neck', 'Torso', 'Left shoulder', 'Left elbow',
              'Left hand', 'Right shoulder', 'Right elbow', 'Right hand',
              'Left hip', 'Left knee', 'Left foot', 'Right hip', 'Right knee',
              'Right foot', 'COM']

# ID dei link:
HEAD_NECK = 0
NECK_L_SHOULDER = 1
L_SHOULDER_L_ELBOW = 2
L_ELBOW_L_HAND = 3
NECK_R_SHOULDER = 4
R_SHOULDER_R_ELBOW = 5
R_ELBOW_R_HAND = 6
L_SHOULDER_TORSO = 7
R_SHOULDER_TORSO = 8
TORSO_L_HIP = 9
TORSO_R_HIP = 10
L_HIP_L_KNEE = 11
L_KNEE_L_FOOT = 12
R_HIP_R_KNEE = 13
R_KNEE_R_FOOT = 14
L_HIP_R_HIP = 15

# Lista di tuple contenenti gli ID dei giunti collegati da ogni link:
LINKED_JOINTS = ((HEAD, NECK),
                 (NECK, L_SHOULDER),
                 (L_SHOULDER, L_ELBOW),
                 (L_ELBOW, L_HAND),
                 (NECK, R_SHOULDER),
                 (R_SHOULDER, R_ELBOW),
                 (R_ELBOW, R_HAND),
                 (L_SHOULDER, TORSO),
                 (R_SHOULDER, TORSO),
                 (TORSO, L_HIP),
                 (TORSO, R_HIP),
                 (L_HIP, L_KNEE),
                 (L_KNEE, L_FOOT),
                 (R_HIP, R_KNEE),
                 (R_KNEE, R_FOOT),
                 (L_HIP, R_HIP))

# Raggio delle sfere che rappresentano i giunti:
RADIUS = 0.03   

# Crea una lista contenente il raggio di ogni giunto (compreso il COM):
RADII = [3.0*RADIUS,
         RADIUS,
         RADIUS,
         RADIUS,
         RADIUS,
         1.5 * RADIUS,
         RADIUS,
         RADIUS,
         1.5 * RADIUS,
         RADIUS,
         RADIUS,
         1.5 * RADIUS,
         RADIUS,
         RADIUS,
         1.5 * RADIUS,
         RADIUS]

# Numero di parametri che definisco posizione e orientamento del pavimento
# (3 coordinate di un punto + 3 coseni direttori della normale)
N_PARAMETERS_FLOOR = 6

# Dimensioni pavimento:
FLOOR_THICKNESS = 0.01
FLOOR_DEPTH = 7
FLOOR_WIDTH = 6

# Colore dei button selezionati:
COLOR_SELECTED_BUTTON = [72, 216, 251]
# Colore dei button deselezionati:
COLOR_DESELECTED_BUTTON = [255, 255, 255]

# Opacita' vettori riferimento assoluto
OPACITY_WORLD_AXIS = 0.5
# Opacita' pavimento quando e' reso trasparente:
OPACITY_FLOOR_TRANSPARENT = 0.3
# Lunghezza vettori riferimento assoluto:
LEN_WORLD_AXIS = 0.7


###############################################################################
# CREAZIONE VARIABILI GLOBALI CONTENENTI I VALORI DI DEFAULT:

# Numero di righe dell'header. Viene letto all'apertura del mocap file e
# permette di adeguarsi a file con header diversi, purche' sia presente la
# corretta stringa di inizio dati:
this_mocap_file_total_header_lines = '-'
# Versione del modulo di tracciamento usata nell'acquisizione corrente:
this_trk_module_version = '-'

# Variabile booleana che rappresenta lo stato di esecuzione dell'exe che
# registra un nuovo mocap file:
is_recording = False

# Valore di default della velocita' di riproduzione in percentuale, modificabile
# dall'utente. Influenza esclusivamente la velocita' di riproduzione dell'animazione
# 3D e dei plot, non i calcoli:
play_speed_percentage = 100.0

# Numero di frame di cui avanza la riproduzione ad ogni scatto del timer (valore di default).
# E' funzione del periodo del timer espresso in [frame] e della velocita' di riproduzione
# scelta dall'utente. In questo modo quando la velocita' di riproduzione e' 100%
# si ha un framerate uguale a quello di interpolazione.
# Non e' necessario che sia un intero perche' numpy (su cui anche vpython si basa)
# gestisce gli array in modo che, se l'indice richiesto e' con la virgola, viene
# arrotondato all'intero precedente: questo fatto permette un controllo fine della
# velocita' di riproduzione in quanto si ha un avanzamento reale solo quando il frame
# attuale supera la soglia del numero intero successivo.
frames_step_at_timer_update = TIMER_PERIOD_FRAMES * play_speed_percentage / 100.0

# Variabile booleana che contiene lo stato di caricamento del mocap file:
# quando e' False (ad esempio quando si avvia il programma), i pulsanti della sezione
# "Player" della GUI sono disabilitati:
is_loaded_mocap_file = False

# Valore iniziale della variabile mocap_file_rec:
mocap_file_rec = False
# Valore iniziale della variabile mocap_file_play:
mocap_file_play = 'Click on "Browse"'

# Valore iniziale della variabile athlete:
athleteRec = 'no athlete\'s name'
athletePlay = '-'

# Valore iniziale della variabile technique:
techniqueRec = 'no technique name'
techniquePlay = '-'

# Valore iniziale della variabile weight:
# Si vuole che la variabile sia float per evitare problemi di divisione per zero.
weightRec = 75.0
weightPlay = '-'

# Valore iniziale della variabile height_cm:
# Si vuole che la variabile sia float per evitare problemi di divisione per zero.
height_cmRec = 186.0
height_cmPlay = '-'

# Valore iniziale della variabile gender:
genderRec = False
genderPlay = '-'

# Variabile booleana che controlla la visibilita' delle labels.
# Si vuole che all'avvio del programma siano invisibili:
is_labels_visible = False

# Variabile booleana che regola lo stato di visibilita' del centro di massa
# del corpo umano (COM); per default e' invisibile.
is_visible_com = False

# Variabile booleana che regola lo stato di trasparenza del pavimento
# per default e' opaco.
is_transp_floor = False

# Variabile booleana della modalita' loop:
isLoop = False

joints_trail_on = []

# Variabile booleana relativa all'operazione di cropping: indica se l'utente sta
# facendo o meno il cropping e serve a visualizzare/nascondere gli span sui
# grafici.
is_cropping = False
# Valori di default delle ascisse degli span di cropping:
crop_start_frame = 0
crop_end_frame = 0

# Numero della tab correntemente selezionata nei plot:
active_tab = 0


###############################################################################
# IMPOSTAZIONI (O PREFERENZE) DI DEFAULT CHE ANDRANNO NEL CONFIG FILE
default_config_dict = {"trail_retain": 30,  
                       "color_bg": [0, 0, 0],
                       "color_links": [1, 1, 1],
                       "color_joints": [0, 0, 1],
                       "color_ends": [1, 0, 0],
                       "color_com": [0, 1, 0]
                       }
