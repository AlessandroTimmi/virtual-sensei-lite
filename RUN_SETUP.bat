rem *** Used to run the file setup.py:

rem **** Cancella la directory (rd) voluta compreso
rem **** il contenuto (/S) senza chiedere conferma (/Q)
rd /S /Q dist

rem **** Esegue lo script che crea la distribution
python setup.py py2exe

rd /S /Q build

rem **** Pause, so we can see the exit codes
pause "done...hit a key to exit"