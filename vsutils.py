'''
Created on 13/ago/2011

@author: Alex
'''
import json
from hashlib import sha1 #IGNORE:E0611
import smtplib
from email.mime.text import MIMEText # per comporre testo mail
from random import randint
from urllib import urlencode
import urllib2
from urlparse import urlunparse
import socket #importato esclusivamente per gestire exception smtp
import os

import wx

import cfg

def arithm_2_dig_rgb(arithm):
    """Converte numeri RGB da (1.0, 0.0, 0.0) a (255, 0, 0)."""
    dig = [x * 255.0 for x in arithm] 
    # Aggiunge un quarto valore pari sempre a 255, come richiesto dal formato
    # di input/output dei button di scelta colore (vedi dialog preferences):
    dig.append(255.0) 
    return dig


def dig_2_arithm_rgb(dig):
    """Converte numeri RGB da (255, 0, 0) a (1.0, 0.0, 0.0).
    
    il .0 e' importante nel caso in cui si abbia dig[int, int, int]:
    dividendo per un int si avrebbe un intero per risultato (cioe' 0).
    Occorre togliere il quarto valore dall'array di input, che e' pari sempre a 255,
    ma e' inutile per descrivere il colore:
    """
    arithm = [x / 255.0 for x in dig[0:3]]      
    return arithm


def extract_string_from_csv_line(line, str_pos):
    """Estrae una stringa da una riga di un file csv.
    

    Questa funzione accetta come input una stringa contenente un'intera linea
    (riga) di un file .csv e ne estrae la cella posizionata nella colonna
    identificata da "str_pos".
    """
    # Split line in a list of strings:
    str_list = line.split(',')
    # Isola la stringa nella posizione "str_pos":
    string = str_list[str_pos]
    
    return string

# TODO: usando il with, serve ancora il try dentro? provare!
def json2dict(json_file, parent=None):
    """Legge un file json restituendo il contenuto in un dict."""
    with open(json_file, 'r') as jfile:
        # legge il JSON convertendolo in dict, contenente oggetti unicode:
        try:
            json_dict = json.load(jfile)
        except ValueError, exc_inst:
            err_dlg(title=cfg.STR_ERROR,
                           errmsg=exc_inst.__str__(),
                           parent=parent,
                           suggestion='Check the file ' + json_file)
        else:
            return json_dict

def dict2json(json_dict, json_file):# TODO: togliere. perche' togliere?
    """Scrive un dict in un file json."""
    with open(json_file, 'w') as jfile:
        json.dump(json_dict, jfile, indent=4)        
    
    
def err_dlg(title, errmsg, parent=None, suggestion=''):
    """Error dialog con titolo, messaggio ed eventuale suggerimento."""
    dlg = wx.MessageDialog(parent, "Error: "+errmsg+".\n"+suggestion,
                           title, wx.ICON_ERROR)
    dlg.ShowModal()
    dlg.Destroy()
    
def msg_dlg(title, msg, parent=None, suggestion=''):
    """Info dialog con titolo, messaggio ed eventuale suggerimento."""
    # Tasto OK e icona Info.
    dlg = wx.MessageDialog(parent, msg+".\n"+suggestion, title,
                           wx.OK | wx.ICON_INFORMATION)
    dlg.ShowModal()
    dlg.Destroy()
    

def keygen(input_str):
    """Genera una chiave di 16 cifre a partire da una stringa."""
    # l'algoritmo sha1 restituisce una stringa hex da 40 caratteri
    hash_str = sha1(input_str).hexdigest()
    key = hash_str[4:12] + hash_str[23:31]
    
    return key
    

def is_verified_key(mail, key):
    """Verifica la validita' della product key."""
    hash_mail = keygen(mail)
    
    if key == hash_mail:
        return True
    else:
        return False
    

class VsActivation(wx.Dialog):
    """Classe per la registrazione dell'utente e l'attivazione del software"""
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY,
                           title='Virtual Sensei Lite activation',
                           size=wx.DefaultSize)
        
        # Creazione widget della dialog
        text_register = wx.StaticText(self, wx.ID_ANY,
                                      "Please, insert a valid email address\n"+
                                      "to receive your FREE product key.\n"+
                                      "Then paste the key in the field below.")
        self.field_mail = wx.TextCtrl(self, wx.ID_ANY,
                                      'Insert your email address')
        self.button_send = wx.Button(self, wx.ID_ANY, 'Send')
        self.gauge = wx.Gauge(self, wx.ID_ANY, style=wx.GA_HORIZONTAL)
        line = wx.StaticLine(self)       
        self.field_key = wx.TextCtrl(self, wx.ID_ANY,
                                        'Paste the key here')
        self.text_status = wx.StaticText(self, wx.ID_ANY, '')
        self.button_ok = wx.Button(self, wx.ID_OK)
        self.button_cancel = wx.Button(self, wx.ID_CANCEL)
        
        # Configurazione sizer
        reg_sizer = wx.GridBagSizer(8, 3)
        reg_sizer.Add(text_register, pos=(0, 0), span=(1, 3), flag=wx.ALL |
                      wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL,
                      border=cfg.EMPTY_BORDER)
        reg_sizer.Add(self.field_mail, pos=(1, 0), span=(1, 2), flag=wx.LEFT |
                      wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND,
                      border=cfg.EMPTY_BORDER)
        reg_sizer.Add(self.button_send, pos=(1, 2), flag=wx.RIGHT |
                      wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL,
                      border=cfg.EMPTY_BORDER)
        reg_sizer.Add(self.gauge, pos=(2, 0), span=(1, 3), flag=wx.ALL | wx.EXPAND |
                      wx.ALIGN_CENTER_VERTICAL, border=cfg.EMPTY_BORDER)

        reg_sizer.Add(line, pos=(3, 0), span=(1, 3), flag=wx.ALL | wx.EXPAND |
                      wx.ALIGN_CENTER_VERTICAL, border=cfg.EMPTY_BORDER)
        
        reg_sizer.Add(self.field_key, pos=(4, 0), span=(1, 3), flag=wx.ALL
                      | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND,
                      border=cfg.EMPTY_BORDER)
        
        reg_sizer.Add(self.text_status, pos=(5, 0), span=(1, 3), flag=wx.ALL |
                      wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL,
                      border=cfg.EMPTY_BORDER)
        
        # Lascio una riga vuota nel sizer per distanziare i pulsanti OK e Cancel
        reg_sizer.Add(self.button_ok, pos=(7, 1), flag=wx.TOP | wx.BOTTOM |
                      wx.ALIGN_RIGHT | wx.ALIGN_BOTTOM,
                      border=cfg.EMPTY_BORDER)
        reg_sizer.Add(self.button_cancel, pos=(7, 2), flag=wx.TOP | wx.RIGHT |
                      wx.BOTTOM | wx.ALIGN_LEFT | wx.ALIGN_BOTTOM,
                      border=cfg.EMPTY_BORDER)
        
        # Assegna il GridBagSizer alla dialog
        self.SetSizerAndFit(reg_sizer)
        
        # Binding eventi
        self.Bind(wx.EVT_TEXT, self.on_field_mail_changed, self.field_mail)   
        self.Bind(wx.EVT_BUTTON, self.on_button_send, self.button_send)
        self.Bind(wx.EVT_TEXT, self.on_field_key_changed, self.field_key)
        self.Bind(wx.EVT_BUTTON, self.on_button_ok, self.button_ok)
        self.Bind(wx.EVT_BUTTON, self.on_button_cancel, self.button_cancel)
        
        # Disabilita alcuni elementi inzialmente
        self.button_send.Enable(False)
        self.button_ok.Enable(False)
        
        # Mostra dialog               
        self.ShowModal()
        
        
    def on_field_mail_changed(self, event):
        """Se il campo mail non e' vuoto, si attiva il pulsante send."""
        if self.field_mail.GetValue():
            self.button_send.Enable(True)
            self.text_status.SetLabel('Click on Send.')
        else:
            self.button_send.Enable(False)
            self.text_status.SetLabel('')
        
    def on_button_send(self, event):
        """Invia mail con chiave all'utente e a virtual sensei."""
        self.button_send.Enable(False)
        self.gauge.SetValue(20)
        
        # Un messaggio di status messo qui da problemi, perche' viene 
        # visualizzato in ritardo a causa dell'inizio dell'invio mail.
        # Message data
        from_addr = cfg.SW_EMAIL
        user_addr = self.field_mail.GetValue()
        
        # Create a text/plain message
        # la classe mimetext restituisce un oggetto di tipo Message
        msg = MIMEText('Hi,\n' \
                       'This is your product key for Virtual Sensei Lite.\n' \
                       'Please, paste it in the activation form.\n\n' +
                       keygen(user_addr) +
                       '\n\nBest regards,\n\n' \
                       'Alessandro Timmi\n' +
                       'Virtual Sensei\n' \
                       'www.virtualsensei.it')
        # Header della mail: il metodo speciale __setitem__(name, val)
        # dell'oggetto Message viene richiamato in questo modo:
        msg['Subject'] = 'Virtual Sensei Lite product key'
        msg['From'] = 'Virtual Sensei <' + from_addr + '>'
        msg['To'] = user_addr
                
        # Indirizzi a cui viene effettivamente inviata la mail
        # (invia una copia anche a virtual sensei)
        to_addrs = [msg['To'], cfg.SW_EMAIL]
        # Send the mail: metto tutto in un try perche' l'eccezione e' la stessa.
        try:
            self.gauge.SetValue(40)
            server = smtplib.SMTP(cfg.VS_SMTP_SERVER)
            self.gauge.SetValue(60)
            server.login(cfg.VS_SMTP_USER, cfg.VS_SMTP_PASS)
            self.gauge.SetValue(80)
            server.sendmail(from_addr, to_addrs, msg.as_string())
            
        except smtplib.SMTPException, exc_inst:
            # Eccezione di base del modulo smtplib, ma non so quando di verifica
            self.text_status.SetLabel(cfg.STR_ERROR + exc_inst.__str__())
            return
        except socket.gaierror, exc_inst:
            # se manca la connessione ad internet
            self.text_status.SetLabel(cfg.STR_ERROR + exc_inst.__str__()+'.\n'+
                                      cfg.STR_CHECK_YOUR_INTERNET_CONNECTION)
            return        
        except socket.error, exc_inst:
            # se il server di posta non risponde
            self.text_status.SetLabel(cfg.STR_ERROR + exc_inst.__str__()+'.\n'+
                                      cfg.STR_TRY_AGAIN_LATER)
            return        
        else:
            self.gauge.SetValue(100)
            self.text_status.SetLabel('Email correctly sent.')
        server.quit()
        self.button_send.Enable(True)
        
    def on_field_key_changed(self, event):
        """Verifica email e chiave immessi dall'utente."""
        mail = self.field_mail.GetValue()
        key = self.field_key.GetValue()
        
        if is_verified_key(mail, key):
            self.field_key.Enable(False)
            self.button_cancel.Enable(False)
                        
            # Salva mail e chiave nell'activation_dict
            self.Parent.activation_dict['mail'] = mail
            self.Parent.activation_dict['product_key'] = key      

            # e scrive l'activation_dict nell'activation file
            dict2json(self.Parent.activation_dict, cfg.ACTIVATION_FILE_NAME)
            
            # Traccia l'evento attivazione in google analytics
            ga_track_event('Activations', cfg.SW_VERSION)    
            
            self.text_status.SetLabel("Product key is valid.\n"+
                                      cfg.SW_NAME+" is now activated.\n"+
                                      "Please, click on OK.")
            self.Parent.SetStatusText(cfg.SW_NAME+" is now activated.", 0)
            
            # solo dopo le precedenti operazioni l'utente puo' procedere
            self.button_ok.Enable(True)            
        else:
            # altrimenti disabilita il tasto ok e mostra un messaggio di errore:
            self.button_ok.Enable(False)
            self.text_status.SetLabel(cfg.STR_ERROR +
                                      ': invalid product key.')               
                
    def on_button_cancel(self, event):
        """Il pulsante cancel chiude il programma."""
        self.Parent.OnCloseGuiSilently(None)
        
    def on_button_ok(self, event):
            # Chiude la dialog:
            self.Destroy()
            
       
def ga_track_event(category, sw_version):
    """Push the minimum data to track download events in google analytics.""" 
    # Create a string, calculate the SHA1 sum of it, convert this from base 16
    # to base 10 and get first 10 digits of this number.
    visitor = str(int("0x%s" % sha1('xxxxx').hexdigest(), 0))[:10]
    
    # Collect everything in a dictionary.
    # utmcc contiene i cookie, di cui l'unico obbligatorio e' utma.
    # Il parametro utma contiene parecchie info, ma possono essere
    # tutte settate ad 1 tranne l'ID del visitatore.
    # utma = XXXX.RRRR.FFFF.PPPP.CCCC.N
    # Where:
    # XXXX = A domain hash. A domain hash is simply a group of numbers that relates
    # directly back to the domain name of a site. Think of it as a sort of numerical
    # representation of your domain name.
    # RRRR = A random number the GoAn script generates to be used as a Unique ID for
    # each visitor.
    # FFFF = A timestamp of the first visit/session for the user. Or in English, the
    # time someone first hit the site. As a note, all of these timestamps are in the
    # same format you'd give if you ran a php date('U'); call. They're not in the
    # date format we humans are used to seeing, but they're just as effective.
    # PPPP = A timestamp of the Previous visit by the user. Or the date and time the
    # user last visited your site.
    # CCCC = The current time, in the same timestamp format as the previous two.
    # N = The number of visitor Sessions the user has had since their first visit.
    # This number gets incremented by 1 each time the returning visitor starts a new
    # Session.
    DATA = {"utmwv": "5.3.2d",                      # Tracking code version
            "utmn": str(randint(1, 9999999999)),    # id univoco della gif
            "utmp": cfg.ANALYTICS_PAGE,             # Page request of the current page
            "utmac": cfg.ANALYTICS_ID,              # Account String. Appears on all requests
            "utmcc": "__utma=%s;" % ".".join(["1", visitor, "1", "1", "1", "1"]), # vedi su
            "utme": '5(' + category + '*VSLite*v.' + sw_version + ')', # parametri evento
            "utmt": "event"}                        # tipo della richiesta
    
    # Costruisce un URL da una tupla costituita da 6 argomenti standard:
    gif_request = urlunparse(("http",                       # scheme
                              "www.google-analytics.com",   # netloc
                              "/__utm.gif",                 # path
                              "",                           # parameters
                              urlencode(DATA),              # query
                              ""))                          # fragment
    
    # Make the request
    # print "Requesting", URL
    try: 
        #print urllib2.urlopen(gif_request).info()
        urllib2.urlopen(gif_request)
    except urllib2.URLError, exc_inst:
        # Se ci sono problemi continua silenziosamente:
        #print exc_inst.__str__() # Stampa i dettagli dell'eccezione
        pass
                