'''
Created on Mar 2, 2012

@author: Alex
'''
import visual as vp

import cfg 
   
class Vs3dScene(): 
    """Crea la scena 3D.
    
    Attributi:
    parent = in questo caso il parent della scena 3D e' la GUI principale;
    scene = l'oggetto scena 3D;
    joints = una lista contenente i giunti dello scheletro;
    links = una lista contenente i link dello scheletro;
    semilinks = una lista contenente i semilink dello scheletro:
    floor = l'oggetto pavimento.
    
    """
    def __init__(self, parent):
        self.Parent = parent    
        # Imposta la finestra contenente l'animazione e le luci:
        self.scene = vp.display(title=cfg.SW_NAME+' - 3D skeleton animation',
                               width=640, height=480,
                               ambient=vp.color.gray(0.4))
        self.scene.lights = [vp.distant_light(direction=(0.22, 0.44, 0.88),
                                              color=vp.color.gray(0.6)),
                             vp.distant_light(direction=(-0.88, -0.22, -0.44),
                                              color=vp.color.gray(0.6))]
        
        # Questo comando evita che quando si chiude la scena dell'animazione
        # si esca anche dal programma:
        self.scene.exit = False   
                    
        # Creazione pavimento. I parametri dimensionali seguono il seguente
        # criterio:
        # length = dimensione lungo l'asse orientabile tramite il comando axis:
        # quest'ultimo puo' essere assunto come la normale al pavimento,
        # pertanto length diventa lo spessore del pavimento.
        # height =  dimensione
        self.floor = vp.box(length=cfg.FLOOR_THICKNESS, height=cfg.FLOOR_WIDTH,
                       width=cfg.FLOOR_DEPTH, color = vp.color.orange,
                       material = vp.visual.materials.wood)
        
        # Riferimento assoluto:
        O = vp.sphere(pos=vp.vector(0, 0, 0), radius=3 * cfg.RADIUS,
                      color=vp.color.white)
        vp.arrow(pos=O.pos, axis=vp.vector(1, 0, 0), length=cfg.LEN_WORLD_AXIS,
                 color=vp.color.red, opacity=cfg.OPACITY_WORLD_AXIS)
        vp.arrow(pos=O.pos, axis=vp.vector(0, 1, 0), length=cfg.LEN_WORLD_AXIS,
                 color=vp.color.green, opacity=cfg.OPACITY_WORLD_AXIS)
        vp.arrow(pos=O.pos, axis=vp.vector(0, 0, 1), length=cfg.LEN_WORLD_AXIS,
                 color=vp.color.blue, opacity=cfg.OPACITY_WORLD_AXIS)
        
        # Lista dei giunti piu' il centro di massa (COM) del corpo umano:
        self.joints = [vp.sphere(radius=r, make_trail=True) for r in cfg.RADII]
        
        # Visualizza/nasconde il centro di massa (COM) del corpo umano in base allo stato della
        # relativa variabile booleana:
        self.joints[cfg.COM].visible = cfg.is_visible_com
         
        # Attiva/disattiva le trail secondo la scelta dell'utente.
        self.joints_trails_switch()
        
        # Definizione dei link (segmenti articolari) rappresentati
        # tramite 2 piramidi contrapposte.
        # Crea per ogni link un oggetto frame, che funziona come un sistema di
        # riferimento locale:
        self.links = [vp.frame() for j_num in range(cfg.N_LINKS)]
        
        # Le due possibili direzioni dei semilink:
        axes = [(1, 0, 0), (-1, 0, 0)]
        # Per ogni link inserisce due piramidi (semilink) tangenti sulle
        # rispettive basi e con vertici agli antipodi:
        self.semilinks = [[vp.pyramid(frame=link, axis=ax)
                           for ax in axes] for link in self.links]
        
        # Legge il config file ed imposta alcune preferenze dell'animazione:
        self.set_anim_from_config_file()
        
        
        
    def joints_trails_switch(self):
        """Attiva/disattiva la trail dei giunti secondo selezione utente."""
        for j_num in range(cfg.N_JOINTS_PLUS_COM):
            if j_num in cfg.joints_trail_on:
                # reimposta il retain al valore vero, ottenenendo la visualizzazione della trail:
                self.joints[j_num].retain = self.Parent.config_dict["trail_retain"]
            else:
                # Elimina i punti della trail precedentemente creati:
                self.joints[j_num].trail_object.pos = []
                self.joints[j_num].retain = 0
                
                
    def set_anim_from_config_file(self):
        """Read the config file and set the animation preferences."""
        
        self.scene.background = self.Parent.config_dict["color_bg"]
        
        for link in self.semilinks:
            for semilink in link:
                semilink.color = self.Parent.config_dict["color_links"]
        
        color_joints = self.Parent.config_dict["color_joints"]
        color_ends = self.Parent.config_dict["color_ends"]
        color_com = self.Parent.config_dict["color_com"]
        # Lista dei colori dei giunti compreso il centro di massa (COM) del
        # corpo umano:
        colors = [color_ends,
                  color_joints,
                  color_joints,
                  color_joints,
                  color_joints,
                  color_ends,
                  color_joints,
                  color_joints,
                  color_ends,
                  color_joints,
                  color_joints,
                  color_ends,
                  color_joints,
                  color_joints,
                  color_ends,
                  color_com]
        
        for j_num in range(cfg.N_JOINTS_PLUS_COM):
            # Imposta il colore del giunto
            self.joints[j_num].color = colors[j_num]
            # imposta il colore della trail (lo stesso del giunto)
            self.joints[j_num].trail_object.color = colors[j_num]
        
        
        
    def render_anim_frame(self, XYZj, XYZGbody, floor_data): 
        """Renderizza il frame corrente dell'animazione.
        
        Input:
        XYZj = array bidimensionale contenente le coordinate XYZ assolute dei giunti;
        XYZGbody = coordinate del baricentro (COM) del corpo umano;
        floor = il pavimento;
        floor_data = coordinate di un punto appartenente al pavimento e coseni
        direttori della normale ad esso.
        """         
        # Posizione del centro del pavimento al frame corrente:
        self.floor.pos = floor_data[0:3]
        # Orientamento della normale al pavimento: visual orienta il box in modo
        # che la dimensione "length" sia disposta lungo questo asse:
        self.floor.axis = floor_data[3:cfg.N_PARAMETERS_FLOOR]
        # Senza senso: occorre reimpostare la "length" del box ad ogni
        # modifica di "axis", altrimenti visual imposta la "length" pari al
        # modulo del vettore "axis":
        self.floor.length = cfg.FLOOR_THICKNESS
        
        # Posizione dei giunti (escluso il COM):
        for j in range(cfg.N_JOINTS):
            self.joints[j].pos = vp.vector(XYZj[3 * j], XYZj[3 * j + 1],
                                           XYZj[3 * j + 2])
            
        # Posizione del centro di massa (COM) del corpo umano:
        self.joints[cfg.COM].pos = vp.vector(XYZGbody[:])
        
        # Centra la scena sul giunto Torso:
        self.scene.center = self.joints[cfg.TORSO].pos 
        
        # Calcola la lunghezza dei vari link (varia ad ogni istante ed e' pari
        # alla distanza fra i due giunti collegati dal link stesso):
        link_lenght = [vp.mag(self.joints[j1].pos -
                              self.joints[j2].pos) for
                       j1,j2 in cfg.LINKED_JOINTS]
               
        # Le misure dei lati della base delle piramidi (= mezzo link) sono costanti:
        p_height = 2 * cfg.RADIUS
        p_width = 2 * cfg.RADIUS  
        for link in range(cfg.N_LINKS):
            # La lunghezza di ogni piramide deve essere pari a meta'
            # della distanza corrente tra i due giunti collegati dal link. 
            self.semilinks[link][0].size = vp.vector(link_lenght[link] / 2,
                                                p_height, p_width)
            self.semilinks[link][1].size = vp.vector(link_lenght[link] / 2,
                                                p_height, p_width)
           
            # Nel riferimento del link, le piramidi hanno l'origine a distanza
            # lunghezza_link / 2 dall'origine del riferimento locale (origine
            # del frame):
            self.semilinks[link][0].pos = vp.vector(link_lenght[link] / 2, 0, 0)
            self.semilinks[link][1].pos = vp.vector(link_lenght[link] / 2, 0, 0)
    
            # Posizione assoluta del primo vertice dei link (= origine dei frame):
            self.links[link].pos = self.joints[cfg.LINKED_JOINTS[link][0]].pos
        
            # Posizione vertice opposto dei link:
            # link.axis = joint2.pos - joint1.pos
            self.links[link].axis = ( - self.joints[cfg.LINKED_JOINTS[link][0]].pos +
                                      self.joints[cfg.LINKED_JOINTS[link][1]].pos)
            
   
        
        
        

        
